﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using System.Net.Mail;
using System.Net;

namespace Scheduler
{
    public class TestJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            using (var message = new MailMessage("bill.applegate@gmail.com", "bill.applegate@gmail.com"))
            {
                message.Subject = "Test";
                message.Body = "Test at " + DateTime.Now;
                using (SmtpClient client = new SmtpClient
                {
                    EnableSsl = true,
                    Host = "smtp.gmail.com",
                    Port = 587,
                    Credentials = new NetworkCredential("bill.appplegate@gmail.com", "galactus27")
                })
                {
                    client.Send(message);
                }
            }
        }
    }
}
