﻿using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Library;
using Library.Exceptions;
// This is the library used to export to CSV.
using Library.Exporter;
using Models;
using Models.Interfaces;
using Models.Models;
using DataTables.Mvc;
using TemplateApplication.Filters;
using TemplateApplication.Models.ViewModels;
using WebMatrix.WebData;

namespace TemplateApplication.Controllers
{
    // To access any of the post methods or controller actions in this controller,
    // the user must be an administrator.
    [Authorize(Roles = "Administrator")]
    [InitializeSimpleMembership]
    public class SiteUtilitiesController : Controller
    {
        private const int StartingPage = 1;
        private const int RecordsPerPage = 10;

        private readonly ITradeLeadRepository _tradeLeadRepository;
        private readonly IListingRepository   _listingRepository;
        private readonly IListingService      _listingService;
        private readonly IValidationService   _validationService;
        private readonly IDatabaseContext     _database;

        private static readonly object Lock = new object();

        public SiteUtilitiesController(ITradeLeadRepository tradeLeadRepository, IListingRepository listingRepository, IDatabaseContext database, IListingService listingService, IValidationService validationService)
        {
            _tradeLeadRepository = tradeLeadRepository;
            _listingRepository   = listingRepository;
            _listingService      = listingService;
            _validationService   = validationService;

            _database = database;
        }

        // This is the Action method for the Listing Conversion utility.
        // This utility is used to convert a "Bulk" listing to a "Manual" listing when a 
        // user returns a form indicating they'd like to pay for a description.
        public ActionResult ListingConversion()
        {
            return View();
        }

        // This procedure converts a "bulk" listing in the database to a "Manual" listing.
        // "Manual" listings have a connected user account so a user can log in and manage them.
        // They also show up in certain utilities, like the billable listings utility.
        // This is the first step of the process when a user signs up for a subscription.
        // We convert their bulk listing to a manual listing so we can create an order for the listing.
        // todo, need to finish up the implementation of this feature.
        [HttpPost]
        public JsonResult ConvertBulkListing(int listingIdentifier, string username, string emailAddress)
        {
            try
            {
                // Make sure a username and email address are provided.
                if(string.IsNullOrEmpty(username) || string.IsNullOrEmpty(emailAddress))
                    return Json(new { errorMessage = "Please provide a username and email address." });
                if(!IsEmailAddressValid(emailAddress))
                    return Json(new { errorMessage = "Please provide valid email address." });

                // Create a linked username and password.
                var newUserID = CreateUserNamePassword(username, emailAddress);
                if (newUserID.HasValue)
                {
                    // Link the newly created user account to the listing.
                    _database.LinkListingToUserID(listingIdentifier, newUserID.Value);
                    // Add a single entry to dbo.Listings_Directories for the default directory.
                    _database.AddDefaultDirectoryToListing(listingIdentifier);
                    // Convert the listing type to "Manual" as it will now be managed by administrators.
                    _listingRepository.UpdateListingType(listingIdentifier, "Manual");
                    // Return a success message.
                    return Json(new { successMessage = "Listing has been converted successfully." });
                }
                return Json(new { errorMessage = "A user could not be created." });
            }
            catch (MembershipCreateUserException exception)
            {
                return Json(new { errorMessage = exception.Message });
            }
            catch (Exception)
            {
                return Json(new { errorMessage = "There was an issue converting this listing." });
            }
        }

        // This helper method creates a new user account and returns the user identifier.
        public int? CreateUserNamePassword(string userName, string email)
        {
            // Check to make sure email addresses are not associated with more than one user account.
            if (!_database.EmailExists(email))
            {
                // Generate a temporary password for the user account.
                var temporaryPassword = Membership.GeneratePassword(8, 1);

                // Create the user account using the temporary password, and associate the email address.
                WebSecurity.CreateUserAndAccount(userName, temporaryPassword);
                var userID = WebSecurity.GetUserId(userName);
                _database.SaveEmailAddress(userID, email);
                return userID;
            }
            // If the email address exists in the system, throw a new exception.
            throw new MembershipCreateUserException("The email address provided exists in the database already.");
        }

        private bool IsEmailAddressValid(string emailAddress)
        {
            return new EmailAddressAttribute().IsValid(emailAddress);
        }

        // Action method for the Trade Lead editor utility.
        public ActionResult TradeLeadEditor()
        {
            TradeLeadParameterSet parameters = new TradeLeadParameterSet
            {
                Page = StartingPage,
                NumberOfRecords = RecordsPerPage
            };
            PagedList<TradeLead> tradeLeads = _tradeLeadRepository.GetTradeLeadsPaged(parameters);
            return View(tradeLeads);
        }

        // Action method for the Billable Listings utility.
        public ActionResult BillableListings()
        {
            BillableListingParameterSet parameters = new BillableListingParameterSet
            {
                Page = StartingPage,
                NumberOfRecords = RecordsPerPage
            };
            PagedList<BillableListingsModel> model = _listingRepository.GetBillableListings(parameters);
            return View(model);
        }

        // Action method for the View Listings utility.
        public ActionResult Listings()
        {
            ManagedListingsReportParameters parameters = new ManagedListingsReportParameters
            {
                Page            = StartingPage,
                NumberOfRecords = RecordsPerPage
            };

            ManagedListingsViewModel model = new ManagedListingsViewModel
            {
                ManagedListings = _listingRepository.GetManagedListings(parameters),
                ListingTypes    = _listingRepository.GetListingTypes().Where(x => x.Type != "Bulk").ToList()
            };

            return View(model);
        }

        // Action method for the Site Users utility.
        public ActionResult Users()
        {
            SiteUsersModel model = new SiteUsersModel
            {
                SiteUsersPaged = _database.GetSiteUsers(StartingPage, RecordsPerPage, null, null, null, null, null)
            };
            return View(model);
        }

        /// <summary>
        /// Returns details about the currently selected user.
        /// </summary>
        /// <param name="id">The user identifier for the selected user.</param>
        /// <returns>View displaying the details for a specified user.</returns>
        public ActionResult UserDetail(int id)
        {
            UserDetailViewModel model = new UserDetailViewModel
            {
                UserDetail   = _database.GetUserDetails(id),
                UserListings = _listingRepository.GetUserListings(id)
            };
            return View(model);
        }

        // Action method fro the Pricing utility.
        public ActionResult Pricing()
        {
            PricingViewModel model = new PricingViewModel
            {
                // Retrieve the current pricing model.
                CurrentPricingModel = _database.GetCurrentListingPricingModel(),
                // Retrieve all the historical changes to the pricing model for display.
                HistoricalPricingModels = _database.GetPricingHistory()
            };
            return View(model);
        }

        // Post method for the Pricing utility form. This POST method saves a new pricing model.
        [HttpPost]
        public ActionResult Pricing(PricingModel currentPricingModel)
        {
            // Validate the pricing model values adhere to required validation.
            if (_validationService.ValidatePricingModel(currentPricingModel))
            {
                try
                {
                    // Save the new pricing model associated to the change user.
                    int currentUserID = WebSecurity.GetUserId(User.Identity.Name);
                    _database.SaveNewPricingModel(currentPricingModel, currentUserID);
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "There was an error when updating pricing information in the database.");
                }
            }
            else
            {
                // If validation fails, return an explanation as to why, and render the view.
                foreach (KeyValuePair<string, string> entry in _validationService.GetErrorDictionary())
                {
                    ModelState.AddModelError("", entry.Value);
                }
            }

            // Return the model to the view, with any error messages, if applicable.
            PricingViewModel model = new PricingViewModel
            {
                // Retrieve the current pricing model.
                CurrentPricingModel = _database.GetCurrentListingPricingModel(),
                // Retrieve all the historical changes to the pricing model for display.
                HistoricalPricingModels = _database.GetPricingHistory()
            };
            return View(model);
        }

        // Action method for the Configure Directories utility.
        // This utility allows a user to add / edit directories in the site.
        public ActionResult ConfigureDirectories()
        {
            var directories = _database.GetDirectories().ToList();
            // Determine whether a default directory has been assigned.
            bool doesDefaultDirectoryExists = directories.Any(x => x.IsDefault);

            DirectoryConfigurationViewModel model = new DirectoryConfigurationViewModel
            {
                // Return the directories in the site.
                Directories               = directories,
                // Return the default directory flag.
                DoesDefaultDirectoryExist = doesDefaultDirectoryExists
            };
            return View(model);
        }

        // This is the post method for the add new directory form.
        [HttpPost]
        public ActionResult ConfigureDirectories(Directory directory)
        {
            // Validate the directory submitted.
            if (_validationService.ValidateDirectory(directory))
            {
                // Save the new directory associated with the change user.
                int userIdentifier = WebSecurity.GetUserId(User.Identity.Name);
                _database.UpsertDirectory(directory, userIdentifier);
            }
            else
            {
                // Add the validation errors to the ModelState to be displayed in the view.
                foreach (KeyValuePair<string, string> entry in _validationService.GetErrorDictionary())
                {
                    ModelState.AddModelError("", entry.Value);
                }
            }

            // Return the model to the view with any validation errors if necessary.
            var directories = _database.GetDirectories().ToList();
            bool doesDefaultDirectoryExists = directories.Any(x => x.IsDefault);
            DirectoryConfigurationViewModel model = new DirectoryConfigurationViewModel
            {
                Directories               = directories,
                DoesDefaultDirectoryExist = doesDefaultDirectoryExists
            };
            return View(model);
        }

        // Action method for the Edit Directory functionality.
        public ActionResult EditDirectory(int id)
        {
            var directories = _database.GetDirectories();
            // Retrieve the selected directory to be edited.
            var directory = directories.First(x => x.DirectoryID == id);

            if (directory != null)
            {
                return View(directory);
            }
            // If the specified directory could not be found, return the user to:
            return RedirectToAction("ConfigureDirectories", "SiteUtilities");
        }

        // POST method for the Edit Directory utility.
        [HttpPost]
        public ActionResult EditDirectory(Directory directory)
        {
            // Validate the directory values submitted.
            if (_validationService.ValidateDirectory(directory))
            {
                int userIdentifier = WebSecurity.GetUserId(User.Identity.Name);
                _database.UpsertDirectory(directory, userIdentifier);
            }
            else
            {
                // Add any validation errors to the ModelState.
                foreach (KeyValuePair<string, string> entry in _validationService.GetErrorDictionary())
                {
                    ModelState.AddModelError("", entry.Value);
                }
                return View(directory);
            }
            // If we arrived here, an unknown issue may have occured.
            // Return them to the Configure Directories utility.
            return RedirectToAction("ConfigureDirectories");
        }

        // Action method for the "Configure Industries" utility.
        // This utility allows a user to Add/Edit industries in the site.
        public ActionResult ConfigureIndustries()
        {
            // Retrieve the industries in the site currently.
            var model = new ConfigureIndustriesModel
            {
                Industries = _database.GetIndustries()
            };
            return View(model);
        }

        #region POST Methods

        // This POST method is used in the users utility to convert a user to an admin.
        public void ConvertUserToAdmin(int userID)
        {
            WebSecurity.GetUserId(User.Identity.Name);
            _database.ConvertUserToAdmin(userID);
        }

        // Deletes a specific trade lead.
        [HttpPost]
        public void DeleteTradeLead(int tradeLeadID)
        {
            _tradeLeadRepository.DeleteTradeLead(tradeLeadID);
        }

        // This POST method deletes a directory from the system.
        // A directory cannot be deleted if it is associated with any Listing records.
        [HttpPost]
        public void DeleteDirectory(int directoryID)
        {
            _database.DeleteDirectory(directoryID);
        }

        // POST method used to create orders for selected "Manual" listings.
        [HttpPost]
        public ActionResult GenerateOrders(List<int> listingIdentifiers)
        {
            // Lock the controller method so if another user happens to generate orders too there won't be duplicates.
            lock (Lock)
            {
                // If there are selected listing identifiers...
                if (listingIdentifiers != null && listingIdentifiers.Count != 0)
                {
                    // This List stores all the error messages that may be encountered when generating orders.
                    List<ActionError> errors = new List<ActionError>();

                    int userIdentifier = WebSecurity.GetUserId(User.Identity.Name);
                    foreach (int identifier in listingIdentifiers)
                    {
                        try
                        {
                            _listingService.CreateNewManualOrder(identifier, userIdentifier);
                        }
                        catch (ListingServiceException exception)
                        {
                            errors.Add(new ActionError(string.Format("Listing Identifier {0}", identifier), exception.Message));
                        }
                    }
                    // Return the error string to the view.
                    return PartialView("_ErrorsView", errors);
                }

                return Json("<div class='note note-danger note-shadow'>Please specify one or more listings to generate orders for.</div>");
            }
        }

        // Return the industries in the site as JSON.
        // Currently used to reload the industries datatable in "Configure Industries."
        [HttpPost]
        public JsonResult GetIndustries()
        {
            return Json(_database.GetIndustries().OrderByDescending(x => x.IndustryID));
        }

        // This Post method saves a new industry, or saves edits to an existing one.
        [HttpPost]
        public JsonResult SaveIndustry(Industry industry)
        {
            // Validate the newly created industry, or edits to an existing industry.
            if (_validationService.ValidateIndustry(industry))
            {
                _database.SaveIndustry(industry);
                return Json(new { Success = true, Message = "Industry saved successfully!" });
            }
            var errors = _validationService.GetErrorString();
            return Json(new { Success = false, Message = errors });
        }

        // Deletes a specified industry. Industries associated with listings cannot be deleted.
        [HttpPost]
        public void DeleteIndustry(int industryIdentifier)
        {
            _database.DeleteIndustry(industryIdentifier);
        }

        #endregion

        #region Bulk Listings Datatable Code

        [HttpPost]
        public string GetBulkListings([ModelBinder(typeof(BulkListingsBinder))] BulkListingsRequest request)
        {
            int start = request.Start;
            int length = request.Length;
            int page = (int)Math.Ceiling((double)start / length) + 1;

            IOrderedEnumerable<Column> sort = request.Columns.GetSortedColumns();
            var column = sort.FirstOrDefault();
            string sortColumn = null;
            string sortDirection = null;
            if (column != null)
            {
                sortColumn = column.Data;
                sortDirection = column.SortDirection.ToString();
            }

            BulkListingParameterSet parameters = new BulkListingParameterSet
            {
                Page = page,
                NumberOfRecords = length,
                ListingIdentifier = request.ListingIdentifier,
                BusinessName = request.BusinessName,
                FaxNumber = request.FaxNumber,
                SortColumn = sortColumn,
                SortDirection = sortDirection
            };

            var results = _listingRepository.GetBulkListings(parameters);
            return new JavaScriptSerializer().Serialize(new DataTablesResponse(request.Draw, results.PagedListItems, results.TotalRecords, results.TotalRecords));
        }


        public class BulkListingsBinder : DataTablesBinder
        {
            // Override the default BindModel called by ASP.NET and make it call Bind passing the type of your
            // implementation of IDataTablesRequest:
            public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                return Bind(controllerContext, bindingContext, typeof(BulkListingsRequest));
            }

            // Override MapAditionalProperties so you can set your aditional data into the model:
            protected override void MapAditionalProperties(IDataTablesRequest requestModel, NameValueCollection requestParameters)
            {
                var myModel = (BulkListingsRequest)requestModel;

                var listingIdentifier = Get<string>(requestParameters, "ListingIdentifier");
                var businessName = Get<string>(requestParameters, "BusinessName");
                var faxNumber = Get<string>(requestParameters, "FaxNumber");

                int value;
                bool isInteger = Int32.TryParse(listingIdentifier, out value);
                if (isInteger)
                    myModel.ListingIdentifier = value;
                else
                    myModel.ListingIdentifier = null;

                myModel.BusinessName = string.IsNullOrEmpty(businessName) ? null : businessName;
                myModel.FaxNumber = string.IsNullOrEmpty(faxNumber) ? null : faxNumber;
            }
        }

        public class BulkListingsRequest : DefaultDataTablesRequest
        {
            public int? ListingIdentifier { get; set; }
            public string BusinessName { get; set; }
            public string FaxNumber { get; set; }
        }

        #endregion

        #region Trade Leads Datatable Code

        /// <summary>
        /// Returns paged Trade Leads for the Trade Lead editor.
        /// </summary>
        /// <param name="request">Model sent from the data table containing state information for the table.</param>
        /// <returns>JSON string with all the returned rows.</returns>
        [HttpPost]
        public string GetTradeLeadsPaged([ModelBinder(typeof(TradeLeadBinder))] TradeLeadRequest request)
        {
            int start  = request.Start;
            int length = request.Length;
            int page   = (int)Math.Ceiling((double)start / length) + 1;

            // Retrieve the column that has been sorted, if available.
            IOrderedEnumerable<Column> sort = request.Columns.GetSortedColumns();
            var column = sort.FirstOrDefault();
            string sortColumn = null;
            string sortDirection = null;
            if (column != null)
            {
                sortColumn = column.Data;
                sortDirection = column.SortDirection.ToString();
            }

            // Set up the parameters for the GetTradeLeadsPaged procedure.
            TradeLeadParameterSet parameters = new TradeLeadParameterSet
            {
                Page            = page,
                NumberOfRecords = length,
                TradeLeadID     = request.TradeLeadID,
                Title           = request.Title,
                CreateDateStart = request.CreateDateStart,
                CreateDateEnd   = request.CreateDateEnd,
                UserName        = request.UserName,
                SortColumn      = sortColumn,
                SortDirection   = sortDirection
            };
            var results = _tradeLeadRepository.GetTradeLeadsPaged(parameters);

            return new JavaScriptSerializer().Serialize(new DataTablesResponse(request.Draw, results.PagedListItems, results.TotalRecords, results.TotalRecords));
        }

        public class TradeLeadBinder : DataTablesBinder
        {
            // Override the default BindModel called by ASP.NET and make it call Bind passing your specific Request.
            public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                return Bind(controllerContext, bindingContext, typeof(TradeLeadRequest));
            }

            // Override MapAditionalProperties so you can set your aditional data into the model:
            protected override void MapAditionalProperties(IDataTablesRequest requestModel, NameValueCollection requestParameters)
            {
                var myModel = (TradeLeadRequest)requestModel;
                var tradeLeadID = Get<string>(requestParameters, "TradeLeadID");
                var title = Get<string>(requestParameters, "Title");
                var createDateStart = Get<string>(requestParameters, "CreateDateStart");
                var createDateEnd = Get<string>(requestParameters, "CreateDateEnd");
                var userName = Get<string>(requestParameters, "UserName");

                int value;
                bool isInteger = Int32.TryParse(tradeLeadID, out value);
                if (isInteger)
                    myModel.TradeLeadID = value;
                else
                    myModel.TradeLeadID = null;

                myModel.Title = title;
                myModel.CreateDateStart = DateConverter.ConvertDate(createDateStart, false);
                myModel.CreateDateEnd = DateConverter.ConvertDate(createDateEnd, true);

                myModel.UserName = userName;
            }
        }

        public class TradeLeadRequest : DefaultDataTablesRequest
        {
            public int? TradeLeadID { get; set; }
            public string Title { get; set; }
            public DateTime? CreateDateStart { get; set; }
            public DateTime? CreateDateEnd { get; set; }
            public string UserName { get; set; }
        }

        #endregion

        #region Billable Listings Datatable Code
        [HttpPost]
        public string GetBillableListingsPaged([ModelBinder(typeof(BillableListingsBinder))] BillableListingsRequest request)
        {
            int start = request.Start;
            int length = request.Length;
            int page = (int)Math.Ceiling((double)start / length) + 1;

            IOrderedEnumerable<Column> sort = request.Columns.GetSortedColumns();
            var column = sort.FirstOrDefault();
            string sortColumn = null;
            string sortDirection = null;
            if (column != null)
            {
                sortColumn = column.Data;
                sortDirection = column.SortDirection.ToString();
            }

            BillableListingParameterSet parameters = new BillableListingParameterSet
            {
                Page                      = page,
                NumberOfRecords           = length,
                ListingIdentifier         = request.ListingIdentifier,
                BusinessName              = request.BusinessName,
                ListingCreateDateStart    = request.ListingCreateDateStart,
                ListingCreateDateEnd      = request.ListingCreateDateEnd,
                LastSubscriptionDateStart = request.LastSubscriptionDateStart,
                LastSubscriptionDateEnd   = request.LastSubscriptionDateEnd,
                OrderGenerated            = request.OrderGenerated,
                OrderGeneratedDateStart   = request.OrderGeneratedDateStart,
                OrderGeneratedDateEnd     = request.OrderGeneratedDateEnd,
                SortColumn                = sortColumn,
                SortDirection             = sortDirection
            };

            var results = _listingRepository.GetBillableListings(parameters);

            return new JavaScriptSerializer().Serialize(new DataTablesResponse(request.Draw, results.PagedListItems, results.TotalRecords, results.TotalRecords));
        }

        public class BillableListingsBinder : DataTablesBinder
        {
            // Override the default BindModel called by ASP.NET and make it call Bind passing your specific datatable request.
            public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                return Bind(controllerContext, bindingContext, typeof(BillableListingsRequest));
            }

            // Override MapAditionalProperties so you can set your aditional data into the model:
            protected override void MapAditionalProperties(IDataTablesRequest requestModel, NameValueCollection requestParameters)
            {
                var myModel = (BillableListingsRequest)requestModel;
                var listingIdentifier = Get<string>(requestParameters, "ListingIdentifier");
                var businessName = Get<string>(requestParameters, "BusinessName");
                var createDateStart = Get<string>(requestParameters, "ListingCreateDateStart");
                var createDateEnd = Get<string>(requestParameters, "ListingCreateDateEnd");
                var subscriptionDateStart = Get<string>(requestParameters, "LastSubscriptionDateStart");
                var subscriptionDateEnd = Get<string>(requestParameters, "LastSubscriptionDateEnd");
                var orderGenerated = Get<string>(requestParameters, "OrderGenerated");
                var generateDateStart = Get<string>(requestParameters, "OrderGeneratedDateStart");
                var generateDateEnd = Get<string>(requestParameters, "OrderGeneratedDateEnd");

                int value;
                bool isInteger = Int32.TryParse(listingIdentifier, out value);
                if (isInteger)
                    myModel.ListingIdentifier = value;
                else
                    myModel.ListingIdentifier = null;

                myModel.BusinessName = string.IsNullOrEmpty(businessName) ? null : businessName;

                myModel.ListingCreateDateStart = DateConverter.ConvertDate(createDateStart, false);
                myModel.ListingCreateDateEnd = DateConverter.ConvertDate(createDateEnd, true);
                myModel.LastSubscriptionDateStart = DateConverter.ConvertDate(subscriptionDateStart, false);
                myModel.LastSubscriptionDateEnd = DateConverter.ConvertDate(subscriptionDateEnd, true);
                myModel.OrderGenerated = string.IsNullOrEmpty(orderGenerated) ? (bool?)null : Convert.ToBoolean(orderGenerated);
                myModel.OrderGeneratedDateStart = DateConverter.ConvertDate(generateDateStart, false);
                myModel.OrderGeneratedDateEnd = DateConverter.ConvertDate(generateDateEnd, true);
            }
        }

        public class BillableListingsRequest : DefaultDataTablesRequest
        {
            public int? ListingIdentifier { get; set; }
            public string BusinessName { get; set; }
            public DateTime? ListingCreateDateStart { get; set; }
            public DateTime? ListingCreateDateEnd { get; set; }
            public DateTime? LastSubscriptionDateStart { get; set; }
            public DateTime? LastSubscriptionDateEnd { get; set; }
            public bool? OrderGenerated { get; set; }
            public DateTime? OrderGeneratedDateStart { get; set; }
            public DateTime? OrderGeneratedDateEnd { get; set; }
        }

        #endregion

        #region Managed Listing Datatable Code
        [HttpPost]
        public string GetManagedListingsPaged([ModelBinder(typeof(ManagedListingsBinder))] ManagedListingsRequest request)
        {
            int start  = request.Start;
            int length = request.Length;
            int page   = (int)Math.Ceiling((double)start / length) + 1;

            // Retrieve the sorted column, if available.
            IOrderedEnumerable<Column> sort = request.Columns.GetSortedColumns();
            var column = sort.FirstOrDefault();
            string sortColumn = null;
            string sortDirection = null;
            if (column != null)
            {
                sortColumn = column.Data;
                sortDirection = column.SortDirection.ToString();
            }

            ManagedListingsReportParameters parameters = new ManagedListingsReportParameters
            {
                Page              = page,
                NumberOfRecords   = length,
                ListingIdentifier = request.ListingIdentifier,
                BusinessName      = request.BusinessName,
                ListingTypeID     = request.ListingTypeID,
                ShowCancelled     = request.ShowCancelled,
                SortColumn        = sortColumn,
                SortDirection     = sortDirection
            };

            var results = _listingRepository.GetManagedListings(parameters);

            return new JavaScriptSerializer().Serialize(new DataTablesResponse(request.Draw, results.PagedListItems, results.TotalRecords, results.TotalRecords));
        }

        public class ManagedListingsBinder : DataTablesBinder
        {
            // Override the default BindModel called by ASP.NET and make it call Bind passing your specific datatable request.
            public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                return Bind(controllerContext, bindingContext, typeof(ManagedListingsRequest));
            }

            // Override MapAditionalProperties so you can set your aditional data into the model:
            protected override void MapAditionalProperties(IDataTablesRequest requestModel, NameValueCollection requestParameters)
            {
                var myModel = (ManagedListingsRequest)requestModel;
                var listingIdentifier = Get<string>(requestParameters, "ListingIdentifier");
                var businessName      = Get<string>(requestParameters, "BusinessName");
                var listingTypeID     = Get<string>(requestParameters, "ListingTypeID");
                var showCancelled     = Get<string>(requestParameters, "ShowCancelled");

                int value;
                bool isInteger = Int32.TryParse(listingIdentifier, out value);
                if (isInteger)
                    myModel.ListingIdentifier = value;
                else
                    myModel.ListingIdentifier = null;

                myModel.BusinessName  = string.IsNullOrEmpty(businessName) ? null : businessName;
                myModel.ListingTypeID = string.IsNullOrEmpty(listingTypeID) ? (int?)null : Convert.ToInt32(listingTypeID);
                myModel.ShowCancelled = string.IsNullOrEmpty(showCancelled) ? (bool?)null : Convert.ToBoolean(showCancelled);
            }
        }

        public class ManagedListingsRequest : DefaultDataTablesRequest
        {
            public int? ListingIdentifier { get; set; }
            public string BusinessName { get; set; }
            public int? ListingTypeID { get; set; }
            public bool? ShowCancelled { get; set; }
        }

        #endregion

        #region Site Users Datatable Code

        [HttpPost]
        public string GetSiteUsersPaged([ModelBinder(typeof(SiteUsersBinder))] SiteUsersRequest request)
        {
            int start  = request.Start;
            int length = request.Length;
            int page   = (int)Math.Ceiling((double)start / length) + 1;

            // Retrieve the sorted column, if available.
            IOrderedEnumerable<Column> sort = request.Columns.GetSortedColumns();
            var column = sort.FirstOrDefault();
            string sortColumn = null;
            string sortDirection = null;
            if (column != null)
            {
                sortColumn    = column.Data;
                sortDirection = column.SortDirection.ToString();
            }

            var results = _database.GetSiteUsers(page, length, request.UserID, request.UserName, request.EmailAddress, sortColumn, sortDirection);

            return new JavaScriptSerializer().Serialize(new DataTablesResponse(request.Draw, results.PagedListItems, results.TotalRecords, results.TotalRecords));
        }

        public class SiteUsersBinder : DataTablesBinder
        {
            // Override the default BindModel called by ASP.NET and make it call Bind passing your specific datatable request.
            public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                return Bind(controllerContext, bindingContext, typeof(SiteUsersRequest));
            }

            // Override MapAditionalProperties so you can set your aditional data into the model:
            protected override void MapAditionalProperties(IDataTablesRequest requestModel, NameValueCollection requestParameters)
            {
                var myModel = (SiteUsersRequest)requestModel;

                var userID       = Get<string>(requestParameters, "UserID");
                var userName     = Get<string>(requestParameters, "UserName");
                var emailAddress = Get<string>(requestParameters, "EmailAddress");

                int value;
                bool isInteger = Int32.TryParse(userID, out value);
                if (isInteger)
                    myModel.UserID = value;
                else
                    myModel.UserID = null;

                myModel.UserName     = string.IsNullOrEmpty(userName) ? null : userName;
                myModel.EmailAddress = string.IsNullOrEmpty(emailAddress) ? null : emailAddress;
            }
        }

        public class SiteUsersRequest : DefaultDataTablesRequest
        {
            public int? UserID { get; set; }
            public string UserName { get; set; }
            public string EmailAddress { get; set; }
        }

        #endregion
    }
}
