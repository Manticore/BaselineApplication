﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the history of all the pricing changes in the Pricing
-- database table.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetPricingHistory
AS
BEGIN

	SET NOCOUNT ON;

    SELECT PricingID, 
           CostPerDirectory, 
           CostPerDescription, 
           CreateDate, 
           CreateUser, 
           UP.UserName AS CreateUserName
    FROM dbo.Pricing P 
	INNER JOIN dbo.UserProfile UP
             ON UP.UserId = P.CreateUser
	ORDER BY CreateDate DESC;

END;