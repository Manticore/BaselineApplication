﻿CREATE TABLE [dbo].[SubscriptionStatuses]
(
	[SubscriptionStatusID] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[SubscriptionStatus] NVARCHAR(100) NOT NULL
);
