﻿using System.Collections.Generic;
using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class ViewOrdersModel
    {
        public PagedList<Order> Orders { get; set; }
        public List<OrderStatus> OrderStatuses { get; set; }
    }
}