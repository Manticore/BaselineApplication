﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the username of a user associated with a specified
-- email address.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetUserFromEmail
       @EmailAddress NVARCHAR(320)
AS
BEGIN

	SET NOCOUNT ON;

    SELECT UserName
    FROM dbo.EmailAddresses E 
    INNER JOIN dbo.UserProfile UP
		ON E.UserID = UP.UserId
    WHERE EmailAddress = @EmailAddress;

END;