﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure updates a specified directory if the DirectoryID is passed,
-- or saves a new Directory in the database if the directory does not exist.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.UpsertDirectory
       @DirectoryID INT = NULL, 
       @Name		NVARCHAR(100), 
       @Description NVARCHAR(500),
	   @IsDefault	BIT,
	   @UserID		INT
AS
BEGIN

	-- Grab the current date setting to be used in the stored procedure.
	DECLARE @CurrentDate DATETIME = dbo.GetCurrentDate();

    UPDATE dbo.Directories
           SET [Name]		 = @Name, 
               [Description] = @Description,
			   [IsDefault]	 = @IsDefault,
			   EditUser		 = @UserID,
			   EditDate		 = @CurrentDate
    WHERE [DirectoryID] = @DirectoryID;

	DECLARE @NewDirectoryID INT;

	-- If no row was updated, we know we need to insert a new record.
    IF @@ROWCOUNT = 0
        BEGIN

            INSERT INTO dbo.Directories
            (
				[Name], 
				[Description],
				IsDefault,
				CreateUser,
				EditUser,
				CreateDate,
				EditDate
            )
            VALUES
            (
				@Name, 
				@Description,
				@IsDefault,
				@UserID,
				@UserID,
				@CurrentDate,
				@CurrentDate
            );

			SET @NewDirectoryID = SCOPE_IDENTITY();

        END;

		-- If the record that has been inserted / updated is marked as the default
		-- we need to remove the flag from the other directories.
		IF @IsDefault = 1
		BEGIN
			UPDATE dbo.Directories 
			SET IsDefault = 0
			WHERE DirectoryID != ISNULL(@NewDirectoryID, @DirectoryID)
		END;
END;