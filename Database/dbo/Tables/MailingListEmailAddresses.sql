﻿CREATE TABLE [dbo].[MailingListEmailAddresses]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	EmailAddress NVARCHAR(320)
)
