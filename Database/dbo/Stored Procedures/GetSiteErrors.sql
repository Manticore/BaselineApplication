﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the set of errors that have been encountered and 
-- logged in the site.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetSiteErrors
       @Page			INT = NULL, 
       @NumberOfRecords INT = NULL
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @OFFSET INT = (@Page - 1) * @NumberOfRecords;

    DECLARE @Errors TABLE
    (
	   [ErrorID] UNIQUEIDENTIFIER, 
	   [Application] NVARCHAR(60), 
	   [Host] NVARCHAR(50), 
	   [Source] NVARCHAR(60), 
	   [Message] NVARCHAR(500), 
	   [User] NVARCHAR(50), 
	   [StatusCode] INT, 
	   [Time] DATETIME2, 
	   [Total] INT
    );

    INSERT INTO @Errors
    (
	   [ErrorID], 
	   [Application], 
	   [Host], 
	   [Source], 
	   [Message], 
	   [User], 
	   [StatusCode], 
	   [Time], 
	   [Total]
    )
    SELECT [ErrorId] AS [ErrorID], 
           [Application], 
           [Host], 
           [Source], 
           [Message], 
           [User], 
           [StatusCode], 
           [TimeUtc], 
           [Total] = COUNT(*)OVER()
    FROM dbo.ELMAH_Error
    ORDER BY TimeUtc DESC
    OFFSET @OFFSET ROWS FETCH NEXT @NumberOfRecords ROWS ONLY;

    -- Return the total records for paging.
    SELECT TOP 1 Total FROM @Errors;

    -- Return the subset of the total records.
    SELECT [Application], 
           [Host], 
           [Source], 
           [Message], 
           [User], 
           [StatusCode], 
           [Time]
    FROM @Errors;

END;