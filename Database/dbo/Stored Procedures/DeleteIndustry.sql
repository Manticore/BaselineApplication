﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure deletes a specified industry.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.DeleteIndustry
       @IndustryID INT
AS
BEGIN

    DELETE FROM dbo.Industries
    WHERE IndustryID = @IndustryID;

END;