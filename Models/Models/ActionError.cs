﻿
namespace Models.Models
{
    public class ActionError
    {
        public string Target { get; set; }
        public string ErrorDescription { get; set; }

        public ActionError(string target, string errorDescription)
        {
            Target = target;
            ErrorDescription = errorDescription;
        }
    }
}
