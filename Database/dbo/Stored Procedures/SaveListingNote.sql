﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure saves a listing note associated to a specified listing.
-------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SaveListingNote]
	@ListingID INT,
	@NoteText  NVARCHAR(2000),
	@UserID	   INT

AS
BEGIN

	-- Retrieve the current date.
    DECLARE @CurrentDate DATETIME2 = dbo.GetCurrentDate();

    INSERT INTO dbo.Notes
    (
	   ListingID,
	   NoteText,
	   CreateDate,
	   EditDate,
	   CreateUser,
	   EditUser
    )
    VALUES
    (
	   @ListingID,		-- ListingID
	   @NoteText,		-- NoteText
	   @CurrentDate,	-- CreateDate
	   @CurrentDate,	-- EditDate
	   @UserID,			-- CreateUser
	   @UserID			-- EditUser
    );

END;