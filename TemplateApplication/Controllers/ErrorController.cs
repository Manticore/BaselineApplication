﻿using System.Web.Mvc;

namespace TemplateApplication.Controllers
{
    // This Controller is used to display a generic error page when a user encounters an error.
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NotFound()
        {
            return View();
        }
    }
}
