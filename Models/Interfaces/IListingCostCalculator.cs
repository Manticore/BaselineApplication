﻿using Models.Models;

namespace Models.Interfaces
{
    public interface IListingCostCalculator
    {
        int CalculateListingCost(Listing listing);
        int CalculateDirectoryCost(int numDirectories, int numDescriptions);
    }
}
