﻿using System.Collections.Generic;
using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class PayOrderViewModel
    {
        public int Id { get; set; }
        public Order Order { get; set; }
        public IEnumerable<PaymentType> PaymentTypes { get; set; }
        public bool IsAdministrator { get; set; }
        public Payment Payment { get; set; }
        public IEnumerable<State> States { get; set; }
    }
}