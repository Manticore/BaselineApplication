﻿using System.Collections.Generic;
using Models;
using Models.Interfaces;
using Models.Models;

namespace Services
{
    public class ValidationService : IValidationService
    {
        private readonly ModelStateDictionary _modelState;

        public ValidationService()
        {
            // Initialize the model state dictionary.
            _modelState = new ModelStateDictionary();
        }

        public bool ValidateDirectory(Directory directory)
        {
            if (directory.Name == null || directory.Name.Trim().Length == 0)
                _modelState.AddError("Name", "Please provide a name for the directory.");
            if (directory.Description == null || directory.Description.Trim().Length == 0)
                _modelState.AddError("Description", "Please provide a description for the directory.");
            return _modelState.IsValid();
        }

        public bool ValidatePricingModel(PricingModel pricing)
        {
            if (pricing.CostPerDirectory == null)
                _modelState.AddError("CostPerDirectory", "Please provide a valid cost per directory.");
            if (pricing.CostPerDirectory != null && pricing.CostPerDirectory < 100)
                _modelState.AddError("CostPerDirectory-Range", "Please provide a cost per directory greater than $1.00.");
            if (pricing.CostPerDescription == null)
                _modelState.AddError("CostPerDescription", "Please provide a valid cost per description line.");
            return _modelState.IsValid();
        }

        public bool ValidateIndustry(Industry industry)
        {
            if (industry.Name == null)
                _modelState.AddError("IndustryName", "Please provide a name for the industry.");
            return _modelState.IsValid();
        }

        public Dictionary<string, string> GetErrorDictionary()
        {
            return _modelState.ModelState;
        }

        public string GetErrorString()
        {
            return string.Format("{0}", string.Join(",", _modelState.ModelState.Values));
        }
    }
}
