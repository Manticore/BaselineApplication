﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure adds the administrator role to a selected user.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.ConvertUserToAdmin
       @UserID INT
AS
BEGIN

    DECLARE @AdminRoleID INT;

	-- Retrieve the administrator role identifier.
    SELECT @AdminRoleID = RoleId
    FROM dbo.webpages_Roles
    WHERE RoleName = 'Administrator';

    INSERT INTO dbo.webpages_UsersInRoles
    (
	   UserId, 
	   RoleId
    )
    VALUES
    (
	   @UserID,	    -- UserID
	   @AdminRoleID -- RoleID
    );

END;