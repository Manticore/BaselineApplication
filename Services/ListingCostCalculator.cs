﻿using System.Linq;
using Library.Exceptions;
using Models.Interfaces;
using Models.Models;

namespace Services
{
    public class ListingCostCalculator : IListingCostCalculator
    {
        private readonly IDatabaseContext _database;

        public ListingCostCalculator(IDatabaseContext database)
        {
            this._database = database;
        }

        public int CalculateListingCost(Listing listing)
        {
            PricingModel pricingModel = _database.GetCurrentListingPricingModel();
            if (pricingModel.CostPerDirectory.HasValue && pricingModel.CostPerDescription.HasValue)
            {
                int costPerDirectory = pricingModel.CostPerDirectory.Value;
                int costPerDescription = pricingModel.CostPerDescription.Value;

                // Retrieve the number of descriptions provided.
                int numDescriptionLines = listing.DescriptionLines.Count();
                return (costPerDirectory * listing.Directories.Count()) + ((listing.Directories.Count()) * (numDescriptionLines * costPerDescription));
            }
            throw new ListingCostCalculationException("Pricing information has not been provided.");
        }

        public int CalculateDirectoryCost(int numDirectories, int numDescriptions)
        {
            PricingModel pricingModel = _database.GetCurrentListingPricingModel();
            if (pricingModel.CostPerDirectory.HasValue && pricingModel.CostPerDescription.HasValue)
            {
                return pricingModel.CostPerDirectory.Value + (pricingModel.CostPerDescription.Value * numDescriptions);
            }
            throw new ListingCostCalculationException("Pricing information has not been provided.");
        }
    }
}
