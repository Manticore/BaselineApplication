﻿using System.Web.Mvc;
using Models.Interfaces;
using Stripe;
using TemplateApplication.Filters;
using WebMatrix.WebData;

namespace TemplateApplication.Controllers
{
    [InitializeSimpleMembership]
    // Make sure all the POST methods receive a valid token.
    [ValidateAntiForgeryTokenOnAllPosts]
    public class SubscriptionsController : Controller
    {
        private readonly IListingService    _listingService;
        private readonly IListingRepository _listingRepository;

        public SubscriptionsController(IListingService listingService, IListingRepository listingRepository)
        {
            _listingService    = listingService;
            _listingRepository = listingRepository;
        }

        // THIS IS NOT CURRENTLY USED, BUT IT MAY BE USEFUL TO KEEP AROUND.
        /// <summary>
        /// This action method displays the details for a given subscription. This action will be consumed
        /// by users of the site. Therefore we only need to make sure the user is logged in.
        /// </summary>
        /// <param name="id">The Specified subscription Identifier.</param>
        /// <returns>A view with details about the specified subscription.</returns>
        [Authorize]
        public ActionResult Details(int id)
        {
            var subscription = _listingRepository.GetSubscription(id);
            return View(subscription);
        }

        /// <summary>
        /// This action allows an administrative user to cancel the subscription for a listing.
        /// </summary>
        /// <param name="id">The specified subscription identifier.</param>
        /// <returns>The view to cancel a specified subscription.</returns>
        [Authorize(Roles = "Administrator")]
        public ActionResult Cancel(int id)
        {
            var subscription = _listingRepository.GetSubscription(id);
            // Make sure the listing is not already cancelled.
            if (!subscription.CancelledDate.HasValue)
            {
                ViewBag.ReturnURL = Request.UrlReferrer;
                return View(subscription);
            }
            if (Request.UrlReferrer != null)
                return Redirect(Request.UrlReferrer.ToString());
            return RedirectToAction("", "");
        }

        [Authorize]
        public ActionResult Reactivate(int id)
        {
            var isAdministrator = User.IsInRole("Administrator");
            int currentUserID = WebSecurity.GetUserId(User.Identity.Name);
            if (isAdministrator || _listingRepository.IsUserSubscriptionOwner(id, currentUserID))
            {
                ViewBag.Layout = isAdministrator ? "~/Views/Shared/_AdminLayout.cshtml" : "~/Views/Shared/_Layout.cshtml";
                var subscription = _listingRepository.GetSubscription(id);

                // Make sure the subscription is actually cancelled.
                if (subscription.CancelledDate.HasValue)
                {
                    ViewBag.ReturnURL = Request.UrlReferrer;
                    return View(subscription);
                }          
            }
            // If we got here there was an issue. Redirect the user.
            return RedirectToAction("", "");
        }

        #region POST Methods

        // This POST method is used to Cancel a subscription for a listing.
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public JsonResult CancelSubscription(int listingIdentifier)
        {
            try
            {
                _listingService.CancelSubscription(listingIdentifier);
            }
            catch (StripeException)
            {
                return Json("An error occured when canceling the subscription for this listing. Please try again later.");
            }
            return Json("Subscription cancelled successfully.");
        }

        // This POST method is used to Reactivate a subscription that was cancelled.
        [HttpPost]
        public JsonResult ReactivateSubscription(int listingIdentifier)
        {
            var isAdministrator = User.IsInRole("Administrator");
            int currentUserID = WebSecurity.GetUserId(User.Identity.Name);
            // Only allow an admin user or the owner of the subscription to reactivate it.
            if (isAdministrator || _listingRepository.IsUserListingOwner(listingIdentifier, currentUserID))
            {
                try
                {
                    _listingService.ReactivateSubscription(listingIdentifier);
                }
                catch (StripeException)
                {
                    return Json("There was an issue reactivating your subscription. Please try again later.");
                }
            }
            return Json("Subscription reactivated successfully.");
        }

        // This POST method returns a detail view for a designated subscription.
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult GetSubscriptionDetailsHTML(int subscriptionIdentifier)
        {
            var subscription = _listingRepository.GetSubscription(subscriptionIdentifier);
            return PartialView("_SubscriptionDetail", subscription);
        }

        #endregion
    }
}