﻿using System;
using System.Collections.Generic;
using Models;
using Models.Interfaces;
using Models.Models;

namespace Repositories.Repositories
{
    public class ListingRepository : IListingRepository
    {
        private readonly IDatabaseContext _databaseAccess;

        public ListingRepository(IDatabaseContext databaseAccess)
        {
            _databaseAccess = databaseAccess;
        }

        public IEnumerable<Listing> GetListings(int page, int numberOfRecords, string keyword, int? directoryID, List<int?> industryIDs, int? stateID)
        {
            return _databaseAccess.GetListings(page, numberOfRecords, keyword, directoryID, industryIDs, stateID);
        }

        public Listing GetSpecificListing(int? listingID)
        {
            return _databaseAccess.GetSpecificListing(listingID);
        }

        public int? UpsertListing(Listing listing, int userID)
        {
            return _databaseAccess.UpsertListing(listing, userID);
        }

        public void DeleteListing(int listingID)
        {
            _databaseAccess.DeleteListing(listingID);
        }

        public PagedList<BillableListingsModel> GetBillableListings(BillableListingParameterSet parameters)
        {
            return _databaseAccess.GetBillableListings(parameters);
        }

        public PagedList<BulkListingsReportModel> GetBulkListings(BulkListingParameterSet parameters)
        {
            return _databaseAccess.GetBulkListings(parameters);
        }

        public bool IsUserListingOwner(int? listingID, int userID)
        {
            return _databaseAccess.IsUserListingOwner(listingID, userID);
        }

        public IEnumerable<Listing> GetUserListings(int userID)
        {
            return _databaseAccess.GetUserListings(userID);
        }

        public int? CreateSubscription(Subscription subscription, int userID)
        {
            int? subscriptionID = _databaseAccess.CreateSubscription(subscription, userID);
            return subscriptionID;
        }

        public Listing GetSpecificListingFull(int? listingID)
        {
            return _databaseAccess.GetSpecificListingFull(listingID);
        }

        public void CancelSubscription(int subscriptionID)
        {
            _databaseAccess.CancelSubscription(subscriptionID);
        }

        public void ReactivateSubscription(int subscriptionID)
        {
            _databaseAccess.ReactivateSubscription(subscriptionID);
        }

        public Subscription GetSubscription(int subscriptionIdentifier)
        {
            return _databaseAccess.GetSubscription(subscriptionIdentifier);
        }

        public PagedList<ManagedListingsModel> GetManagedListings(ManagedListingsReportParameters parameters)
        {
            return _databaseAccess.GetManagedListings(parameters);
        }

        public string GetDefaultPlanID()
        {
            return _databaseAccess.GetDefaultPlanID();
        }

        public bool DoesUnpaidOrderExist(int listingID)
        {
            return _databaseAccess.DoesUnpaidOrderExist(listingID);
        }

        public void UpdateLocalSubscription(int subscriptionID, int directoryCount, int descriptionLineCount, int? newSubscriptionCost, string stripeSubscriptionID, int userID)
        {
            _databaseAccess.UpdateLocalSubscription(subscriptionID, directoryCount, descriptionLineCount, newSubscriptionCost, stripeSubscriptionID, userID);
        }

        public IEnumerable<ListingType> GetListingTypes()
        {
            return _databaseAccess.GetListingTypes();
        }

        public Subscription GetCurrentSubscription(int listingIdentifier)
        {
            return _databaseAccess.GetCurrentSubscription(listingIdentifier);
        }

        public int? GetListingOwnerUserID(int listingIdentifier)
        {
            return _databaseAccess.GetListingOwnerUserID(listingIdentifier);
        }

        public void SaveListingNote(Note note, int userIdentifier)
        {
            _databaseAccess.SaveListingNote(note, userIdentifier);
        }

        public IEnumerable<Note> GetListingNotes(int listingIdentifier)
        {
            return _databaseAccess.GetListingNotes(listingIdentifier);
        }

        public void UpdateListingType(int listingIdentifier, string listingType)
        {
            _databaseAccess.UpdateListingType(listingIdentifier, listingType);
        }

        public bool IsUserSubscriptionOwner(int subscriptionIdentifier, int userIdentifier)
        {
            return _databaseAccess.IsUserSubscriptionOwner(subscriptionIdentifier, userIdentifier);
        }

        public bool DoesSubscriptionExist(string stripeSubscriptionID, DateTime subscriptionStart, DateTime subscriptionEnd)
        {
            return _databaseAccess.DoesSubscriptionExist(stripeSubscriptionID, subscriptionStart, subscriptionEnd);
        }

        public void ClearCancelledFlag(int listingIdentifier)
        {
            _databaseAccess.ClearCancelledFlag(listingIdentifier);
        }

        public void CancelListing(int listingIdentifier)
        {
            _databaseAccess.CancelListing(listingIdentifier);
        }
    }
}