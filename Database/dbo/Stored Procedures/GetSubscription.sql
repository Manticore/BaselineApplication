﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns a local subscription record given a subscription identifier.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetSubscription
       @SubscriptionID INT
AS
BEGIN
    
    -- Retrieve the subscription record.
    SELECT SubscriptionID, 
           ListingID, 
           OrderID, 
           StripeSubscriptionID,
		   CurrentDirectoryCount,
		   CurrentDescriptionLinesCount,
		   PaidDirectoryCount, 
           PaidDescriptionLinesCount,
           StartDate, 
           EndDate, 
           CancelledDate, 
           SubscriptionCost,
		   MaxSubscriptionCost, 
		   SubscriptionStatusID,
           CreateDate, 
           EditDate, 
           CreateUser, 
           EditUser
    FROM dbo.Subscriptions
    WHERE SubscriptionID = @SubscriptionID;

END;