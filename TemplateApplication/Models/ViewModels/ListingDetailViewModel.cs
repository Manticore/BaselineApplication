﻿using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class ListingDetailViewModel
    {
        public string Message { get; set; }
        public Listing Listing { get; set; }
    }
}