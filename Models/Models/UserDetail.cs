﻿using System;
using System.Globalization;

namespace Models.Models
{
    public class UserDetail
    {
        public string UserName { get; set; }
        public DateTime? LastLoginDate { get; set; }

        public string LastLoginDateDisplay
        {
            get
            {
                if (LastLoginDate.HasValue)
                {
                    return LastLoginDate.Value.ToString(CultureInfo.InvariantCulture);
                }
                return "User has never logged in.";
            }
        }
    }
}
