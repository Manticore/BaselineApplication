﻿using System;

namespace Models
{
    public class BulkListingsReportModel
    {
        public int ListingID { get; set; }
        public string BusinessName { get; set; }
        public string FaxNumber { get; set; }
        public DateTime ListingCreateDate { get; set; }

        public string ListingCreateDateDisplay
        {
            get { return ListingCreateDate.ToShortDateString(); }
        }
    }
}
