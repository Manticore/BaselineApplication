﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure creates a subscription record for a listing given a few parameters.
-- The IsManual flag determines whether the subscription should be activated when created.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.CreateSubscription
      @ListingID				 INT,
	  @OrderID					 INT,
      @StripeSubscriptionID		 NVARCHAR(50),
	  @PaidDirectoryCount		 INT,
	  @PaidDescriptionLinesCount INT,
	  @SubscriptionCost			 INT,
	  @StartDate				 DATETIME2,
	  @EndDate					 DATETIME2,
      @UserID					 INT
AS
BEGIN
	
    SET NOCOUNT ON;

    -- Grab the current date setting to be used in the stored procedure.
    DECLARE @CurrentDate DATETIME = dbo.GetCurrentDate();
    
    -- Retrieve the status identifier for the 'New' status.
    DECLARE @SubscriptionStatusID_NEW INT;
    SELECT @SubscriptionStatusID_NEW = SubscriptionStatusID 
    FROM dbo.SubscriptionStatuses 
	WHERE SubscriptionStatus = 'New';

    INSERT INTO dbo.Subscriptions
    (
	   ListingID,
	   OrderID,
	   StripeSubscriptionID,
	   CurrentDirectoryCount,
	   PaidDirectoryCount,
	   CurrentDescriptionLinesCount,
	   PaidDescriptionLinesCount,
	   StartDate,
	   EndDate,
	   SubscriptionCost,
	   MaxSubscriptionCost,
	   CreateDate,
	   EditDate,
	   CreateUser,
	   EditUser,
	   SubscriptionStatusID
    )
    VALUES
    (
	   @ListingID,
	   @OrderID,
	   @StripeSubscriptionID,
	   @PaidDirectoryCount,
	   @PaidDirectoryCount,
	   @PaidDescriptionLinesCount,
	   @PaidDescriptionLinesCount,
	   @StartDate,
	   @EndDate,
	   @SubscriptionCost,
	   @SubscriptionCost,
	   @CurrentDate,  
	   @CurrentDate,	   
	   @UserID,
	   @UserID,
	   @SubscriptionStatusID_NEW
    );

	-- Return the newly created subscription identifier.
	DECLARE @SubscriptionID INT;
	SET @SubscriptionID = SCOPE_IDENTITY();
	SELECT @SubscriptionID

END; 