﻿
namespace Models.Models
{
    public class State
    {
        public int StateID { get; set; }
        public string  Abbreviation { get; set; }
        public string Name { get; set; }
    }
}
