﻿using System.Collections.Generic;
using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class DirectoryConfigurationViewModel
    {
        public IEnumerable<Directory> Directories { get; set; }
        public bool DoesDefaultDirectoryExist { get; set; }
    }
}