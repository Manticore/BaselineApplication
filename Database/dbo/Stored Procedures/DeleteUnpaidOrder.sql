﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure is used to delete an order that is currently unpaid. The
-- invoice dates and line items are deleted along with the order.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.DeleteUnpaidOrder
    @OrderID INT
AS 
BEGIN

	SET NOCOUNT ON;

	BEGIN TRY
	    BEGIN TRANSACTION;
    
		-- Retrieve the unpaid order status identifier.
		DECLARE @UnpaidOrderStatusID INT;
		SELECT @UnpaidOrderStatusID = OrderStatusID 
		FROM dbo.OrderStatuses 
		WHERE [Status] = 'Unpaid'
    
		-- If an order exists with the specified order identifier and is unpaid.
		IF EXISTS(SELECT 1 FROM dbo.Orders WHERE OrderID = @OrderID AND Orders.OrderStatusID = @UnpaidOrderStatusID)
		   BEGIN
			  -- Delete the invoice dates for the order.
			  DELETE FROM dbo.InvoiceDates
			  WHERE OrderID = @OrderID;

			  -- Delete the order line items.
			  DELETE FROM OrderLineItems 
			  WHERE OrderID = @OrderID;
    	   
			  -- Delete the order.
			  DELETE FROM dbo.Orders 
			  WHERE OrderID = @OrderID
		   END;
	   
	   COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		-- If an error occured, rollback the transaction and return error.
		DECLARE @ErrorMessage  NVARCHAR(4000) = ERROR_MESSAGE();
		DECLARE @ErrorSeverity INT			  = ERROR_SEVERITY();
		DECLARE @ErrorState    INT			  = ERROR_STATE();

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

		ROLLBACK TRANSACTION;

	END CATCH;

END;