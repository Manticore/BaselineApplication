﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure retrieves order records from the database.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetOrders
       @Page		    INT, 
       @NumberOfRecords INT, 
       @OrderNumber		BIGINT		  = NULL, 
       @ListingID	    INT			  = NULL,
	   @BusinessName	NVARCHAR(250) = NULL,
       @StartDate	    DATETIME2	  = NULL, 
       @EndDate			DATETIME2	  = NULL,
       @OrderStatusID   INT			  = NULL,
	   @SortColumn		NVARCHAR(50)  = NULL,
	   @SortDirection	NVARCHAR(10)  = NULL
AS
BEGIN

    SET NOCOUNT ON;
    
    -- Calculate the paging offset.
    DECLARE @OFFSET INT = (@Page - 1) * @NumberOfRecords;
    
    DECLARE @Orders TABLE 
	(
	   OrderID		  INT, 
	   ListingID	  INT,
	   BusinessName   NVARCHAR(250),
	   OrderNumber	  BIGINT,
	   OrderTotal	  INT,
	   OrderStatusID  INT,
	   [Status]		  NVARCHAR(50),
	   UserID	      INT, 
	   UserName       NVARCHAR(56), 
	   CreateDate     DATETIME2, 
	   Total	      INT
    );  
	
    INSERT INTO @Orders
    (
	    OrderID, 
        ListingID,
		BusinessName,
		OrderNumber,
        OrderTotal,
        OrderStatusID,
        [Status],
        CreateDate, 
        Total
    )
    SELECT OrderID, 
           O.ListingID,
		   L.BusinessName,
		   OrderNumber,
           OrderTotal,
           OS.OrderStatusID,
           OS.Status,
           O.CreateDate, 
           Total = COUNT(*) OVER()
    FROM dbo.Orders O 
	   INNER JOIN dbo.OrderStatuses OS 
		  ON OS.OrderStatusID = O.OrderStatusID
	   INNER JOIN dbo.Listings L
		  ON L.ListingID = O.ListingID
    WHERE(@ListingID IS NULL OR O.ListingID = @ListingID) 
		AND (@BusinessName IS NULL OR L.BusinessName LIKE '%' + @BusinessName +'%')
	    AND (@OrderNumber IS NULL OR O.OrderNumber = @OrderNumber) 
	    AND (@StartDate IS NULL OR O.CreateDate >= @StartDate) 
	    AND (@EndDate IS NULL OR O.CreateDate <= @EndDate)
	    AND (@OrderStatusID IS NULL OR O.OrderStatusID = @OrderStatusID)
	ORDER BY 
		CASE WHEN @SortColumn = 'OrderNumber' AND @SortDirection = 'Ascendant'
			  THEN O.OrderNumber END ASC, 
		CASE WHEN @SortColumn = 'OrderNumber' AND @SortDirection = 'Descendant' 
			  THEN O.OrderNumber END DESC,
		CASE WHEN @SortColumn = 'ListingID' AND @SortDirection = 'Ascendant'
			  THEN O.ListingID END ASC, 
		CASE WHEN @SortColumn = 'ListingID' AND @SortDirection = 'Descendant' 
			  THEN O.ListingID END DESC,
		CASE WHEN @SortColumn = 'BusinessName' AND @SortDirection = 'Ascendant'
			  THEN L.BusinessName END ASC, 
		CASE WHEN @SortColumn = 'BusinessName' AND @SortDirection = 'Descendant' 
			  THEN L.BusinessName END DESC,
		CASE WHEN @SortColumn = 'CreateDateDisplay' AND @SortDirection = 'Ascendant'
			  THEN O.CreateDate END ASC, 
		CASE WHEN ((@SortColumn IS NULL) OR (@SortColumn = 'CreateDateDisplay' AND @SortDirection = 'Descendant'))
			  THEN O.CreateDate END DESC,
		CASE WHEN @SortColumn = 'OrderTotalDisplay' AND @SortDirection = 'Ascendant'
			  THEN O.OrderTotal END ASC, 
		CASE WHEN @SortColumn = 'OrderTotalDisplay' AND @SortDirection = 'Descendant'
			  THEN O.OrderTotal END DESC,
		CASE WHEN @SortColumn = 'Status' AND @SortDirection = 'Ascendant'
			  THEN OS.[Status] END ASC, 
		CASE WHEN @SortColumn = 'Status' AND @SortDirection = 'Descendant'
			  THEN OS.[Status] END DESC
    OFFSET @OFFSET ROWS FETCH NEXT @NUMBEROFRECORDS ROWS ONLY;

    -- Return the total records for paging.
    SELECT TOP 1 Total FROM @Orders;

	-- Return the order objects.
    SELECT OrderID, 
           ListingID,
		   BusinessName,
		   OrderNumber,
           OrderTotal, 
		   OrderStatusID,
		   [Status],
           CreateDate
    FROM @Orders;
    
	-- Retrieve the payments for the orders, if they exist.
	SELECT PaymentID, 
		   PaymentDate, 
		   PaymentDetails, 
		   PaymentTypeID, 
		   OrderID, 
		   CreateDate, 
		   EditDate, 
		   CreateUser, 
		   EditUser
	FROM dbo.Payments
	WHERE OrderID IN (SELECT OrderID FROM @Orders);

END; 
