﻿using System;

namespace Models.Models
{
    public class Subscription
    {
        public int SubscriptionID { get; set; }
        public int ListingID { get; set; }
        public int OrderID { get; set; }
        public string StripeSubscriptionID { get; set; }
        public int CurrentDirectoryCount { get; set; }
        public int CurrentDescriptionLinesCount { get; set; }
        public int PaidDirectoryCount { get; set; }
        public int PaidDescriptionLinesCount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? CancelledDate { get; set; }
        public int SubscriptionCost { get; set; }
        public int MaxSubscriptionCost { get; set; }
        public int SubscriptionStatusID { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateUser { get; set; }
        public int EditUser { get; set; }

        public string StartDateDisplay
        {
            get
            {
                return StartDate.ToShortDateString();
            }
        }

        public string EndDateDisplay
        {
            get
            {
                return EndDate.ToShortDateString();
            }
        }

        public string RenewalDateDisplay
        {
            get
            {
                return EndDate.ToShortDateString();
            }
        }

        public string SubscriptionCostDisplay
        {
            get
            {
                return string.Format("${0:0.00}", (SubscriptionCost * 0.01));
            }
        }
    }
}
