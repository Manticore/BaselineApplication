﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure retrieves the user identifier from the listing that the 
-- order is associated with. This procedure is mainly used to stop a user from
-- paying or modifying another user's order.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetOrderOwnerUserID
       @OrderID INT
AS
BEGIN

	SET NOCOUNT ON;

    DECLARE @ListingID INT;

    SELECT @ListingID = ListingID
    FROM Orders
    WHERE OrderID = @OrderID;

    SELECT UserID
    FROM dbo.Listings
    WHERE ListingID = @ListingID;

END;
