﻿
-------------------------------------------------------------------------------
-- Scalar-Valued Function Description						  
-------------------------------------------------------------------------------
-- This scalar valued function returns a boolean value indicating if the order 
-- number specified has been generated previously.
-------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[DoesOrderNumberExist] 
( 
    @OrderNumber BIGINT
)
RETURNS BIT
AS
BEGIN

	DECLARE @OrderExists BIT = 0;

    IF EXISTS(SELECT 1 FROM dbo.Orders WHERE OrderNumber = @OrderNumber)
	   SET @OrderExists = 1;
	   
	RETURN @OrderExists;

END