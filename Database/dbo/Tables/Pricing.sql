﻿CREATE TABLE dbo.Pricing
(
	PricingID INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	CostPerDirectory INT NOT NULL,
	CostPerDescription INT NOT NULL,
	CreateDate DATETIME2 NOT NULL,
	CreateUser INT NOT NULL
);