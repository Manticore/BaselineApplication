﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns details for a specified user in the system.
-------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GetUserDetails]
	@UserID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT UserName, 
		   AD.LastLoginDate
	FROM dbo.UserProfile UP
	LEFT JOIN dbo.AccessDates AD 
		ON AD.UserID = Up.UserId
	WHERE UP.UserId = @UserID;

END;