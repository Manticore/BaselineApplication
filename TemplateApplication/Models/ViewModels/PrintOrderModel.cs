﻿using Models;
using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class PrintOrderModel
    {
        public PagedList<PrintOrderReportModel> OrdersForPrinting { get; set; }
    }
}