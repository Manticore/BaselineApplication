﻿using Library;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Models.Models
{
    public class TradeLead
    {
        public int? TradeLeadID { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }

        public string BodyPreview
        {
            get { return Body.TruncateAtWord(200); }
        }

        public List<Industry> IndustryList { get; set; }

        public string[] SelectedIndustryIDs
        {
            get
            {
                if (IndustryList != null)
                {
                    return IndustryList.Select(x => x.IndustryID.ToString()).ToArray();
                }
                return null;
            }
        }

        public DateTime CreateDate { get; set; }

        public string CreateDateDisplay
        {
            get { return CreateDate.ToShortDateString(); }
        }

        public DateTime EditDate { get; set; }
        public int CreateUser { get; set; }
        public string CreateUserName { get; set; }
        public int EditUser { get; set; }
        public string EditUserName { get; set; }

        public string CreateUserNameDisplay
        {
            get { return CreateUserName.ToUpper(); }
        }
    }
}