﻿using System;
using System.Collections.Generic;
using Models.Models;

namespace Models.Interfaces
{
    public interface ITradeLeadRepository
    {
        TradeLead GetSpecificTradeLead(int tradeLeadID);
        IEnumerable<TradeLead> GetTradeLeads(int page, int numberOfRecords, string keyword, List<int?> industryIDs, DateTime? starTime, DateTime? end);
        PagedList<TradeLead> GetTradeLeadsPaged(TradeLeadParameterSet parameters);
        void SaveTradeLeadView(int tradeLeadID);
        IEnumerable<TradeLead> GetTopTradeLeads();
        void UpsertTradeLead(TradeLead tradeLead, int userID);
        void DeleteTradeLead(int tradeLeadID);
    }
}