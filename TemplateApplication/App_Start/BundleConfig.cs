﻿using System.Web.Optimization;

namespace TemplateApplication
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            // THEME SPECIFIC STYLING -- ADMIN 4 THEME
            // ----------------------------------------------
            bundles.Add(new StyleBundle("~/Content/custom/Admin4CustomStyling.css")
                               .Include("~/Content/custom/Admin4CustomStyling.css"));

            bundles.Add(new StyleBundle("~/Content/assets/admin/layout4/css/Admin4Layout")
                               .Include("~/Content/assets/admin/layout4/css/layout.css")
                               .Include("~/Content/assets/admin/layout4/css/default.css")
                               .Include("~/Content/assets/admin/layout4/css/custom.css"));

            bundles.Add(new ScriptBundle("~/Content/assets/admin/layout4/scripts/Layout")
                               .Include("~/Content/assets/admin/layout4/scripts/layout.js"));

            bundles.Add(new StyleBundle("~/Content/custom/Admin3CustomStyling.css")
                               .Include("~/Content/custom/Admin3CustomStyling.css"));

            bundles.Add(new StyleBundle("~/Content/assets/admin/layout3/css/Admin3Layout")
                               .Include("~/Content/assets/admin/layout3/css/layout.css")
                               .Include("~/Content/assets/admin/layout3/css/themes/blue-steel.css")
                               .Include("~/Content/assets/admin/layout3/css/custom.css"));

            bundles.Add(new ScriptBundle("~/Content/assets/admin/layout3/scripts/Layout")
                                .Include("~/Content/assets/admin/layout3/scripts/layout.js"));

            // GLOBAL SCRIPTS
            bundles.Add(new ScriptBundle("~/Content/assets/global/plugins/GlobalScripts")
                                .Include("~/Content/assets/global/plugins/jquery.js")              // Added to the assets folder.
                                .Include("~/Content/assets/global/plugins/jquery-migrate.js")
                                .Include("~/Content/assets/global/plugins/jquery-ui/jquery-ui.js") // Added to the assets folder.
                                .Include("~/Content/assets/global/plugins/bootstrap/js/bootstrap.js")
                                .Include("~/Content/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js")
                                .Include("~/Content/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.js")
                                .Include("~/Content/assets/global/plugins/jquery.blockui.js")      // Added to the assets folder.
                                .Include("~/Content/assets/global/plugins/jquery.cokie.js")        // Added to the assets folder.
                                .Include("~/Content/assets/global/plugins/uniform/jquery.uniform.js")
                                .Include("~/Scripts/global/SerializeForm.js")
                                .Include("~/Scripts/global/AjaxMethods.js")
                                .Include("~/Content/assets/global/scripts/metronic.js")
                                .Include("~/Content/assets/global/plugins/bootbox/bootbox.min.js"));

            // GLOBAL STYLES
            bundles.Add(new StyleBundle("~/Content/assets/global/plugins/font-awesome/css/FontAwesome")
                               .Include("~/Content/assets/global/plugins/font-awesome/css/font-awesome.css"));

            bundles.Add(new StyleBundle("~/Content/assets/global/plugins/simple-line-icons/SimpleLineIcons")
                               .Include("~/Content/assets/global/plugins/simple-line-icons/simple-line-icons.css"));

            //bundles.Add(new StyleBundle("~/Content/assets/global/plugins/bootstrap/css/Bootstrap")
            //                   .Include("~/Content/assets/global/plugins/bootstrap/css/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/assets/global/plugins/uniform/css/Uniform")
                               .Include("~/Content/assets/global/plugins/uniform/css/uniform.default.css"));

            bundles.Add(new StyleBundle("~/Content/custom/GlobalCustomStyling")
                               .Include("~/Content/custom/GlobalCustomStyling.css"));

            // GLOBAL THEME STYLES
            bundles.Add(new StyleBundle("~/Content/assets/global/css/GlobalComponentThemeLayout")
                               .Include("~/Content/assets/global/css/components-md.css")
                               .Include("~/Content/assets/global/css/plugins-md.css"));

            // PAGE LEVEL BUNDLES BEGIN:
            bundles.Add(new StyleBundle("~/Content/assets/admin/pages/css/TradeLeadDetails")
                               .Include("~/Content/assets/admin/pages/css/news.css")
                               .Include("~/Content/assets/admin/pages/css/blog.css"));


            // Component Bundles
            // Datatable Bundle
            // ------------------------------------------------------
            bundles.Add(new StyleBundle("~/Content/assets/global/plugins/datatables/plugins/bootstrap/DataTablesStyling")
                               .Include("~/Content/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"));

            bundles.Add(new ScriptBundle("~/DatatableScripts")
                                .Include("~/Content/assets/global/plugins/datatables/media/js/jquery.dataTables.js")
                                .Include("~/Content/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"));

            bundles.Add(new ScriptBundle("~/jQueryValidation")
                                .Include("~/Content/assets/global/plugins/jquery-validation/js/jquery.validate.js")
                                .Include("~/Content/assets/global/plugins/jquery-validation/js/additional-methods.js"));

            bundles.Add(new StyleBundle("~/Content/assets/global/plugins/bootstrap-select/BootstrapSelectStyling")
                                .Include("~/Content/assets/global/plugins/bootstrap-select/bootstrap-select.min.css"));

            bundles.Add(new ScriptBundle("~/BootstrapSelectScripts")
                                .Include("~/Content/assets/global/plugins/bootstrap-select/bootstrap-select.min.js"));

           //BundleTable.EnableOptimizations = true;
        }
    }
}