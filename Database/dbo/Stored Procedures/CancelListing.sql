﻿/*
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
||| This procedure is used to mark a business listing as cancelled in the	 |||
||| database. An admin may want to cancel a listing if a user decides they	 |||
||| don't want to receive ANY communications anymore. We can then filter	 |||
||| out businesses that have a cancelled flag when creating a solicitation   |||
||| list.																	 |||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
*/

CREATE PROCEDURE dbo.CancelListing
       @ListingID INT
AS
BEGIN
    SET NOCOUNT ON;

    -- Retrieve the current date.
    DECLARE @CurrentDate DATETIME = dbo.GetCurrentDate();

    UPDATE dbo.Listings
           SET [CancelledDate] = @CurrentDate
    WHERE [ListingID] = @ListingID;

END;
