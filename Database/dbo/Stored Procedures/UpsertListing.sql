﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure saves a given listing record.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.UpsertListing 
	@ListingID				 INT = NULL,
	@BusinessName			 NVARCHAR(250),
	@BusinessContact		 NVARCHAR(250),
	@ContactNumber			 NVARCHAR(15),
	@FaxNumber				 NVARCHAR(15),
	@ContactEmail			 NVARCHAR(320),
	@Description			 NVARCHAR(MAX),
	@ProductDescription		 NVARCHAR(MAX),
	@WebsiteURL				 NVARCHAR(100),
	@Street1				 NVARCHAR(150),
	@Street2				 NVARCHAR(150),
	@City					 NVARCHAR(50),
	@StateID				 INT,
	@Zip					 NVARCHAR(10),
	@CountryID				 INT,
	@SelectedIndustryIDs	 IndustryIdentifierTable_UDT  READONLY,
	@SelectedDirectoryIDs	 DirectoryIdentifierTable_UDT READONLY,
	@ListingDescriptionLines DescriptionLinesTable_UDT    READONLY,
	@UserID					 INT
AS
BEGIN

	SET NOCOUNT ON;

	BEGIN TRY
	    BEGIN TRANSACTION;

		-- Grab the current date setting to be used in the stored procedure.
		DECLARE @CurrentDate DATETIME = dbo.GetCurrentDate();
		DECLARE @NewListingID INT = NULL;
		DECLARE @NewAddressID INT = NULL;

		DECLARE @ListingTypeID INT
		SELECT @ListingTypeID = ListingTypeID 
		FROM ListingTypes WHERE Type = 'Online';

		UPDATE dbo.Listings
		SET [BusinessName]		 = @BusinessName,
			[BusinessContact]	 = @BusinessContact,
			[ContactNumber]		 = @ContactNumber,
			[FaxNumber]			 = @FaxNumber,
			[ContactEmail]		 = @ContactEmail,
			[Description]		 = @Description,
			[ProductDescription] = @ProductDescription,
			[Website]			 = @WebsiteURL,
			-- Do not update the user ID when updating.
			-- This is because an administrator can make changes as well.
			-- UserID = @UserID,
			[EditDate]			 = @CurrentDate,
			[EditUser]			 = @UserID
		WHERE ListingID			 = @ListingID;

		-- The lisitng exists in the database.
		IF @@ROWCOUNT > 0
		BEGIN
			-- Remove the previous Industries.
			DELETE FROM dbo.Listings_Industries
			WHERE ListingID = @ListingID;

			-- Insert the newly selected industries.
			INSERT INTO dbo.Listings_Industries 
			(
				ListingID,
				IndustryID
			)
			SELECT @ListingID,
				   IndustryID
			FROM @SelectedIndustryIDs;

			-- Insert the newly selected directories.
			DELETE FROM Listings_Directories 
			WHERE ListingID = @ListingID

			INSERT INTO dbo.Listings_Directories
			(
				ListingID,
				DirectoryID
			)
			SELECT @ListingID, 
				  DirectoryID 
			FROM @SelectedDirectoryIDs

			-- Insert the new description lines.
			DELETE
			FROM dbo.Descriptions
			WHERE ListingID = @ListingID;

			INSERT INTO dbo.Descriptions 
			(
				Code,
				Body,
				ListingID,
				CreateUser,
				EditUser
			)
			SELECT [DescriptionCode],
				   [Description],
				   @ListingID,
				   @UserID,
				   @UserID
			FROM @ListingDescriptionLines;

			-- Update the address.
			DECLARE @AddressID INT;

			SELECT @AddressID = AddressID
			FROM dbo.Listings_Addresses
			WHERE ListingID = @ListingID;

			IF @AddressID IS NOT NULL
			BEGIN
				UPDATE dbo.Addresses
				SET Street1   = @Street1,
					Street2   = @Street2,
					City	  = @City,
					StateID   = @StateID,
					Zip		  = @Zip,
					CountryID = @CountryID
				WHERE AddressID = @AddressID;
			END;
			ELSE
			BEGIN
				INSERT INTO Addresses 
				(
					Street1,
					Street2,
					City,
					StateID,
					Zip,
					CountryID
				)
				VALUES 
				(
					@Street1,
					@Street2,
					@City,
					@StateID,
					@Zip,
					@CountryID
				);

				SET @NewAddressID = SCOPE_IDENTITY();

				INSERT INTO dbo.Listings_Addresses 
				(
					ListingID,
					AddressID
				)
				VALUES 
				(
					@ListingID,
					@NewAddressID
				);
			END;

			-- Return the ID of the listing that was updated.
			SELECT @ListingID;
		END;
		ELSE
			BEGIN
				INSERT INTO dbo.Listings 
				(
					BusinessName,
					BusinessContact,
					ContactNumber,
					FaxNumber,
					ContactEmail,
					Description,
					ProductDescription,
					Website,
					UserID,
					ListingTypeID,
					CreateDate,
					CreateUser,
					EditDate,
					EditUser
				)
				VALUES 
				(
					@BusinessName,
					@BusinessContact,
					@ContactNumber,
					@FaxNumber,
					@ContactEmail,
					@Description,
					@ProductDescription,
					@WebsiteURL,
					@UserID,
					@ListingTypeID,
					@CurrentDate,
					@UserID,
					@CurrentDate,
					@UserID
				);

				SET @NewListingID = SCOPE_IDENTITY();

				-- Insert the selected industry IDs.
				INSERT INTO dbo.Listings_Industries 
				(
					ListingID,
					IndustryID
				)
				SELECT @NewListingID,
					   IndustryID
				FROM @SelectedIndustryIDs;

				INSERT INTO Listings_Directories
				(
					ListingID,
					DirectoryID
				)
				SELECT @NewListingID, DirectoryID 
				FROM @SelectedDirectoryIDs

				-- Insert the Description lines.
				INSERT INTO dbo.Descriptions 
				(
					Code,
					Body,
					ListingID,
					CreateUser,
					EditUser
				)
				SELECT [DescriptionCode],
					   [Description],
					   @NewListingID,
					   @UserID,
					   @UserID
				FROM @ListingDescriptionLines;

				-- Insert the Address.
				INSERT INTO dbo.Addresses 
				(
					Street1,
					Street2,
					City,
					StateID,
					Zip,
					CountryID
				)
				VALUES
				(
					@Street1,
					@Street2,
					@City,
					@StateID,
					@Zip,
					@CountryID
				);

				SET @NewAddressID = SCOPE_IDENTITY();

				INSERT INTO dbo.Listings_Addresses 
				(
					ListingID,
					AddressID
				)
				VALUES 
				(
					@NewListingID,
					@NewAddressID
				);

				-- Return the newly created listing ID.
				SELECT @NewListingID;
			END;


		COMMIT TRANSACTION;
		END TRY
		BEGIN CATCH

			DECLARE @ErrorMessage  NVARCHAR(4000) = ERROR_MESSAGE();
			DECLARE @ErrorSeverity INT			  = ERROR_SEVERITY();
			DECLARE @ErrorState    INT			  = ERROR_STATE();

			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

			ROLLBACK TRANSACTION;

		END CATCH;
END;