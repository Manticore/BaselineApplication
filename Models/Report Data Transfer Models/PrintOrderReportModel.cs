﻿using System;

namespace Models
{
    public class PrintOrderReportModel
    {
        public int OrderID { get; set; }
        public long OrderNumber { get; set; }
        public int ListingID { get; set; }
        public string BusinessName { get; set; }
        public int OrderTotal { get; set; }
        public int OrderStatusID { get; set; }
        public string Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? LastPrintedDate { get; set; }
        public string IsPrinted { get; set; }

        public string CreateDateDisplay
        {
            get { return CreateDate.ToShortDateString(); }
        }

        public string OrderTotalDisplay
        {
            get { return string.Format("${0:0.00}", (OrderTotal * 0.01)); }
        }

        public string LastPrintedDateDisplay
        {
            get { return LastPrintedDate.ToString(); }
        }
    }
}
