﻿
using System;

namespace Models.Models
{
    public class PricingModel
    {
        public int? PricingID { get; set; }
        public int? CostPerDirectory { get; set; }
        public int? CostPerDescription { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? CreateUser { get; set; }
        public string CreateUserName { get; set; }

        public string CostPerDirectoryDisplay
        {
            get { return string.Format("${0:0.00}", (CostPerDirectory * 0.01)); }
        }

        public string CostPerDescriptionDisplay
        {
            get { return string.Format("${0:0.00}", (CostPerDescription * 0.01)); }
        }

        public string CreateUserNameDisplay
        {
            get { return CreateUserName.ToUpper(); }
        }
    }
}
