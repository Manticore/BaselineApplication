﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the top 4 trade leads in the site based on the number
-- views.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetTopTradeLeads
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @TradeLeads TABLE
    (
	   [TradeLeadID] INT, 
	   [Title] NVARCHAR(MAX), 
	   [Body] NVARCHAR(MAX), 
	   [CreateDate] DATETIME2, 
	   [CreateUser] NVARCHAR(50), 
	   [Views] INT
    );

    INSERT INTO @TradeLeads
    (
	   [TradeLeadID], 
	   [Title], 
	   [Body], 
	   [CreateDate], 
	   [CreateUser], 
	   [Views]
    )
    SELECT TOP 4 [TradeLeadID], 
                 [Title], 
                 [Body], 
                 [CreateDate], 
                 [CreateUser], 
                 [Views]
    FROM dbo.TradeLeads
    ORDER BY [Views] DESC, CreateDate;

    SELECT [TradeLeadID], 
           [Title], 
           [Body], 
           [CreateDate], 
           [CreateUser], 
           [Views]
    FROM @TradeLeads;

    SELECT I.IndustryID, 
           I.Name, 
           TI.TradeLeadID
    FROM dbo.TradeLeads_Industries TI 
	   INNER JOIN dbo.Industries I
             ON TI.IndustryID = I.IndustryID
    -- Return the industry tags for only the TradeLeads return above.
    WHERE TI.TradeLeadID IN(SELECT TradeLeadID FROM @TradeLeads);

END;