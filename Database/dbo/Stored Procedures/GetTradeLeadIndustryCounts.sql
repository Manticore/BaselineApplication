﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns all the industries associated with Trade Leads in 
-- the database, along with the number of trade leads each industry is 
-- associated with.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetTradeLeadIndustryCounts
AS
BEGIN

    SELECT I.IndustryID, 
           I.Name, 
           COUNT(*) AS IndustryCount
    FROM dbo.TradeLeads_Industries TLI 
	   INNER JOIN dbo.Industries I
             ON TLI.IndustryID = I.IndustryID
    GROUP BY I.IndustryID, 
             Name;

END;