﻿using System.Collections.Generic;
using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class ConfigureIndustriesModel
    {
        public IEnumerable<Industry> Industries { get; set; }
    }
}