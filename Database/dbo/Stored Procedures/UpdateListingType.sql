﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- Given a listing identifier and a listing type the listing is updated to the 
-- new listing type.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.UpdateListingType
    @ListingID   INT,
    @ListingType NVARCHAR(50)
AS
BEGIN
	
	-- Retrieve the listing type ID using the Type text.
    DECLARE @ListingTypeID INT;
    SELECT @ListingTypeID = ListingTypeID 
    FROM dbo.ListingTypes 
    WHERE [Type] = @ListingType;

	-- Update the specified listing.
    UPDATE dbo.Listings
           SET ListingTypeID = @ListingTypeID
    WHERE ListingID = @ListingID;

END;