﻿using System.Collections.Generic;
using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class ListingSummaryViewModel
    {
        public string PassedKeyword { get; set; }
        public int? PassedDirectoryID { get; set; }
        public int? PassedIndustryID { get; set; }
        public int? PassedStateID { get; set; }
        public IEnumerable<Industry> SelectIndustries { get; set; }
        public IEnumerable<Listing> Listings { get; set; }
        public IEnumerable<Directory> Directories { get; set; }
        public List<Industry> IndustryCounts { get; set; }
        public IEnumerable<State> States { get; set; }
    }
}