﻿namespace Models.Models
{
    public class Contact
    {
        public string ContactTitle { get; set; }
        public string Name         { get; set; }
    }
}