﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- Procedure that deletes a specified trade lead.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.DeleteTradeLead
       @TradeLeadID INT
AS
BEGIN
    
	SET NOCOUNT ON;

    -- Remove any industries associated with this trade lead.
    DELETE FROM dbo.TradeLeads_Industries
    WHERE TradeLeadID = @TradeLeadID;

    -- Remove the trade lead.
    DELETE FROM dbo.TradeLeads
    WHERE TradeLeadID = @TradeLeadID;

END; 