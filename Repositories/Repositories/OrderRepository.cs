﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Models;
using Models.Interfaces;
using Models.Models;

namespace Repositories.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly IDatabaseContext _databaseAccess;

        public OrderRepository(IDatabaseContext databaseAccess)
        {
            _databaseAccess = databaseAccess;
        }

        public PagedList<Order> GetOrdersPaged(OrdersParameterSet parameters)
        {
            return _databaseAccess.GetOrdersPaged(parameters);
        }

        public Order GetOrder(int orderID)
        {
            return _databaseAccess.GetOrder(orderID);
        }

        public int? SaveOrder(Order order, bool isPaid, int userID)
        {
            return _databaseAccess.SaveOrder(order, isPaid, userID);
        }

        public IEnumerable<OrderStatus> GetOrderStatuses()
        {
            return _databaseAccess.GetOrderStatuses();
        }

        public IEnumerable<PaymentType> GetPaymentTypes(bool isAdmin)
        {
            return _databaseAccess.GetPaymentTypes(isAdmin);
        }

        public int? SaveOrderPayment(Payment payment, int orderIdentifier, int userIdentifier)
        {
            try
            {
                return _databaseAccess.SaveOrderPayment(payment, orderIdentifier, userIdentifier);
            }
            catch (SqlException)
            {
                return null;
            }
        }

        public PagedList<PrintOrderReportModel> GetUnpaidOrdersForPrinting(PrintOrdersParameterSet parameters)
        {
            return _databaseAccess.GetUnpaidOrdersForPrinting(parameters);
        }

        public int? GetOrderOwnerUserID(int orderIdentifier)
        {
            return _databaseAccess.GetOrderOwnerUserID(orderIdentifier);
        }

        public bool IsUserOrderOwner(int userIdentifier, int orderIdentifier)
        {
            int? orderOwnerUserIdentifier = this.GetOrderOwnerUserID(orderIdentifier);

            if (orderOwnerUserIdentifier.HasValue)
            {
                return (orderOwnerUserIdentifier.Value == userIdentifier);
            }
            return false;
        }

        public void SaveOrderPrintedDate(int orderIdentifier, int userIdentifier)
        {
            _databaseAccess.SaveOrderPrintedDate(orderIdentifier, userIdentifier);
        }

        public OrderBillDataModel GetOrderDetailsForBill(int orderIdentifier)
        {
            return _databaseAccess.GetOrderDetailsForBill(orderIdentifier);
        }

        public Payment GetPayment(int paymentIdentifier)
        {
            return _databaseAccess.GetPayment(paymentIdentifier);
        }

        public int? GetUnpaidOrderIdentifierForListing(int listingIdentifier)
        {
            return _databaseAccess.GetUnpaidOrderIdentifierForListing(listingIdentifier);
        }

        public int? GetCreditCardPaymentTypeID()
        {
            return _databaseAccess.GetCreditCardPaymentTypeID();
        }

        public bool HasOrderBeenInvoiced(int orderIdentifier)
        {
            return _databaseAccess.HasOrderBeenInvoiced(orderIdentifier);
        }

        public void DeleteUnpaidOrder(int orderIdentifier)
        {
            _databaseAccess.DeleteUnpaidOrder(orderIdentifier);
        }
    }
}
