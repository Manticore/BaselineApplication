﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the Countries available in the site.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetCountries
AS
BEGIN

    SET NOCOUNT ON;

    SELECT CountryID, 
           Name
    FROM dbo.Countries;

END;