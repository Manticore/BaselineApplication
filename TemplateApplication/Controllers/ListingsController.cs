﻿using System.Web.Helpers;
using Elmah;
using Library.Exceptions;
using Models.Interfaces;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Stripe;
using TemplateApplication.Models.ViewModels;
using WebMatrix.WebData;
using TemplateApplication.Filters;

namespace TemplateApplication.Controllers
{
    [InitializeSimpleMembership]
    [ValidateAntiForgeryTokenOnAllPosts]
    public class ListingsController : Controller
    {
        private readonly IListingRepository _listingRepository;
        private readonly IDatabaseContext _database;
        private readonly IListingService _listingService;
        private readonly IListingCostCalculator _listingCalculator;

        private const int StartingPage = 1;
        private const int DefaultNumberRecords = 25;

        public ListingsController(IListingRepository listingRepository, IDatabaseContext database, IListingService listingService, IListingCostCalculator listingCalculator)
        {
            _listingRepository = listingRepository;
            _listingService = listingService;
            _database = database;
            _listingCalculator = listingCalculator;
        }

        // This is the Action method for the /Listings page.
        // This page contains controls used to search for business listings.
        // A subset of the records are displayed as the page is loaded.
        // The parameters are optional GET parameters sent from the home page.
        public ActionResult Index(string keyword, int? directory, int? industry, int? state)
        {
            // If an industry ID has been passed via GET parameter...
            List<int?> industryList = new List<int?>();
            if (industry.HasValue)
            {
                industryList.Add(industry.Value);
            }

            ListingSummaryViewModel model = new ListingSummaryViewModel
            {
                PassedKeyword = keyword,
                PassedDirectoryID = directory,
                PassedIndustryID = industry,
                PassedStateID = state,
                // Get the first page of listings to be displayed.
                Listings = _listingRepository.GetListings(StartingPage, DefaultNumberRecords, keyword, directory, industryList, state),
                // Get the list of industries to be displayed in the search control.
                SelectIndustries = _database.GetIndustries(),
                Directories = _database.GetDirectories(),
                IndustryCounts = _database.GetListingIndustryCount().ToList(),
                States = _database.GetStates()
            };
            return View(model);
        }

        // This Action method displays details for a selected business listing.
        public ActionResult Details(int id, string message)
        {
            ListingDetailViewModel model = new ListingDetailViewModel
            {
                Message = message,
                Listing = _listingRepository.GetSpecificListing(id)
            };
            return View(model);
        }

        // THIS ACTION METHOD IS UNUSED. LEFT INTACT IN CASE A FUTURE USE IS NECESSARY.
        //[Authorize]
        //[Authorize(Roles = "Administrator")]
        //public ActionResult AdminCreate()
        //{
        //    // Use the ViewBag to store the button text to be displayed in the form partial view.
        //    ViewBag.ButtonText = "Create Listing";

        //    var viewModel = new ListingViewModel
        //    {
        //        Listing = null,
        //        States = _database.GetStates(),
        //        Directores = _database.GetDirectories()
        //    };
        //    return View(viewModel);
        //}

        // This is the Action method for the Admin Edit page.
        [Authorize(Roles = "Administrator")]
        public ActionResult AdminEdit(int id)
        {
            ViewBag.ReturnURL = Request.UrlReferrer;

            var viewModel = new ListingViewModel
            {
                // Retrieve the details for a specific listing.
                Listing = _listingRepository.GetSpecificListing(id),
                States = _database.GetStates(),
                Directores = _database.GetDirectories(),
                Industries = _database.GetIndustries()
            };
            return View(viewModel);
        }

        /// <summary>
        /// This action method retrieves complete details for a specified listing.
        /// Users can view subscriptions, orders, and other detailed information.
        /// </summary>
        /// <param name="id">The specified listing identifier.</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrator")]
        public ActionResult AdminDetails(int id)
        {
            // Retrieve the listing from the repository.
            var listing = _listingRepository.GetSpecificListingFull(id);
            return View(listing);
        }

        // This is the Action Method for the "Create" page.
        // This page is used to create a new business listing, but it is
        // also used to edit an existing listing. Administrators can use this
        // utility to modify a listing on behalf of a user, resulting in charges if applicable.
        [HttpGet]
        [Authorize]
        public ActionResult Create(int? listingIdentifier)
        {
            // todo add a clause in here stopping an admin from entering if a listing identifier is not present?
            if (listingIdentifier.HasValue)
            {
                int currentUserIdentifier = WebSecurity.GetUserId(User.Identity.Name);
                var isAdministrator = User.IsInRole("Administrator");
                // If the user is not an administrator and they are not the listing owner redirect them.
                if (!(isAdministrator || _listingRepository.IsUserListingOwner(listingIdentifier.Value, currentUserIdentifier)))
                {
                    // Redirect the user to the home page.
                    return RedirectToAction("Index", "Home");
                }

                // If there is an unpaid order for this listing we do not want the user to configure the listing.
                // We want them to pay their order bill instead. 
                if (_listingRepository.DoesUnpaidOrderExist(listingIdentifier.Value))
                {
                    return RedirectToAction("Manage", "Account", new { message = "UnpaidOrderExists" });
                }
            }

            // Retrieve the data required for the page.
            ListingViewModel viewModel = new ListingViewModel
            {
                // Return the listing to be edited, if applicable.
                Listing = _listingRepository.GetSpecificListing(listingIdentifier),
                // Return states, directories, and industries available in the site.
                States = _database.GetStates(),
                Directores = _database.GetDirectories(),
                Industries = _database.GetIndustries(),
                // Set the form action to the correct POST action.
                ListingFormAction = "/Listings/Create",
                SubmitButtonText = "Continue"
            };
            return View(viewModel);
        }

        // This is the POST Action method for the Create page.
        [HttpPost]
        [Authorize]
        public ActionResult Create([ModelBinder(typeof(ListingBinder))]Listing listing)
        {
            var currentUserIdentifier = WebSecurity.GetUserId(User.Identity.Name);
            // If the listing has not been modified in a way that requires subscription changes
            // just save the listing for the user and redirect them to their listing page.
            if (!_listingService.HasListingSubscriptionChanged(listing) && listing.ListingID.HasValue)
            {
                _listingService.SaveListing(listing, currentUserIdentifier);
                return RedirectToAction("Details", "Listings", new { id = listing.ListingID.Value, message = "ListingSaved" });
            }
            // If the listing is a completely new listing, or the directories or descriptions
            // have been changed we need to show them the purchase screen if charges are required
            // or the subscription change confirmation page if charges are not required.
            // Save the listing in session for use later.
            Session["Listing"] = listing;
            // This redirect will take them to the page where they configure their subscription plan.
            return RedirectToAction("Purchase", "Listings");
        }

        // This is the purchase Action Method.
        [Authorize]
        public ActionResult Purchase()
        {
            var listing = (Listing)Session["Listing"];

            // Get the current subscription if one exists for the listing. If this listing is completely new,
            // or no subscription exists, this weill be null.
            Subscription currentSubscription = null;
            if (listing.ListingID.HasValue)
            {
                currentSubscription = _listingRepository.GetCurrentSubscription(listing.ListingID.Value);
            }

            // Calculate the subscription cost for the listing.
            var listingSubscriptionCost = _listingCalculator.CalculateListingCost(listing);


            // Generate a preview order based on the current state of the listing.
            var previewSubscriptionOrder = CreatePreviewSubscriptionOrder(listing);

            // If the preview listing order is null, there are no charges are required, or the cost would be < 50 cents.
            // Thus the purchase page will be displayed, along with a description of the necessary charges.
            if (previewSubscriptionOrder != null)
            {
                ListingOrderViewModel model = new ListingOrderViewModel
                {
                    CurrentSubscription = currentSubscription,
                    ListingSubscriptionCost = listingSubscriptionCost,
                    Order = previewSubscriptionOrder,
                    States = _database.GetStates()
                };
                return View(model);
            }

            // If the preview listing order is null, there are no charges are required, or the cost would be < 50 cents.
            // In this case redirect the user to a page where their subscription will be changed, but no payment is necessary.
            return RedirectToAction("Downgrade", "Listings");
        }

        [HttpPost]
        [Authorize]
        public ActionResult Purchase(string cardToken)
        {
            // The new state of the user's listing.
            var listing = (Listing)Session["Listing"];
            bool newListingFlag = false;
            int? newListingID = null;

            try
            {
                int ownerUserID;

                // If this is for a brand new listing we need to save the listing before begining.
                if (!listing.ListingID.HasValue)
                {
                    ownerUserID = WebSecurity.GetUserId(User.Identity.Name);
                    newListingID = _listingService.SaveListing(listing, ownerUserID);
                    if (newListingID.HasValue)
                    {
                        listing.ListingID = newListingID.Value;
                        newListingFlag = true;
                    }
                    else throw new Exception();
                }
                    // If the listing already exists, we need to get the owner user ID.
                else
                {
                    var userIdentifier = _listingRepository.GetListingOwnerUserID(listing.ListingID.Value);
                    if (userIdentifier.HasValue)
                    {
                        ownerUserID = userIdentifier.Value;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                // Make sure we have a valid listing identifier before continuing.
                if (listing.ListingID.HasValue)
                {
                    // Retrieve the user identifier for the user who is making these subscription changes.
                    int currentUserID = WebSecurity.GetUserId(User.Identity.Name);
                    _listingService.ConfigureSubscription(listing, cardToken, ownerUserID, currentUserID);
                    // Redirect the user to the success page.
                    var listingCost = _listingCalculator.CalculateListingCost(listing);
                    return RedirectToAction("PurchaseConfirmation", "Listings",
                        new {listingIdentifier = listing.ListingID.Value, listingCost = listingCost});
                }
            }
            catch (Exception exception)
            {
                // If a new listing was created using the wizad, we need to delete if there was an issue.
                // This is because a listing needs to be created before a subscription is created in order to link them.
                // If the subscription fails to pay, we need to delete the listing that was previously created.
                // When the user submits the payment form again, the listing will be created from what is stored in the session state.
                if (newListingFlag)
                {
                    _database.DeleteListing(newListingID.Value);
                    listing.ListingID = null;
                }

                string errorMessage = null;
                if (exception is StripeException)
                {
                    var stripeException = exception as StripeException;
                    // If there was an issue with payment, set the appropriate error message.
                    switch (stripeException.StripeError.Code)
                    {
                        case "card_declined":
                            errorMessage = "Your credit card was declined. Please try another credit card.";
                            break;
                        case "incorrect_cvc":
                            errorMessage = "The CVC code you provided was incorrect. Please try again.";
                            break;
                        case "expired_card":
                            errorMessage = "The credit card you provided is expired. Please try another credit card.";
                            break;
                        default:
                            errorMessage = "An issue occured when making a payment. Please try again later.";
                            break;
                    }
                }
                else
                {
                    errorMessage = "An error occured when purchasing you listing subscription. Please try again.";
                }

                // Redisplay error messages and the purchase form.
                ModelState.AddModelError("", errorMessage);

                Subscription currentSubscription = null;
                if (listing.ListingID.HasValue) 
                {
                    currentSubscription = _listingRepository.GetCurrentSubscription(listing.ListingID.Value);
                }

                // Calculate the subscription cost for the listing.
                var listingSubscriptionCost = _listingCalculator.CalculateListingCost(listing);

                // Recreate the order based on the state of their listing.
                ListingOrderViewModel model = new ListingOrderViewModel
                {
                    Order = CreatePreviewSubscriptionOrder(listing),
                    States = _database.GetStates(),
                    CurrentSubscription = currentSubscription,
                    ListingSubscriptionCost = listingSubscriptionCost

                };
                // Return the payment form view so they can switch payment, etc.
                return View(model);
            }

            // If there was a strange issue.
            return RedirectToAction("", "");
        }

        public ActionResult PurchaseConfirmation(int listingIdentifier, int listingCost)
        {
            PurchaseConfirmationViewModel model = new PurchaseConfirmationViewModel
            {
                ListingIdentifier = listingIdentifier,
                SubscriptionCost = listingCost
            };
            return View(model);
        }

        public ActionResult Downgrade()
        {
            Listing listing = (Listing)Session["Listing"];
            int newSubscriptionCost = _listingCalculator.CalculateListingCost(listing);
            DowngradeSubscriptionModel model = new DowngradeSubscriptionModel
            {
                PreviewListing = listing,
                NewSubscriptionCost = newSubscriptionCost
            };

            return View(model);
        }

        public Order CreatePreviewSubscriptionOrder(Listing listing)
        {
            // If the listing does not have a listing identifier we need to generate a preview order for a new subscription.
            if (!listing.ListingID.HasValue)
            {
                var previewOrder = _listingService.CreateNewSubscriptionOrder(listing);
                return previewOrder;
            }
            // If the listing has a listing ID, we know that it exists already.
            // We need to retrieve the current subscription, if it exists to determine what the user will owe.
            Subscription currentSubscription = _listingRepository.GetCurrentSubscription(listing.ListingID.Value);
            // If a subscription does not exists, they owe for a new subscription.
            if (currentSubscription == null)
            {
                var previewOrder = _listingService.CreateNewSubscriptionOrder(listing);
                return previewOrder;
            }

            // If a subscription exists, we need to determine if the changes the user made require additional charges.
            if (_listingService.ChargesAreRequired(listing, currentSubscription))
            {
                int userIdentifier = WebSecurity.GetUserId(User.Identity.Name);
                var previewOrder = _listingService.CreateProratedSubscriptionOrder(listing, currentSubscription, userIdentifier);

                // If the cost for the prorated changes are less than .50 cents we can't charge the user.
                if (previewOrder.OrderTotal <= 50)
                {
                    return null;
                }
                return previewOrder;
            }
            return null;
        }

        //[HttpGet]
        //[Authorize]
        //public ActionResult Edit(int id)
        //{
        //    int currentUserID = WebSecurity.GetUserId(User.Identity.Name);
        //    if (_listingRepository.IsUserListingOwner(id, currentUserID))
        //    {
        //        ListingViewModel viewModel = new ListingViewModel
        //        {
        //            Listing    = _listingRepository.GetSpecificListing(id),
        //            States     = _database.GetStates(),
        //            Directores = _database.GetDirectories(),
        //            Industries = _database.GetIndustries(),
        //            ListingFormAction = "/Listings/Edit",
        //            SubmitButtonText  = "Save Listing"
        //        };
        //        return View(viewModel);
        //    }
        //    // Redirect the user if they are not the listing owner.
        //    return RedirectToAction("Index", "Home");
        //}

        //[HttpPost]
        //[Authorize]
        //public ActionResult Edit([ModelBinder(typeof(ListingBinder))]Listing listing)
        //{
        //    int currentUserID = WebSecurity.GetUserId(User.Identity.Name);
        //    var listingIdentifier = listing.ListingID;
        //    if (_listingRepository.IsUserListingOwner(listingIdentifier, currentUserID))
        //    {
        //        _listingService.SaveListing(listing, currentUserID);
        //    }
        //    return RedirectToAction("Manage", "Account");
        //}

        #region POST Methods
        [HttpPost]
        public ActionResult GetListingResults(int pageNumber, int numberPerPage, ListingSearchViewModel search)
        {
            // Make sure the data passed via the ListingSearchViewModel is valid.
            var listings = _listingRepository.GetListings(pageNumber, numberPerPage, search.Keyword, search.DirectoryID, search.IndustryIDs, search.StateID).ToList();
            return PartialView("_ListingSummary", listings);
        }

        [HttpPost]
        public ActionResult GetListingDetailsHTML(int listingID)
        {
            var listing = _listingRepository.GetSpecificListing(listingID);
            return PartialView("_ListingDetail", listing);
        }

        [HttpPost]
        [Authorize]
        public JsonResult UpsertListing(Listing listing)
        {
            int currentUserID = WebSecurity.GetUserId(User.Identity.Name);

            if (User.IsInRole("Administrator") || (listing.ListingID.HasValue && _listingRepository.IsUserListingOwner(listing.ListingID.Value, currentUserID)))
            {
                try
                {
                    _listingService.SaveListing(listing, currentUserID);
                }
                catch (ListingValidationException exception)
                {
                    return Json(exception.Message);
                }
            }
            return Json("Listing saved successfully.");
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public void SaveListingNote(Note note)
        {
            int currentUserID = WebSecurity.GetUserId(User.Identity.Name);
            _listingRepository.SaveListingNote(note, currentUserID);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult GetListingNotes(int listingIdentifier)
        {
            var notes = _listingRepository.GetListingNotes(listingIdentifier);
            return PartialView("_ListingNotes", notes);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public JsonResult CancelListing(int listingIdentifier)
        {
            try
            {
                _listingRepository.CancelListing(listingIdentifier);
                return Json(new { success = true });
            }
            catch (Exception)
            {
                return Json(new { success = false });
            }
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public JsonResult ClearCancelledListingFlag(int listingIdentifier)
        {
            try
            {
                _listingRepository.ClearCancelledFlag(listingIdentifier);
                return Json(new { success = true });
            }
            catch (Exception)
            {
                return Json(new { success = false });
            }
        }

        #endregion

        #region Custom Listing Model Binder
        public class ListingBinder : IModelBinder
        {
            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                HttpRequestBase request = controllerContext.HttpContext.Request;
                // Retrieve the industry identifier that have been passed.
                var industriesString = request.Form.Get("Listing[IndustryList][][IndustryID]");
                var industryIdentifierArray = industriesString.Split(',').Select(Int32.Parse).ToList();
                List<Industry> industryList = industryIdentifierArray.Select(id => new Industry
                {
                    IndustryID = id
                }).ToList();


                var directoriesString = request.Form.Get("Listing[Directories][][DirectoryID]");
                var directoryIdentifierArray = directoriesString.Split(',').Select(Int32.Parse).ToList();
                List<Directory> directoryList = directoryIdentifierArray.Select(id => new Directory
                {
                    DirectoryID = id
                }).ToList();

                var descriptionCodeString = request.Form.Get("Listing[DescriptionLines][][Code]");
                var descCodeArray = descriptionCodeString.Split(',');
                var descriptionBodyString = request.Form.Get("Listing[DescriptionLines][][Body]");
                var descBodyArray = descriptionBodyString.Split(',');

                List<Description> descriptionLines = new List<Description>();
                for (int i = 0; i < descCodeArray.Length; i++)
                {
                    descriptionLines.Add(new Description()
                    {
                        Code = descCodeArray[i],
                        Body = descBodyArray[i]
                    });
                }

                int? listingIdentifier;
                int value;
                bool isInteger = Int32.TryParse(request.Form.Get("Listing[ListingID]"), out value);
                if (isInteger)
                    listingIdentifier = value;
                else
                    listingIdentifier = null;

                return new Listing
                {
                    ListingID = listingIdentifier,
                    BusinessName = request.Form.Get("Listing[BusinessName]"),
                    BusinessContact = request.Form.Get("Listing[BusinessContact]"),
                    ContactNumber = request.Form.Get("Listing[ContactNumber]"),
                    FaxNumber = request.Form.Get("Listing[FaxNumber]"),
                    ContactEmail = request.Form.Get("Listing[ContactEmail]"),
                    Website = request.Form.Get("Listing[Website]"),

                    Address = new Address
                    {
                        Street1 = request.Form.Get("Listing[Address][Street1]"),
                        Street2 = request.Form.Get("Listing[Address][Street2]"),
                        City = request.Form.Get("Listing[Address][City]"),
                        StateID = Convert.ToInt32(request.Form.Get("Listing[Address][StateID]")),
                        Zip = request.Form.Get("Listing[Address][Zip]")
                    },

                    Description = request.Form.Get("Listing[Description]"),
                    ProductDescription = request.Form.Get("Listing[ProductDescription]"),
                    IndustryList = industryList,
                    Directories = directoryList,
                    DescriptionLines = descriptionLines
                };
            }
        }
        #endregion
    }
}