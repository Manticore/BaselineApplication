﻿using System.Collections.Generic;

namespace Models.Models
{
    public class PagedList<T>
    {
        public PagedList(IEnumerable<T> pagedListItems, int totalRecords)
        {
            PagedListItems = pagedListItems;
            TotalRecords   = totalRecords;
        }

        public IEnumerable<T> PagedListItems { get; set; }
        public int TotalRecords { get; set; }
    }
}
