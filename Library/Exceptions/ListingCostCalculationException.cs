﻿using System;

namespace Library.Exceptions
{
    [Serializable]
    public class ListingCostCalculationException : Exception
    {
        public ListingCostCalculationException(string message) : base(message)
        {
        }
    }
}
