﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure saves a given trade lead record.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.UpsertTradeLead
       @TradeLeadID INT = NULL, 
       @Title		NVARCHAR(MAX), 
       @Body		NVARCHAR(MAX),
	   @UserID		INT,
       @SelectedIndustryIDs IndustryIdentifierTable_UDT READONLY
AS
BEGIN

	BEGIN TRY
	    BEGIN TRANSACTION;

		SET NOCOUNT ON;

		-- Grab the current date setting to be used in the stored procedure.
		DECLARE @CurrentDate DATETIME2 = dbo.GetCurrentDate();

		UPDATE dbo.TradeLeads
			   SET Title	= @Title, 
				   Body		= @Body,
				   EditDate = @CurrentDate,
				   EditUser = @UserID
		WHERE TradeLeadID = @TradeLeadID;

		-- The Trade Lead exists in the database.
		IF @@ROWCOUNT > 0
			BEGIN
				DELETE FROM dbo.TradeLeads_Industries
				WHERE TradeLeadID = @TradeLeadID;

				INSERT INTO dbo.TradeLeads_Industries
				(
					TradeLeadID,
					IndustryID
				 )
				SELECT @TradeLeadID, 
					   IndustryID
				FROM @SelectedIndustryIDs;
			END;

		-- The Trade Lead does not exist in the database.
		ELSE
			BEGIN
				INSERT INTO dbo.TradeLeads(Title, Body, CreateDate, CreateUser, EditDate, EditUser)
				VALUES(@Title, @Body, @CurrentDate, @UserID, @CurrentDate, @UserID);

			  -- Get the newly created Trade Lead ID.
			  DECLARE @NewTradeLeadID INT = SCOPE_IDENTITY()

			  INSERT INTO dbo.TradeLeads_Industries (TradeLeadID, IndustryID)
			  SELECT @NewTradeLeadID, IndustryID FROM @SelectedIndustryIDs

			END;

		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH

		DECLARE @ErrorMessage  NVARCHAR(4000) = ERROR_MESSAGE();
		DECLARE @ErrorSeverity INT			  = ERROR_SEVERITY();
		DECLARE @ErrorState    INT			  = ERROR_STATE();

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

		ROLLBACK TRANSACTION;

	END CATCH;

END;