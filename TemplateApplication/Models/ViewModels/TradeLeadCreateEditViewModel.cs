﻿using System.Collections.Generic;
using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class TradeLeadCreateEditViewModel
    {
        public TradeLead TradeLead { get; set; }
        public IEnumerable<Industry> Industries { get; set; }
    }
}