﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure is used to delete a listing. This procedure is primarily used
-- to delete a listing that has been created by a user who visits the site when
-- due to an error, purchase of the listing cannot be completed. The reason this
-- is necessary is when creating a Stripe subscription for a listing it must
-- exists in our database. If there is an error when creating a subscription
-- and the listing did not exist prior to purchasing a subscription, we want to
-- delete the listing and start over so the user can update their credit card
-- details or fix the issue that was encountered.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.DeleteListing
    @ListingID INT
AS
BEGIN

    -- Remove the directories saved for this listing.
    DELETE FROM dbo.Listings_Directories
    WHERE ListingID = @ListingID

    -- Delete the descriptions associated with this listing.
    DELETE FROM dbo.Descriptions 
    WHERE Descriptions.ListingID = @ListingID

    -- Delete the industries for this listing.
    DELETE FROM dbo.Listings_Industries 
    WHERE ListingID = @ListingID

    -- Retrieve the ID for the address associated with this listing.
    DECLARE @AddressID INT;
    SELECT @AddressID = AddressID
    FROM dbo.Listings_Addresses LA 
    WHERE ListingID = @ListingID

	-- Delete the cross reference table record.
	DELETE FROM dbo.Listings_Addresses
	WHERE ListingID = @ListingID;

	-- Delete the address associated with the listing.
    DELETE FROM dbo.Addresses
    WHERE AddressID = @AddressID

    -- Delete the listing.
    DELETE FROM dbo.Listings 
    WHERE ListingID = @ListingID

END;