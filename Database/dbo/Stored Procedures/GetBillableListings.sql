﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns Listings that need to be billed by administrators.
-- These listings do not have a valid Stripe subscription and cannot be billed
-- automatically. 
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetBillableListings
       @Page					INT			  = NULL, 
       @NumberOfRecords			INT			  = NULL,
	   @ListingID				INT			  = NULL,
	   @BusinessName			NVARCHAR(250) = NULL,
	   @ListingCreateDateStart	DATETIME2	  = NULL,
	   @ListingCreateDateEnd	DATETIME2	  = NULL,
	   @LastSubDateStart		DATETIME2	  = NULL,
	   @LastSubDateEnd			DATETIME2	  = NULL,
	   @OrderGenerated			BIT			  = NULL,
	   @OrderGenDateStart		DATETIME2	  = NULL,
	   @OrderGenDateEnd			DATETIME2	  = NULL,
	   @SortColumn				NVARCHAR(50)  = NULL,
	   @SortDirection			NVARCHAR(10)  = NULL
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @CurrentDate DATETIME = dbo.GetCurrentDate();

    -- Retrieve the ID for the Manual Listing type.
    DECLARE @ManualListingTypeID INT;
    SELECT @ManualListingTypeID = ListingTypeID
    FROM ListingTypes
    WHERE Type = 'Manual';

    -- Retrieve the unpaid order status identifier.
    DECLARE @UnpaidOrderStatusID INT;
    SELECT @UnpaidOrderStatusID = OrderStatusID
    FROM dbo.OrderStatuses
    WHERE Status = 'Unpaid';

    -- Calculate the paging Offset.
    DECLARE @OFFSET INT = (@Page - 1) * @NumberOfRecords;

    DECLARE @Listings TABLE
    (
	   ListingID INT, 
	   BusinessName NVARCHAR(250),
	   ListingCreateDate DATETIME2,
	   LastSubscriptionDate DATETIME2, 
	   OrderGenerated NVARCHAR(10), 
	   GeneratedDate DATETIME2, 
	   TotalRecords INT
    );

    -- Retrieve all manual listings without a subscription.
    INSERT INTO @Listings
    (
	    ListingID, 
	    BusinessName,
		ListingCreateDate,
	    LastSubscriptionDate, 
	    OrderGenerated, 
	    GeneratedDate, 
	    TotalRecords
    )
    SELECT L.ListingID, 
           L.BusinessName, 
		   L.CreateDate,
           LastSubscription.LastSubscriptionDate, 
           CASE
           WHEN UnpaidOrder.OrderID IS NULL THEN 'No'
               ELSE 'Yes'
           END AS OrderGenerated, 
           UnpaidOrder.CreateDate AS GeneratedDate, 
           TotalRecords = COUNT(1) OVER()
    FROM dbo.Listings L 
		 LEFT JOIN(SELECT MAX(EndDate) AS LastSubscriptionDate, 
                                         ListingID
                                  FROM dbo.Subscriptions
                                  GROUP BY ListingID)AS LastSubscription
             ON L.ListingID = LastSubscription.ListingID
         LEFT JOIN(SELECT ListingID, 
                          OrderID, 
                          CreateDate
                   FROM dbo.Orders
                   WHERE OrderStatusID = @UnpaidOrderStatusID) AS UnpaidOrder
             ON L.ListingID = UnpaidOrder.ListingID
         LEFT JOIN dbo.GetCurrentSubscriptions() AS CurrentSubscriptions
             ON L.ListingID = CurrentSubscriptions.ListingID
    WHERE L.ListingTypeID = @ManualListingTypeID 
		  AND CurrentSubscriptions.SubscriptionID IS NULL
		  AND L.CancelledDate IS NULL
	 AND (@ListingID IS NULL OR L.ListingID = @ListingID)
	 AND (@BusinessName IS NULL OR (L.BusinessName LIKE '%'+ @BusinessName + '%'))
	 AND (@ListingCreateDateStart IS NULL OR L.CreateDate >= @ListingCreateDateStart) 
	 AND (@ListingCreateDateEnd IS NULL OR L.CreateDate <= @ListingCreateDateEnd)
	 AND (@LastSubDateStart IS NULL OR LastSubscription.LastSubscriptionDate >= @LastSubDateStart) 
	 AND (@LastSubDateEnd IS NULL OR LastSubscription.LastSubscriptionDate <= @LastSubDateEnd)
	 AND (@OrderGenDateStart IS NULL OR UnpaidOrder.CreateDate >= @OrderGenDateStart) 
	 AND (@OrderGenDateEnd IS NULL OR UnpaidOrder.CreateDate <= @OrderGenDateEnd)
	 AND (@OrderGenerated IS NULL OR (@OrderGenerated = CASE WHEN UnpaidOrder.OrderID IS NULL THEN 0 ELSE 1 END))
	ORDER BY 
		CASE WHEN ((@SortColumn IS NULL) OR (@SortColumn = 'ListingID' AND @SortDirection = 'Ascendant')) 
			  THEN L.ListingID END ASC, 
		CASE WHEN @SortColumn = 'ListingID' AND @SortDirection = 'Descendant' 
			  THEN L.ListingID END DESC,
		CASE WHEN @SortColumn = 'BusinessName' AND @SortDirection = 'Ascendant'
			  THEN L.BusinessName END ASC, 
		CASE WHEN @SortColumn = 'BusinessName' AND @SortDirection = 'Descendant' 
			  THEN L.BusinessName END DESC,
		CASE WHEN @SortColumn = 'ListingCreateDateDisplay' AND @SortDirection = 'Ascendant'
			  THEN L.CreateDate END ASC, 
		CASE WHEN @SortColumn = 'ListingCreateDateDisplay' AND @SortDirection = 'Descendant' 
			  THEN L.CreateDate END DESC,
		CASE WHEN @SortColumn = 'LastSubscriptionDateDisplay' AND @SortDirection = 'Ascendant'
			  THEN LastSubscription.LastSubscriptionDate END ASC, 
		CASE WHEN @SortColumn = 'LastSubscriptionDateDisplay' AND @SortDirection = 'Descendant' 
			  THEN LastSubscription.LastSubscriptionDate END DESC,
		CASE WHEN @SortColumn = 'OrderGenerated' AND @SortDirection = 'Ascendant'
			  THEN UnpaidOrder.OrderID END ASC, 
		CASE WHEN @SortColumn = 'OrderGenerated' AND @SortDirection = 'Descendant' 
			  THEN UnpaidOrder.OrderID END DESC,
		CASE WHEN @SortColumn = 'GeneratedDateDisplay' AND @SortDirection = 'Ascendant'
			  THEN 	UnpaidOrder.CreateDate END ASC, 
		CASE WHEN @SortColumn = 'GeneratedDateDisplay' AND @SortDirection = 'Descendant' 
			  THEN 	UnpaidOrder.CreateDate END DESC
    OFFSET @OFFSET ROWS FETCH NEXT @NumberOfRecords ROWS ONLY;
    
    -- Return the total number of records for paging.
    SELECT TOP 1 TotalRecords
    FROM @Listings;

    -- Return the listings.
    SELECT ListingID, 
           BusinessName,
		   ListingCreateDate,
           LastSubscriptionDate,
           OrderGenerated, 
           GeneratedDate
    FROM @Listings;

END;