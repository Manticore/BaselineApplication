﻿using System.Collections.Generic;
using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class UserDetailViewModel
    {
        public UserDetail UserDetail { get; set; }
        public IEnumerable<Listing> UserListings { get; set; }
    }
}