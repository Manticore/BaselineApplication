﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Library.Exceptions;
using Models;
using Models.Interfaces;
using Models.Models;
using Stripe;

namespace Services
{
    public class ListingService : IListingService
    {
        private readonly IListingRepository _listingRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IListingCostCalculator _listingCalculator;
        private readonly IDatabaseContext _database;

        private readonly ModelStateDictionary _modelState;

        private int? _webhookListingIdentifier;

        public ListingService(IListingRepository listingRepository, IPaymentRepository paymentRepository, IOrderRepository orderRepository, IListingCostCalculator listingCalculator, IDatabaseContext database)
        {
            // Inject the repositories used in the service.
            _listingRepository = listingRepository;
            _paymentRepository = paymentRepository;
            _orderRepository = orderRepository;
            _listingCalculator = listingCalculator;
            _database = database;

            // Initialize the model state dictionary.
            _modelState = new ModelStateDictionary();
        }

        public bool ValidateListing(Listing listing)
        {
            if (listing.BusinessName == null || listing.BusinessName.Trim().Length == 0)
                _modelState.AddError("BusinessName", "Business name is required.");
            if (listing.Address.Street1.Trim().Length == 0)
                _modelState.AddError("Address-Street", "Street is required.");
            if (listing.Address.City.Trim().Length == 0)
                _modelState.AddError("Address-City", "City is required.");
            if (!listing.Address.StateID.HasValue)
                _modelState.AddError("Address-State", "State is required.");
            if (listing.Address.Zip.Trim().Length == 0)
                _modelState.AddError("Address-Zip", "Zip is required.");
            if (listing.Description.Trim().Length == 0)
                _modelState.AddError("Description", "Please provide a description of your business.");
            if (listing.ProductDescription.Trim().Length == 0)
                _modelState.AddError("Product Description", "Please provide a description of the products / services you provide.");
            if (listing.DescriptionLines.Count() > 10)
                _modelState.AddError("DescriptionLines", "Number of description lines cannot be greater than 10.");

            return _modelState.IsValid();
        }

        public int? SaveListing(Listing listing, int userID)
        {
            if (!ValidateListing(listing))
            {
                throw new ListingValidationException("The listing did not pass validation.");
            }
            return _listingRepository.UpsertListing(listing, userID);
        }

        public bool CancelSubscription(int listingIdentifier)
        {
            // Retrieve the full model for the listing object.
            var listing = _listingRepository.GetSpecificListingFull(listingIdentifier);
            // Retrieve the current subscription from the model
            Subscription subscription = listing.CurrentSubscription;
            // If there is an active subscription...
            if (subscription != null)
            {
                // If the subscription has a stripe subscription ID, we need to cancel it in Stripe.
                if (!string.IsNullOrEmpty(subscription.StripeSubscriptionID))
                {
                    if (listing.UserID.HasValue)
                    {
                        // Set up the required parameters for the CancelStripeSubscription method.
                        var localUserID = listing.UserID;
                        string stripeCustomerID = _paymentRepository.GetStripeCustomerID(localUserID.Value);
                        string stripeSubscriptionID = subscription.StripeSubscriptionID;
                        // Passign this values to the method cancels the subscription at the end of the subscription period.
                        // This will allow a user to return and reactivate their subscription if it hasn't ended yet.
                        const bool cancelAtPeriodEnd = true;
                        _paymentRepository.CancelStripeSubscription(stripeCustomerID, stripeSubscriptionID, cancelAtPeriodEnd);
                    }
                }
                // If the cancellation was successful in Stripe, or this is a listing without a Stripe subscription...
                // This is done last incase the cancellation of the Stripe subscription fails.
                _listingRepository.CancelSubscription(subscription.SubscriptionID);
            }
            return true;
        }

        public bool ReactivateSubscription(int listingIdentifier)
        {
            // Retrieve the full model for the listing object.
            var listing = _listingRepository.GetSpecificListingFull(listingIdentifier);
            // Retrieve the current subscription from the model
            Subscription subscription = listing.CurrentSubscription;
            // If there is an active subscription...
            if (subscription != null)
            {
                // If the subscription has a stripe subscription ID, we need to reactivate it in Stripe as well.
                if (!string.IsNullOrEmpty(subscription.StripeSubscriptionID))
                {
                    if (listing.UserID.HasValue)
                    {
                        // Set up the required parameters to reactivate a stripe subscription.
                        var localUserID = listing.UserID;
                        string stripeCustomerID = _paymentRepository.GetStripeCustomerID(localUserID.Value);
                        string stripeSubscriptionID = subscription.StripeSubscriptionID;
                        string stripePlanID = _listingRepository.GetDefaultPlanID();

                        _paymentRepository.ReactivateStripeSubscription(stripeCustomerID, stripeSubscriptionID, stripePlanID);
                    }
                }
                // If the activation was successful in Stripe, or this is a listing without a Stripe subscription...
                // This is done last to ensure the Stripe subscription is reactivated successfully.
                _listingRepository.ReactivateSubscription(subscription.SubscriptionID);
            }
            return true;
        }

        public void CreateNewLocalSubscription(int listingIdentifier, int subscriptionCost, int directoryCount, int descriptionCount, int orderIdentifier, string stripeSubscriptionID, int userIdentifier, DateTime startDate, DateTime endDate)
        {
            Subscription subscription = new Subscription
            {
                ListingID = listingIdentifier,
                OrderID = orderIdentifier,
                StripeSubscriptionID = stripeSubscriptionID,
                PaidDirectoryCount = directoryCount,
                PaidDescriptionLinesCount = descriptionCount,
                SubscriptionCost = subscriptionCost,
                StartDate = startDate,
                EndDate = endDate
            };
            _listingRepository.CreateSubscription(subscription, userIdentifier);
        }

        public void UpdateCurrentLocalSubscription(Listing listing, Subscription currentSubscription, int? orderIdentifier, int? newSubscriptionCost, string stripeSubscriptionID, int userIdentifier)
        {
            _listingRepository.UpdateLocalSubscription(currentSubscription.SubscriptionID, listing.Directories.Count(), listing.DescriptionLines.Count(), newSubscriptionCost, stripeSubscriptionID, userIdentifier);
        }

        public bool ChargesAreRequired(Listing listing, Subscription currentSubscription)
        {
            if (currentSubscription == null)
            {
                return true;
            }

            var cost = _listingCalculator.CalculateListingCost(listing);
            if (cost > currentSubscription.MaxSubscriptionCost)
            {
                return true;
            }
            return false;
        }

        /// This procedure determines wheter a new subscription should be set up for a 
        /// listing, or if the existing subscription needs to be modified instead.
        public void ConfigureSubscription(Listing listing, string creditCardToken, int ownerUserID, int currentUserID)
        {
            if (listing.ListingID.HasValue)
            {
                var subscription = _listingRepository.GetCurrentSubscription(listing.ListingID.Value);
                if (subscription == null)
                {
                    ConfigureNewSubscription(listing, creditCardToken, ownerUserID);
                }
                else
                {
                    ConfigureExistingSubscription(listing, creditCardToken, ownerUserID, currentUserID);
                }
            }
        }

        public void ConfigureNewSubscription(Listing listing, string creditCardToken, int userIdentifier)
        {
            if (listing.ListingID.HasValue)
            {
                // Retrieve the plan ID to be used for the Stripe subscription.
                string stripePlanID = _database.GetDefaultPlanID();
                // Save the listing so it is updated in our system.
                this.SaveListing(listing, userIdentifier);

                // Create a new stripe customer, or retrive their existing stripe customer identifier.
                string emailAddress = _database.GetUserEmail(userIdentifier);
                string stripeCustomerID = _paymentRepository.CreateStripeCustomer(userIdentifier, emailAddress);

                // If they provided a new form of payment for this listing.
                // Set the credit card associated with their account to their new method.
                if (!string.IsNullOrEmpty(creditCardToken))
                    _paymentRepository.UpdatePaymentMethod(userIdentifier, creditCardToken);

                int listingSubscriptionCost = _listingCalculator.CalculateListingCost(listing);
                // Create the new subscription in Stripe.

                var metadata = new Dictionary<string, string>
                {
                    { "ListingID", listing.ListingID.Value.ToString(CultureInfo.InvariantCulture)}
                };
                // Create the subscription in Stripe.
                _paymentRepository.CreateNewSubscription(stripeCustomerID, listingSubscriptionCost, stripePlanID, null, metadata);
                _listingRepository.UpdateListingType(listing.ListingID.Value, "Online");
                // The rest of this process is handled by the invoice.payment_succeeded webhook.
                _listingRepository.ClearCancelledFlag(listing.ListingID.Value);
            }
        }

        public void ConfigureExistingSubscription(Listing listing, string creditCardToken, int ownerUserID, int currentUserID)
        {
            if (listing.ListingID.HasValue)
            {
                // Retrieve the plan ID to be used for the Stripe subscription.
                string stripePlanID = _database.GetDefaultPlanID();
                bool updateFlag = true;

                // Create a new stripe customer, or retrive their existing stripe customer identifier.
                string emailAddress = _database.GetUserEmail(ownerUserID);
                string stripeCustomerID = _paymentRepository.CreateStripeCustomer(ownerUserID, emailAddress);

                // If they provided a new form of payment for this listing.
                // Set the credit card associated with their account to their new method.
                if (!string.IsNullOrEmpty(creditCardToken))
                    _paymentRepository.UpdatePaymentMethod(ownerUserID, creditCardToken);

                // Calculate the cost of the subscription based on the listing selections.
                int listingSubscriptionCost = _listingCalculator.CalculateListingCost(listing);
                // Retrieve the current subscription for the listing.
                var currentSubscription = _listingRepository.GetCurrentSubscription(listing.ListingID.Value);

                string newStripeSubscriptionID = null;
                // Create an order for the prorated cost of the subscription.
                Order proratedOrder = CreateProratedSubscriptionOrder(listing, currentSubscription, currentUserID);
                // If the cost of the listing subscription is > than what the user has paid and the charges are more than .50 cents...
                if (listingSubscriptionCost > currentSubscription.MaxSubscriptionCost && (proratedOrder != null && proratedOrder.OrderTotal > 50))
                {
                    // If the current subscription does not have a Stripe subscription ID, the listing is has never been billed using Stripe before.
                    // Therefore there is no associated Stripe subscription. If we want the listing to be billed automatically using the chosen credit
                    // card next period, we need to create a new Stripe subscription, but it will be a free trial until the current subscription period ends.
                    // Once the current subscription period ends, the listing will be billed automatically without and manual billing process involved.
                    if (string.IsNullOrEmpty(currentSubscription.StripeSubscriptionID))
                    {
                        // Set the free trial for the subscription to end on the end date of the current subscription.
                        DateTime currentSubscriptionEndDate = currentSubscription.EndDate;
                        // Setting the trial end date parameter creates the subscription as a free trial, ending on the specified date.
                        Dictionary<string, string> metadata = new Dictionary<string, string>
                        {
                                {"ListingID", listing.ListingID.Value.ToString(CultureInfo.InvariantCulture)}
                        };

                        var stripeSubscription = _paymentRepository.CreateNewSubscription(stripeCustomerID, listingSubscriptionCost, stripePlanID, currentSubscriptionEndDate, metadata);
                        newStripeSubscriptionID = stripeSubscription.Id;

                        updateFlag = false;
                        // Set the listing type to online type now that the billing will be handled by Stripe.
                        _listingRepository.UpdateListingType(listing.ListingID.Value, "Online");
                    }

                    // Charge the user for the order total which is the prorated cost for the subscription.
                    _paymentRepository.ChargeProrationFee(stripeCustomerID, proratedOrder.OrderTotal);
                    // Save the order.
                    int? orderIdentifier = _orderRepository.SaveOrder(proratedOrder, true, currentUserID);
                    // Save the order payment if the order saved successfully.
                    if (orderIdentifier.HasValue)
                    {
                        // Retrieve the credit card details for this purchase.
                        var card = _paymentRepository.GetUserDefaultPaymentMethod(ownerUserID).FirstOrDefault();
                        var paymentTypeIDCreditCard = _orderRepository.GetCreditCardPaymentTypeID();
                        if (paymentTypeIDCreditCard.HasValue && card != null)
                        {
                            Payment payment = new Payment
                            {
                                PaymentTypeID        = paymentTypeIDCreditCard.Value,
                                CreditCardName       = card.Name,
                                CreditCardNumber     = card.Last4,
                                CreditCardExpiration = string.Format("{0}/{1}", card.ExpirationMonth, card.ExpirationYear)
                            };
                            _orderRepository.SaveOrderPayment(payment, orderIdentifier.Value, currentUserID);
                        }
                    }
                }

                // Only update the Stripe subscription if the updateFlag is true.
                // The updateFlag is set to false if a previously manually billed listing 
                // has a free trial subscription created. This stops the Stripe subscription from being
                // updated when it's unecessary.
                if (updateFlag)
                {
                    if (!string.IsNullOrEmpty(currentSubscription.StripeSubscriptionID))
                    {
                        _paymentRepository.UpdateStripeSubscription(stripeCustomerID, currentSubscription.StripeSubscriptionID, listingSubscriptionCost, false);
                    }
                }

                string stripeSubscriptionID = newStripeSubscriptionID ?? currentSubscription.StripeSubscriptionID;
                _listingRepository.UpdateLocalSubscription(currentSubscription.SubscriptionID, listing.Directories.Count(), listing.DescriptionLines.Count(), listingSubscriptionCost, stripeSubscriptionID, currentUserID);
                SaveListing(listing, currentUserID);
                _listingRepository.ClearCancelledFlag(listing.ListingID.Value);
            }
        }

        public void CreateNewManualOrder(int listingIdentifier, int userIdentifier)
        {
            // If an unpaid prder already exists for this listing, do not create another.
            if (!_listingRepository.DoesUnpaidOrderExist(listingIdentifier))
            {
                // Retrieve the listing so we can generate an order.
                Listing listing = _listingRepository.GetSpecificListing(listingIdentifier);

                if (listing.IsListingBillable)
                {
                    // Create a new order for the based on the listing.
                    // The order will be for a brand new subscription.
                    Order order = this.CreateNewSubscriptionOrder(listing);
                    // Save the returned order in the database, setting isPaid to false.
                    _orderRepository.SaveOrder(order, false, userIdentifier);
                }
                else
                    throw new ListingServiceException("Cannot create an order for this listing. Please select at least one directory.");
            }
            else
                throw new ListingServiceException("Cannot create order. An unpaid order is associated with this listing.");
        }

        private int? PayOrderCreditCard(Order order, Payment payment, int initiatingUserIdentifier)
        {
            // Retrieve the ID for the Stripe plan to be used for subscriptions.
            string stripePlanID = _database.GetDefaultPlanID();
            int subscriptionCost = order.OrderTotal;

            if (order.ListingID.HasValue)
            {
                // Retrieve the listing ID from the order and retrieve the user ID of the owner user.
                var listingIdentifier = order.ListingID.Value;
                var userIdentifier = _listingRepository.GetListingOwnerUserID(listingIdentifier);

                if (userIdentifier.HasValue)
                {
                    // Create a new stripe customer, or retrive their existing stripe customer identifier.
                    string emailAddress = _database.GetUserEmail(userIdentifier.Value);
                    string stripeCustomerID = _paymentRepository.CreateStripeCustomer(userIdentifier.Value, emailAddress);

                    // Add the credit card to their account or update their current payment method.
                    if (!string.IsNullOrEmpty(payment.CardToken))
                        _paymentRepository.UpdatePaymentMethod(userIdentifier.Value, payment.CardToken);

                    Dictionary<string, string> metadata = new Dictionary<string, string>
                    {
                        {"ListingID", listingIdentifier.ToString(CultureInfo.InvariantCulture)}
                    };
                    // Create the new Stripe subscription.
                    _paymentRepository.CreateNewSubscription(stripeCustomerID, subscriptionCost, stripePlanID, null, metadata);
                    // The rest of this process is handled by the invoice.payment_succeeded webhook.
                    // The method that handles this is: HandleStripeSubscriptionPaid();
                }
            }
            // If we got here there was some sort of issue and the payment was never made.
            return null;
        }

        // This method is used to mark a specified order as paid, save the payment details for the order
        // and create a new local subscription in the database for the listing.
        private int? PayOrderManual(Order order, Payment payment, int initiatingUserIdentifier)
        {
            int subscriptionCost = order.OrderTotal;
            if (order.ListingID.HasValue)
            {
                var listingIdentifier = order.ListingID.Value;
                var userIdentifier = _listingRepository.GetListingOwnerUserID(listingIdentifier);
                Listing listing = _listingRepository.GetSpecificListing(listingIdentifier);

                if (userIdentifier.HasValue)
                {
                    DateTime periodStart = DateTime.Now.ToUniversalTime();
                    DateTime periodEnd = DateTime.Now.ToUniversalTime().AddYears(1);

                    this.CreateNewLocalSubscription(listingIdentifier, subscriptionCost, listing.Directories.Count(), listing.DescriptionLines.Count(), order.OrderID, null, initiatingUserIdentifier, periodStart, periodEnd);

                    var paymentIdentifier = _orderRepository.SaveOrderPayment(payment, order.OrderID, initiatingUserIdentifier);
                    // Set the type to Manual.
                    _listingRepository.UpdateListingType(listingIdentifier, "Manual");
                    return paymentIdentifier;
                }
            }
            // If we got here there was some sort of issue and the payment was never made.
            return null;
        }

        // This service method determines which payment method should be used.
        // This determined by the payment type. Credit card payments will use Stripe.
        public int? PayOrder(Order order, Payment payment, bool isAdministrator, int initiatingUserIdentifier)
        {
            int? paymentIdentifier = null;
            // Retrieve the payment types available for the user.
            var paymentTypes = _orderRepository.GetPaymentTypes(isAdministrator);
            // Figure out what type of payment has been made.
            PaymentType selectedPaymentType = paymentTypes.FirstOrDefault(x => x.PaymentTypeID == payment.PaymentTypeID);
            // Determine which pay method to use based on the selected payment type.
            if (selectedPaymentType != null)
            {
                if (selectedPaymentType.Type.Equals("Credit Card"))
                {
                    // This method may throw a StripeException.
                    paymentIdentifier = PayOrderCreditCard(order, payment, initiatingUserIdentifier);
                }
                if (selectedPaymentType.Type.Equals("Check") || selectedPaymentType.Type.Equals("ACH"))
                {
                    paymentIdentifier = PayOrderManual(order, payment, initiatingUserIdentifier);
                }
            }
            return paymentIdentifier;
        }

        /// Given a listing without a subscription, an order record is created.
        public Order CreateNewSubscriptionOrder(Listing listing)
        {
            // Retrieve the directories available in the site.
            var siteDirectories = _database.GetDirectories().ToList();

            Order order = new Order();
            // Create a line item for each directory associated with the listing.
            foreach (var directory in listing.Directories)
            {
                var siteDirectory = siteDirectories.FirstOrDefault(d => d.DirectoryID == directory.DirectoryID);
                if (siteDirectory != null)
                {
                    OrderLineItem item = new OrderLineItem
                    {
                        // Create line item description text.
                        Description = string.Format("{0} with {1} description lines.", siteDirectory.Name, listing.DescriptionLines.Count()),
                        // Calculate the cost of the directory with the selected number of description lines.
                        LineItemTotal = _listingCalculator.CalculateDirectoryCost(1, listing.DescriptionLines.Count())
                    };
                    order.OrderLineItems.Add(item);
                }
            }
            // Calculate the entire subscription cost.
            var listingCost = _listingCalculator.CalculateListingCost(listing);
            order.ListingID = listing.ListingID;
            order.OrderTotal = listingCost;
            return order;
        }

        public Order CreateProratedSubscriptionOrder(Listing listing, Subscription currentsubscription, int userID)
        {
            Order order = new Order();
            OrderLineItem item = new OrderLineItem
            {
                Description = string.Format("Subscription Modification: Directory count of {0} and Description count of {1}.", listing.Directories.Count(), listing.DescriptionLines.Count()),
                LineItemTotal = this.CalculateProratedAmount(listing, currentsubscription)
            };
            order.OrderLineItems.Add(item);
            var listingCost = this.CalculateProratedAmount(listing, currentsubscription);

            order.ListingID = listing.ListingID;
            order.OrderTotal = listingCost;

            return order;
        }

        private int CalculateProratedAmount(Listing listing, Subscription currentSubscription)
        {
            // Calculate the cost of the plan that has been passed.
            var cost = _listingCalculator.CalculateListingCost(listing);
            // If the cost of the plan is less than or equal to the current plan, there should be no proration.
            // This is because they've already spent enough money to cover their plan change.
            if (cost <= currentSubscription.MaxSubscriptionCost)
            {
                return 0;
            }

            var costDifference = (cost - currentSubscription.MaxSubscriptionCost);
            double costPerDay = (costDifference / 36500.00);
            var today = DateTime.Now.ToUniversalTime();
            var daysRemaining = ((currentSubscription.EndDate - today).TotalDays - 1);

            double prorationAmount = Math.Round((costPerDay * daysRemaining), 2);

            return (int)(prorationAmount * 100);
        }

        // This method checks to make sure if a listing has been changed in a way that requires
        // the subscription to be modified. If the user has only edited their listing information
        // and not fields that require charges we can use this method to determine that and just
        // save the listing.
        public bool HasListingSubscriptionChanged(Listing listing)
        {
            if (listing.ListingID.HasValue)
            {
                var currentSubscription = _listingRepository.GetCurrentSubscription(listing.ListingID.Value);
                if (currentSubscription != null)
                {
                    int directoryCount = listing.Directories.Count();
                    int descriptionCount = listing.DescriptionLines.Count();

                    if ((directoryCount == currentSubscription.CurrentDirectoryCount) && (descriptionCount == currentSubscription.CurrentDescriptionLinesCount))
                    {
                        return false;
                    }
                    // If the directory count and description count have been changed.
                    return true;
                }
                // If there is no current subscription for this existing listing, it needs to be charged.
                return true;
            }
            // If the listing does not have a listing identifier it is a new listing.
            // A subscription purchase is required.
            return true;
        }

        public bool HandleStripeSubscriptionPaid(StripeInvoice stripeInvoice)
        {
            try
            {
                // Make sure the stripe invoice that is sent is associated with a customer and it is for a subscription.
                // We don't care about invoices for other events. Only need to know when a subscription is paid.
                if (!string.IsNullOrEmpty(stripeInvoice.CustomerId) && !string.IsNullOrEmpty(stripeInvoice.SubscriptionId))
                {
                    // Retrieve the subscription that the invoice.payment_succeeded is associated with.
                    var stripeSubscriptionID = stripeInvoice.SubscriptionId;
                    StripeSubscription stripeSubscription = _paymentRepository.GetStripeSubscription(stripeInvoice.CustomerId, stripeInvoice.SubscriptionId);
                    if (stripeSubscription != null && (stripeSubscription.PeriodStart.HasValue && stripeSubscription.PeriodEnd.HasValue))
                    {
                        // If the subscription does not exist we need to create a new subscription.
                        if (!_listingRepository.DoesSubscriptionExist(stripeSubscriptionID, stripeSubscription.PeriodStart.Value, stripeSubscription.PeriodEnd.Value) && stripeInvoice.Total > 0)
                        {
                            // Retrieve the cost of the subscription from the invoice.
                            int subscriptionCost = stripeInvoice.Total;
                            string listingIdentifierString;
                            if (stripeSubscription.Metadata.TryGetValue("ListingID", out listingIdentifierString)) // Returns true.
                            {
                                int listingIdentifier;
                                if (Int32.TryParse(listingIdentifierString, out listingIdentifier))
                                {
                                    this._webhookListingIdentifier = listingIdentifier;
                                    Listing listing = _listingRepository.GetSpecificListing(listingIdentifier);
                                    int directoryCount = listing.Directories.Count();
                                    int descriptionCount = listing.DescriptionLines.Count();
                                    // todo need to set this to a correct user.
                                    var webhookUserID = 1;

                                    // If an unpaid order exists, a manual order was generated and a user or admin paid the order with a credit card.
                                    // This means we don't want to create another order, we just want to save a payment associated with the order.
                                    int? orderIdentifier;
                                    if (_listingRepository.DoesUnpaidOrderExist(listingIdentifier))
                                        orderIdentifier = _orderRepository.GetUnpaidOrderIdentifierForListing(listingIdentifier);
                                    else
                                    {
                                        Order order = CreateSubscriptionOrder(listingIdentifier, directoryCount, descriptionCount, subscriptionCost);
                                        orderIdentifier = _orderRepository.SaveOrder(order, true, webhookUserID);
                                    }

                                    if (orderIdentifier.HasValue)
                                    {
                                        var card = _paymentRepository.GetUserDefaultPaymentMethod(stripeInvoice.CustomerId).FirstOrDefault();
                                        // Retrieve the ID of the credit card payment type.
                                        var paymentTypeIDCreditCard = _orderRepository.GetCreditCardPaymentTypeID();
                                        if (paymentTypeIDCreditCard.HasValue && card != null)
                                        {
                                            Payment payment = new Payment
                                            {
                                                PaymentTypeID = paymentTypeIDCreditCard.Value,
                                                CreditCardName = card.Name,
                                                CreditCardNumber = card.Last4,
                                                CreditCardExpiration = string.Format("{0}/{1}", card.ExpirationMonth, card.ExpirationYear)
                                            };
                                            // Save the payment associated with the order.
                                            _orderRepository.SaveOrderPayment(payment, orderIdentifier.Value, webhookUserID);
                                            // Create the new local subscription based off the details of the Stripe subscription.
                                            CreateNewLocalSubscription(listingIdentifier, subscriptionCost, directoryCount, descriptionCount, orderIdentifier.Value, stripeSubscriptionID, webhookUserID, stripeSubscription.PeriodStart.Value, stripeSubscription.PeriodEnd.Value);
                                            // Update the listing type to "Online" because payment is handled by Stripe now.
                                            _listingRepository.UpdateListingType(listingIdentifier, "Online");
                                        }
                                        // If the default payment method could not be retrieved.
                                        else throw new Exception();
                                    }
                                    // If an order could not be created.
                                    else throw new Exception();
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // This private method is used to create a subscription order based off the information sent with the invoice.payment_succeeded event.
        // todo need to make this like the other createsubscriptionorder method. THIS CAN BE LEFT THE SAME.
        private Order CreateSubscriptionOrder(int listingIdentifier, int dirCount, int desCount, int subCost)
        {
            OrderLineItem line = new OrderLineItem
            {
                Description = string.Format("Subscription Purchase: Directory count of {0} and Description count of {1}.", dirCount, desCount)
            };
            Order order = new Order
            {
                ListingID = listingIdentifier,
                OrderTotal = subCost
            };
            order.OrderLineItems.Add(line);
            return order;
        }

        public int? GetWebhookListingID()
        {
            return this._webhookListingIdentifier;
        }
    }
}
