﻿CREATE TABLE dbo.Subscriptions
(
    SubscriptionID INT NOT NULL PRIMARY KEY IDENTITY(1, 1),
    ListingID INT NOT NULL,
	OrderID INT NOT NULL,
    StripeSubscriptionID NVARCHAR(50) NULL,
	CurrentDirectoryCount INT NOT NULL,
	CurrentDescriptionLinesCount INT NOT NULL,
	PaidDirectoryCount INT NOT NULL,
	PaidDescriptionLinesCount INT NOT NULL,
    StartDate DATETIME2 NOT NULL, 
    EndDate DATETIME2 NOT NULL,
	CancelledDate DATETIME2,
    SubscriptionCost INT NOT NULL,
	MaxSubscriptionCost INT NOT NULL,
	SubscriptionStatusID INT NOT NULL,
    CreateDate DATETIME2 NOT NULL, 
    EditDate DATETIME2 NOT NULL, 
    CreateUser INT NOT NULL, 
    EditUser INT NOT NULL, 
    CONSTRAINT FK_SUBSCRIPTIONS_LISTINGID FOREIGN KEY(ListingID) REFERENCES dbo.Listings(ListingID),
	CONSTRAINT FK_SUBSCRIPTIONS_ORDERID FOREIGN KEY(OrderID) REFERENCES dbo.Orders(OrderID),
    CONSTRAINT FK_SUBSCRIPTIONS_SUBSCRIPTION_STATUS_ID FOREIGN KEY(SubscriptionStatusID) REFERENCES dbo.SubscriptionStatuses(SubscriptionStatusID)
);
