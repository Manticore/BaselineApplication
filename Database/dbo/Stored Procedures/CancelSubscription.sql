﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure sets the cancelled date for the current subscription record
-- as well the cancelled date for the listing.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.CancelSubscription
       @SubscriptionID INT
AS
BEGIN
    SET NOCOUNT ON;

	-- Retrieve the current date.
    DECLARE @CurrentDate DATETIME = dbo.GetCurrentDate();

    -- Set the cancelled date field on the specified subscription.
    UPDATE dbo.Subscriptions
           SET CancelledDate = @CurrentDate
    WHERE SubscriptionID = @SubscriptionID;


    DECLARE @ListingID INT;
    -- Retrieve the listing ID for the associated subscription.
    SELECT @ListingID = ListingID
    FROM dbo.Subscriptions
    WHERE SubscriptionID = @SubscriptionID;
    
    
    -- Mark the associated listing in the database as cancelled as well.
    -- This will help simplify queries.
    UPDATE dbo.Listings
           SET CancelledDate = @CurrentDate
    WHERE ListingID = @ListingID;

END;