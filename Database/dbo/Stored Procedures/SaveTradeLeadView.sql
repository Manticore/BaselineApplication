﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure increments the Views column for a Trade lead.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.SaveTradeLeadView
       @TradeleadID INT
AS
BEGIN

    SET NOCOUNT ON;

    UPDATE dbo.TradeLeads
           SET [Views] = [Views] + 1
    WHERE TradeLeadID = @TradeleadID;

END; 