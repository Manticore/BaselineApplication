﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure retrieves the full details for a given listing. It's entire
-- subscription and order history. As well as many other pertinent details.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetSpecificListingFull
       @ListingID INT
AS
BEGIN

	SET NOCOUNT ON;

    -- Retrieve the specified listing.
    SELECT TOP 1 ListingID, 
                 BusinessName, 
                 BusinessContact, 
				 ContactEmail,
                 ContactNumber,
				 FaxNumber,
                 [Description], 
                 ProductDescription, 
                 Website, 
                 L.UserID, 
				 UP.UserName AS OwnerUserName,
				 SC.StripeCustomerID,
                 LT.ListingTypeID,
				 LT.[Type],
				 CancelledDate, 
                 CreateDate, 
                 EditDate, 
                 CreateUser, 
                 EditUser
    FROM dbo.Listings L
		INNER JOIN dbo.ListingTypes LT 
			ON L.ListingTypeID = LT.ListingTypeID
		LEFT JOIN UserProfile UP 
			ON UP.UserId = L.UserID
		LEFT JOIN dbo.StripeCustomers SC
			ON UP.UserId = SC.UserID
    WHERE L.ListingID = @ListingID;

    -- Return the industry tags associated with this listing.
    SELECT I.IndustryID, 
           I.Name
    FROM dbo.Listings_Industries LI 
		INNER JOIN Industries I
             ON I.IndustryID = LI.IndustryID
    WHERE LI.ListingID = @ListingID;

    -- Return the addresses for the selected listings.
    SELECT LA.ListingID, 
           A.AddressID, 
           A.Street1, 
           A.Street2, 
           A.City, 
           A.State, 
           A.StateID, 
           A.Zip
    FROM dbo.Listings_Addresses LA 
		INNER JOIN dbo.v_Addresses A
             ON LA.AddressID = A.AddressID
    WHERE LA.ListingID = @ListingID;

	-- Return the directories for this listing.
    SELECT LD.ListingID,
		   LD.DirectoryID,
		   D.Name
    FROM dbo.Listings_Directories LD
	INNER JOIN Directories D 
		ON LD.DirectoryID = D.DirectoryID
    WHERE LD.ListingID = @ListingID;

    -- Return the description lines for this listing.
    SELECT D.DescriptionID, 
           D.Code, 
           D.Body
    FROM dbo.Descriptions D
    WHERE D.ListingID = @ListingID;

	-- Grab the current subscription for the lisitng.
	SELECT  [SubscriptionID],
			[ListingID],
			[OrderID],
			[StripeSubscriptionID],
			[CurrentDirectoryCount],
			[PaidDirectoryCount],
			[CurrentDescriptionLinesCount],
			[PaidDescriptionLinesCount],
			[StartDate],
			[EndDate],
			[CancelledDate],
			[SubscriptionCost],
			[MaxSubscriptionCost],
			[SubscriptionStatusID],
			[CreateDate],
			[EditDate],
			[CreateUser],
			[EditUser]
	FROM dbo.GetCurrentSubscriptions() 
	WHERE ListingID = @ListingID;

    -- Retrieve all the orders for the specified listing.
    SELECT OrderID, 
           ListingID,
		   OrderNumber, 
           OrderTotal, 
           OS.OrderStatusID, 
		   OS.[Status],
           CreateDate, 
           EditDate, 
           CreateUser, 
           EditUser 
	INTO #Orders
    FROM dbo.Orders O 
	INNER JOIN dbo.OrderStatuses OS 
		ON O.OrderStatusID = OS.OrderStatusID
    WHERE ListingID = @ListingID;

    SELECT * FROM #Orders
	ORDER BY CreateDate DESC; 

    -- Retrieve all the order line items for all associated orders.
    SELECT OrderLineItemID, 
           OrderID,
           [Description]
    FROM dbo.OrderLineItems
    WHERE OrderID IN(SELECT OrderID FROM #Orders);

    -- Get all the subscriptions for the listing.
    SELECT SubscriptionID, 
           ListingID, 
           OrderID, 
           StripeSubscriptionID, 
		   PaidDirectoryCount,
           PaidDescriptionLinesCount,
           StartDate, 
           EndDate, 
           SubscriptionCost,
		   MaxSubscriptionCost,
		   SubscriptionStatusID,
           CreateDate, 
           EditDate, 
           CreateUser, 
           EditUser
    FROM dbo.Subscriptions
    WHERE ListingID = @ListingID
    ORDER BY OrderID DESC;


	-- Get all the notes that have been assciated with this listing.
	SELECT N.NoteID, 
		   N.ListingID, 
		   N.NoteText, 
		   N.CreateDate, 
		   N.CreateUser,
		   UP.UserName AS CreateUsername
	FROM dbo.Notes N 
		INNER JOIN dbo.UserProfile UP
			 ON N.CreateUser = UP.UserId
	WHERE ListingID = @ListingID;

    DROP TABLE #Orders;

END;