﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the details for a specified order.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetOrder
       @OrderID INT
AS
BEGIN

	SET NOCOUNT ON;

    -- Get the order information.
    SELECT OrderID, 
           ListingID,
		   OrderNumber,
           OrderTotal,
		   OS.OrderStatusID,
		   OS.Status,
           CreateDate
    FROM dbo.Orders O 
		INNER JOIN dbo.OrderStatuses OS 
			ON OS.OrderStatusID = O.OrderStatusID
    WHERE O.OrderID = @OrderID;

	-- Return the line items for the specified order.
	SELECT [OrderLineItemID],
		   [OrderID],
		   [Description],
		   [LineItemTotal]
	FROM dbo.OrderLineItems 
	WHERE OrderID = @OrderID

    -- Return the payment information if it exists.
    SELECT PaymentID, 
           PaymentDate, 
           PaymentDetails, 
           PT.PaymentTypeID, 
           PT.[Type],
		   OrderID, 
		   CreditCardName,
		   CreditCardNumber,
		   CreditCardExpiration,
		   CheckBankName,
		   CheckName,
		   CheckRoutingNumber,
		   CheckAccountNumber,
		   CheckNumber,
		   TransferBankName,
		   TransferName,
		   TransferRoutingNumber,
		   TransferAccountNumber,
		   TransferCheckNumber,
           CreateDate, 
           EditDate, 
           CreateUser, 
           EditUser
    FROM dbo.Payments P 
	   INNER JOIN dbo.PaymentTypes PT
             ON PT.PaymentTypeID = P.PaymentTypeID
    WHERE OrderID = @OrderID;

	-- Retrieve the invoicing dates for this order.
	SELECT [InvoiceID], 
		   [OrderID], 
		   [CreateDate], 
		   CreateUser, 
		   UP.[UserName] AS CreateUserName
	FROM dbo.InvoiceDates ID 
		INNER JOIN dbo.UserProfile UP
			 ON UP.UserId = ID.CreateUser
	WHERE OrderID = @OrderID
	ORDER BY [CreateDate] DESC;

END; 