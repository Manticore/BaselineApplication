﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Library.Exceptions;
using Models.Interfaces;
using Models.Models;
using Services;
using TemplateApplication.Models.ViewModels;
using TemplateApplication.Filters;
using WebMatrix.WebData;

namespace TemplateApplication.Controllers
{
    [InitializeSimpleMembership]
    [ValidateAntiForgeryTokenOnAllPosts]
    public class TradeLeadsController : Controller
    {
        private const int StartingPage         = 1;
        private const int DefaultNumberRecords = 25;

        private readonly ITradeLeadRepository _tradeLeadRepository;
        private readonly IDatabaseContext     _database;

        // Repository and database access classes injected using dependency injection.
        public TradeLeadsController(ITradeLeadRepository tradeLeadRepository, IDatabaseContext database)
        {
            _tradeLeadRepository = tradeLeadRepository;
            _database            = database;
        }

        // View the first page of results for the trade lead listing page.
        public ActionResult Index(string keyword, DateTime? start, DateTime? end, int? industry)
        {
            // If an industry ID has been passed via GET parameter...
            List<int?> industryList = new List<int?>();
            if (industry.HasValue)
            {
                industryList.Add(industry.Value);
            }

            TradeLeadSummaryViewModel model = new TradeLeadSummaryViewModel
            {
                PassedKeyword    = keyword,
                PassedStartDate  = start,
                PassedEndDate    = end,
                PassedIndustryID = industry,
                // Top trade lead records to be displayed in the sidebar.
                TopTradeLeads = _tradeLeadRepository.GetTopTradeLeads(),
                // Return the paged trade leads object. Contains a list of Trade Leads to display and the total number for paging.
                TradeLeads = _tradeLeadRepository.GetTradeLeads(StartingPage, DefaultNumberRecords, keyword, industryList, null, null),
                // Get the list of industries to be displayed in the search control.
                Industries = _database.GetIndustries(),
                IndustryCounts =_database.GetTradeLeadIndustryCount()
            };
            return View(model);
        }

        // View the details for a specific trade lead.
        public ActionResult Details(int id)
        {
            // Increment the view column for this trade lead.
            _tradeLeadRepository.SaveTradeLeadView(id);
            // Get the requested Trade Lead by ID.
            TradeLead tradeLead = _tradeLeadRepository.GetSpecificTradeLead(id);

            TradeLeadDetailsViewModel model = new TradeLeadDetailsViewModel
            {
                TradeLead = tradeLead
            };
            return View(model);
        }

        // Action method for the Create Trade Lead page.
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            ViewBag.ButtonText = "Create Trade Lead";
            TradeLeadCreateEditViewModel model = new TradeLeadCreateEditViewModel
            {
                TradeLead  = null,
                Industries = _database.GetIndustries()
            };
            return View(model);
        }

        // Action method used to edit a specified Trade Lead.
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id)
        {
            ViewBag.ButtonText = "Save Trade Lead";
            TradeLeadCreateEditViewModel model = new TradeLeadCreateEditViewModel
            {
                TradeLead  = _tradeLeadRepository.GetSpecificTradeLead(id),
                Industries = _database.GetIndustries()
            };
            return View(model);
        }

        #region POST Methods

        // POST Method to return new page of trade lead records without a page refresh.
        [HttpPost]
        public ActionResult GetTradeLeadResults(int pageNumber, int numberPerPage, TradeLeadSearchViewModel search)
        {
            var tradeLeads = _tradeLeadRepository.GetTradeLeads(pageNumber, numberPerPage, search.Keyword, search.IndustryIDs, search.Start, search.End);
            return PartialView("_TradeLeadSummary", tradeLeads);
        }

        // POST method returning the quick view details for a trade lead.
        [HttpPost]
        public ActionResult GetQuickViewDetail(int tradeLeadID)
        {
            // Save a view for the trade lead.
            _tradeLeadRepository.SaveTradeLeadView(tradeLeadID);
            // Retrieve the lead detail and return HTML.
            var tradeLead = _tradeLeadRepository.GetSpecificTradeLead(tradeLeadID);
            return PartialView("_TradeLeadDetail", tradeLead);
        }

        // POST method used to save a new or existing trade lead.
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public JsonResult UpsertTradeLead(TradeLead tradeLead)
        {
            try
            {
                int userID = WebSecurity.GetUserId(User.Identity.Name);
                TradeLeadService service = new TradeLeadService(_tradeLeadRepository);
                service.SaveTradeLead(tradeLead, userID);
            }
            catch (TradeLeadValidationException exception)
            {
                return Json(exception.Message);
            }
            return Json("Trade Lead saved successfully.");
        }

        #endregion
    }
}