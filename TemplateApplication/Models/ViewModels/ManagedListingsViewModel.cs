﻿using System.Collections.Generic;
using Models;
using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class ManagedListingsViewModel
    {
        public PagedList<ManagedListingsModel> ManagedListings { get; set; }
        public IEnumerable<ListingType> ListingTypes { get; set; }
    }
}