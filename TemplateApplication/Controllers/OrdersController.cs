﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using DataTables.Mvc;
using Ionic.Zip;
using iTextSharp.text;
using Library;
using Models;
using Models.Interfaces;
using Models.Models;
using MvcRazorToPdf;
using Rotativa.MVC;
using Stripe;
using TemplateApplication.Filters;
using TemplateApplication.Models.ViewModels;
using WebMatrix.WebData;

namespace TemplateApplication.Controllers
{
    [InitializeSimpleMembership]
    public class OrdersController : Controller
    {
        private readonly IListingService _listingService;
        private readonly IOrderRepository _orderRepository;
        private readonly IDatabaseContext _database;

        private const int StartingPage = 1;
        private const int DefaultNumberOfRecords = 10;

        public OrdersController(IListingService listingService, IOrderRepository orderRepository, IDatabaseContext database)
        {
            _listingService = listingService;
            _orderRepository = orderRepository;
            _database = database;
        }

        // This is the action method for the "Billed Customers" utility.
        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            OrdersParameterSet parameters = new OrdersParameterSet
            {
                Page = StartingPage,
                NumberOfRecords = DefaultNumberOfRecords
            };

            ViewOrdersModel model = new ViewOrdersModel
            {
                // Retrieve the orders currently in the site.
                Orders = _orderRepository.GetOrdersPaged(parameters),
                // Retrieve the order statuses available in the site.
                OrderStatuses = _orderRepository.GetOrderStatuses().ToList()
            };
            return View(model);
        }

        // Action method for the Order deletion functionality.
        // Can only be used to delete an unpaid order.
        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int id)
        {
            Order order = _orderRepository.GetOrder(id);
            DeleteOrderViewModel model = new DeleteOrderViewModel
            {
                Order = order,
                OrderInvoiced = order.InvoiceDates.Any()
            };
            return View(model);
        }

        // Post method used to delete an unpaid order.
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult DeleteUnpaidOrder(int orderIdentifier)
        {
            try
            {
                _orderRepository.DeleteUnpaidOrder(orderIdentifier);
            }
            catch (Exception)
            {
                return View();
            }
            return RedirectToAction("Index", "Orders");
        }

        // Action method to pay an order in the site.
        // An order can be paid by an administrator, or by the order owner.
        // Administrators have access to more payment methods for the order.
        // If a normal site user pays their order, they can only pay it using a credit card.
        [Authorize]
        public ActionResult Pay(int id)
        {
            // Retrieve the selected order from the database.
            var order = _orderRepository.GetOrder(id);
            // Make sure the order is unpaid before rendering the pay view.
            if (!order.Status.Equals("Unpaid"))
                return RedirectToAction("Index", "Home");

            // Only render the view if the user accessing the pay utility is an admin
            // or a site user paying an order associated with their listing.
            var isAdministrator = User.IsInRole("Administrator");
            int currentUserID = WebSecurity.GetUserId(User.Identity.Name);
            if (isAdministrator || _orderRepository.IsUserOrderOwner(currentUserID, id))
            {
                // If the user is an administrator, then use the admin layout view.
                // Use the normal site layout if not.
                ViewBag.Layout = isAdministrator ? "~/Views/Shared/_AdminLayout.cshtml" : "~/Views/Shared/_Layout.cshtml";

                // Set up the view model for the page.
                PayOrderViewModel model = new PayOrderViewModel
                {
                    Id = id,
                    Order = order,
                    // Retrieve the payment methods available to the user.
                    PaymentTypes = _orderRepository.GetPaymentTypes(isAdministrator),
                    IsAdministrator = isAdministrator,
                    Payment = null,
                    States = _database.GetStates()
                };
                return View(model);
            }
            // Redirect the user to the home page if the order they are trying to pay has already been paid.
            return RedirectToAction("Index", "Home");
        }

        // This is the POST method for the pay order utility.
        // This method will mark an order as paid, associate a payment, and crete a Stripe subscription. (If paid using credit card.)
        [HttpPost]
        [Authorize]
        public ActionResult Pay(int id, PayOrderViewModel model)
        {
            // Retrieve the order identifier stored in the view model.
            Order order = _orderRepository.GetOrder(id);

            // Ensure that the order is still in unpaid status. Redirect to the home page if not.
            if (!order.Status.Equals("Unpaid"))
                return RedirectToAction("", "");

            var isAdministrator = User.IsInRole("Administrator");
            var initiatingUserID = WebSecurity.GetUserId(User.Identity.Name);

            try
            {
                int? paymentIdentifier = _listingService.PayOrder(order, model.Payment, isAdministrator, initiatingUserID);
                // Display a confirmation page if paying the order was successful.
                if(order.ListingID.HasValue)
                return RedirectToAction("Confirmation", "Orders", new { listingIdentifier = order.ListingID.Value });
            }
            catch (StripeException exception)
            {
                string errorMessage;
                switch (exception.StripeError.Code)
                {
                    case "card_declined":
                        errorMessage = "Your credit card was declined. Please try another credit card.";
                        break;
                    case "incorrect_cvc":
                        errorMessage = "The CVC code you provided was incorrect. Please try again.";
                        break;
                    case "expired_card":
                        errorMessage = "The credit card you provided is expired. Please try another credit card.";
                        break;
                    default:
                        errorMessage = "An issue occured when making a payment. Please try again.";
                        break;
                }
                ModelState.AddModelError("", errorMessage);

                // If an issue occurs we need to redisplay the order detail form.
                ViewBag.Layout = isAdministrator ? "~/Views/Shared/_AdminLayout.cshtml" : "~/Views/Shared/_Layout.cshtml";

                model.Order = _orderRepository.GetOrder(id);
                model.PaymentTypes = _orderRepository.GetPaymentTypes(isAdministrator);
                model.IsAdministrator = isAdministrator;
                model.States = _database.GetStates();
                return View(model);
            }
            return RedirectToAction("", "");
        }

        // This confirmation page is displayed after successfully paying an order.
        [Authorize]
        public ActionResult Confirmation(int listingIdentifier)
        {
            ViewBag.ListingID = listingIdentifier;
            return View();
        }

        // This is the Action method for the Print Bills utility.
        [Authorize]
        public ActionResult Print()
        {
            PrintOrdersParameterSet parameters = new PrintOrdersParameterSet
            {
                Page = StartingPage,
                NumberOfRecords = DefaultNumberOfRecords
            };
            // Retrieve all the unpaid orders for the datatable.
            PrintOrderModel model = new PrintOrderModel
            {
                OrdersForPrinting = _orderRepository.GetUnpaidOrdersForPrinting(parameters)
            };
            return View(model);
        }

        // todo I need to figure out the print bills utility below code and methods included.
        // POST method for the Print Bills utility.
        // This method creates PDF files for each order passed to it.
        // The PDFs are returned in a zipped folder.
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult DownloadForms(string commaSeperatedIdentifiers)
        {
            var currentUserID = WebSecurity.GetUserId(User.Identity.Name);
            // Parse the order identifiers passed, make sure they are valid identifiers.
            string[] stringIdentifiers = commaSeperatedIdentifiers.Split(',');
            List<int> orderIdentifiers = new List<int>();
            foreach (string stringIdentifier in stringIdentifiers)
            {
                int tmp;
                if (int.TryParse(stringIdentifier, out tmp))
                {
                    orderIdentifiers.Add(tmp);
                }
            }
            // Create the zipped directory containing the PDF bills.
            MemoryStream workStream = new MemoryStream();
            using (ZipFile zip = new ZipFile())
            {
                List<IDisposable> streams = new List<IDisposable>();
                try
                {
                    foreach (int orderIdentifier in orderIdentifiers)
                    {
                        var pdf = CreatePdfBill(orderIdentifier); //Testing(orderIdentifier);
                        MemoryStream stream = new MemoryStream(pdf);
                        zip.AddEntry(string.Format("Order_Bill_{0}.pdf", orderIdentifier), stream);
                        streams.Add(stream);
                        // Save the date the order was printed.
                        _orderRepository.SaveOrderPrintedDate(orderIdentifier, currentUserID);
                    }
                    zip.Save(workStream);
                    workStream.Position = 0;
                }
                finally
                {
                    foreach (var stream in streams)
                    {
                        stream.Dispose();
                    }
                }
            }
            return File(workStream, "application/zip", string.Format("Generated_Bills_{0}.zip", DateTime.Now.ToUniversalTime()));
        }

        //[HttpPost]
        //[Authorize(Roles = "Administrator")]
        //public ActionResult DownloadForms(string commaSeperatedIdentifiers)
        //{
        //    var currentUserID = WebSecurity.GetUserId(User.Identity.Name);
        //    // Parse the order identifiers passed, make sure they are valid identifiers.
        //    string[] stringIdentifiers = commaSeperatedIdentifiers.Split(',');
        //    List<int> orderIdentifiers = new List<int>();
        //    foreach (string stringIdentifier in stringIdentifiers)
        //    {
        //        int tmp;
        //        if (int.TryParse(stringIdentifier, out tmp))
        //        {
        //            orderIdentifiers.Add(tmp);
        //        }
        //    }

        //    //Parallel.ForEach(orderIdentifiers, identifier =>
        //    //{
        //    //    var pdfByteArray = CreatePdfBill(identifier); //Testing(orderIdentifier);
        //    //    System.IO.File.WriteAllBytes("~/PDF", pdfByteArray);
        //    //});

        //    foreach (var identifier in orderIdentifiers)
        //    {
        //        var pdfByteArray = CreatePdfBill(identifier); //Testing(orderIdentifier);
        //        System.IO.File.WriteAllBytes("PDF", pdfByteArray);
        //    }

        //    return null;
        //    //return File(workStream, "application/zip", string.Format("Generated_Bills_{0}.zip", DateTime.Now.ToUniversalTime()));
        //}

        public byte[] CreatePdfBill(int identifier)
        {
            OrderBillDataModel billDetails = _orderRepository.GetOrderDetailsForBill(identifier);
            var pdfResult = new ViewAsPdf("OrderBillView", billDetails) { FileName = string.Format("Order_Bill_{0}.pdf", identifier) };
            var binary = pdfResult.BuildPdf(this.ControllerContext);
            return binary;
        }

        public Byte[] Testing(int identifier)
        {
            OrderBillDataModel billDetails = _orderRepository.GetOrderDetailsForBill(identifier);
            byte[] pdfOutput = ControllerContext.GeneratePdf(billDetails, "IndexWithAccessToDocumentAndWriter");
            return pdfOutput;
        }

        public ActionResult IndexWithAccessToDocumentAndWriter()
        {
            return new PdfActionResult(null, (writer, document) =>
            {
                document.SetPageSize(new Rectangle(500f, 500f, 90));
                document.NewPage();
            });
        }

        #region Order Datatable Code
        /// <summary>
        /// This post method returns a paged record set of orders to the Orders datatable.
        /// </summary>
        /// <param name="request">Model sent from the data table containing state information for the table.</param>
        /// <returns>A paged record set of orders as JSON.</returns>
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public string GetOrdersPaged([ModelBinder(typeof(OrdersBinder))] OrdersRequest request)
        {
            int start = request.Start;
            int length = request.Length;
            int page = (int)Math.Ceiling((double)start / length) + 1;

            // Retrieve the sorted column if available.
            IOrderedEnumerable<Column> sort = request.Columns.GetSortedColumns();
            var column = sort.FirstOrDefault();
            string sortColumn = null;
            string sortDirection = null;
            if (column != null)
            {
                sortColumn = column.Data;
                sortDirection = column.SortDirection.ToString();
            }

            OrdersParameterSet parameters = new OrdersParameterSet
            {
                Page = page,
                NumberOfRecords = length,
                OrderNumber = request.OrderNumber,
                ListingIdentifier = request.ListingIdentifier,
                BusinessName = request.BusinessName,
                StartDate = request.StartDate,
                EndDate = request.EndDate,
                OrderStatusID = request.OrderStatusID,
                SortColumn = sortColumn,
                SortDirection = sortDirection
            };

            // Return the paged set of orders.
            var results = _orderRepository.GetOrdersPaged(parameters);
            var json = new JavaScriptSerializer().Serialize(new DataTablesResponse(request.Draw, results.PagedListItems, results.TotalRecords, results.TotalRecords));
            return json;
        }


        public class OrdersBinder : DataTablesBinder
        {
            // Override the default BindModel called by ASP.NET and make it call Bind passing the type of your
            // implementation of IDataTablesRequest:
            public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                return Bind(controllerContext, bindingContext, typeof(OrdersRequest));
            }

            // Override MapAditionalProperties so you can set your aditional data into the model:
            protected override void MapAditionalProperties(IDataTablesRequest requestModel, System.Collections.Specialized.NameValueCollection requestParameters)
            {
                var myModel = (OrdersRequest)requestModel;
                var orderNumber = Get<string>(requestParameters, "OrderNumber");
                var listingIdentifier = Get<string>(requestParameters, "ListingIdentifier");
                var businessName = Get<string>(requestParameters, "BusinessName");
                var startDate = Get<string>(requestParameters, "StartDate");
                var endDate = Get<string>(requestParameters, "EndDate");
                var orderStatusID = Get<string>(requestParameters, "OrderStatusID");

                int value;
                bool isInteger = Int32.TryParse(listingIdentifier, out value);
                if (isInteger)
                    myModel.ListingIdentifier = value;
                else
                    myModel.ListingIdentifier = null;

                myModel.BusinessName = businessName;

                long orderNumberValue;
                isInteger = Int64.TryParse(orderNumber, out orderNumberValue);
                if (isInteger)
                    myModel.OrderNumber = orderNumberValue;
                else
                    myModel.OrderNumber = null;

                myModel.StartDate = DateConverter.ConvertDate(startDate, false);
                myModel.EndDate = DateConverter.ConvertDate(endDate, true);

                myModel.OrderStatusID = string.IsNullOrEmpty(orderStatusID) ? (int?)null : Convert.ToInt32(orderStatusID);
            }
        }

        public class OrdersRequest : DefaultDataTablesRequest
        {
            public long? OrderNumber { get; set; }
            public int? ListingIdentifier { get; set; }
            public string BusinessName { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public int? OrderStatusID { get; set; }
        }


        #endregion

        #region Print Orders Datatable Code
        /// <summary>
        /// This post method returns a paged record set of orders to the Orders datatable.
        /// </summary>
        /// <param name="request">Model sent from the data table containing state information for the table.</param>
        /// <returns>A paged record set of orders as JSON.</returns>
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public string GetUnpaidOrdersForPrintingPaged([ModelBinder(typeof(PrintOrdersBinder))] PrintOrdersRequest request)
        {
            int start = request.Start;
            int length = request.Length;
            int page = (int)Math.Ceiling((double)start / length) + 1;

            // Retrieve the sorted column if available.
            IOrderedEnumerable<Column> sort = request.Columns.GetSortedColumns();
            var column = sort.FirstOrDefault();
            string sortColumn = null;
            string sortDirection = null;
            if (column != null)
            {
                sortColumn = column.Data;
                sortDirection = column.SortDirection.ToString();
            }

            PrintOrdersParameterSet parameters = new PrintOrdersParameterSet
            {
                Page = page,
                NumberOfRecords = length,
                OrderID = request.OrderID,
                OrderNumber = request.OrderNumber,
                ListingIdentifier = request.ListingIdentifier,
                BusinessName = request.BusinessName,
                CreateDateStart = request.CreateDateStart,
                CreateDateEnd = request.CreateDateEnd,
                OrderPrinted = request.OrderPrinted,
                LastPrintDateStart = request.LastPrintDateStart,
                LastPrintDateEnd = request.LastPrintDateEnd,
                SortColumn = sortColumn,
                SortDirection = sortDirection
            };
            // Return the paged set of unpaid orders for printing.
            var results = _orderRepository.GetUnpaidOrdersForPrinting(parameters);
            var json = new JavaScriptSerializer().Serialize(new DataTablesResponse(request.Draw, results.PagedListItems, results.TotalRecords, results.TotalRecords));
            return json;
        }

        public class PrintOrdersBinder : DataTablesBinder
        {
            // Override the default BindModel.
            public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                return Bind(controllerContext, bindingContext, typeof(PrintOrdersRequest));
            }

            // Override MapAditionalProperties so you can set your aditional data into the model:
            protected override void MapAditionalProperties(IDataTablesRequest requestModel, System.Collections.Specialized.NameValueCollection requestParameters)
            {
                var myModel = (PrintOrdersRequest)requestModel;
                var orderIdentifier = Get<string>(requestParameters, "OrderIdentifier");
                var orderNumber = Get<string>(requestParameters, "OrderNumber");
                var listingIdentifier = Get<string>(requestParameters, "ListingIdentifier");
                var businessName = Get<string>(requestParameters, "BusinessName");
                var createDateStart = Get<string>(requestParameters, "CreateDateStart");
                var createDateEnd = Get<string>(requestParameters, "CreateDateEnd");
                var orderPrinted = Get<string>(requestParameters, "OrderPrinted");
                var lastPrintDateStart = Get<string>(requestParameters, "LastPrintDateStart");
                var lastPrintDateEnd = Get<string>(requestParameters, "LastPrintDateEnd");

                int value;
                bool isInteger = Int32.TryParse(orderIdentifier, out value);
                if (isInteger)
                    myModel.OrderID = value;
                else
                    myModel.OrderID = null;

                long orderNumberValue;
                bool isLong = Int64.TryParse(orderNumber, out orderNumberValue);
                if (isLong)
                    myModel.OrderNumber = orderNumberValue;
                else
                    myModel.OrderNumber = null;

                isInteger = Int32.TryParse(listingIdentifier, out value);
                if (isInteger)
                    myModel.ListingIdentifier = value;
                else
                    myModel.ListingIdentifier = null;

                myModel.BusinessName = businessName;
                myModel.CreateDateStart = DateConverter.ConvertDate(createDateStart, false);
                myModel.CreateDateEnd = DateConverter.ConvertDate(createDateEnd, true);
                myModel.OrderPrinted = string.IsNullOrEmpty(orderPrinted) ? (bool?)null : Convert.ToBoolean(orderPrinted);
                myModel.LastPrintDateStart = DateConverter.ConvertDate(lastPrintDateStart, false);
                myModel.LastPrintDateEnd = DateConverter.ConvertDate(lastPrintDateEnd, true);
            }
        }

        public class PrintOrdersRequest : DefaultDataTablesRequest
        {
            public int? OrderID { get; set; }
            public long? OrderNumber { get; set; }
            public int? ListingIdentifier { get; set; }
            public string BusinessName { get; set; }
            public DateTime? CreateDateStart { get; set; }
            public DateTime? CreateDateEnd { get; set; }
            public bool? OrderPrinted { get; set; }
            public DateTime? LastPrintDateStart { get; set; }
            public DateTime? LastPrintDateEnd { get; set; }
        }


        #endregion

        #region POST Methods
        /// <summary>
        /// This post method returns an order, rendered using the _OrderDetail partial view.
        /// </summary>
        /// <param name="orderID">The ID for a specified order record in the database.</param>
        /// <returns>Rendered HTML containing the order details.</returns>
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult GetOrderDetailsHTML(int orderID)
        {
            var order = _orderRepository.GetOrder(orderID);
            return PartialView("_OrderDetail", order);
        }

        #endregion
    }
}