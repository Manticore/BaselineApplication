﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns all the "Bulk" listings from the database. "Bulk" 
-- listings are listings that have been loaded into the site, but are not 
-- associated with any users and they are not billed. These listings are also
-- used as a source for the fax blasts. When a user responds to a fax blast
-- indicating they would like their listing to appear in the site, this
-- procedure returns the "Bulk" listings so a user can convert the listing to
-- the "Manual" type. When this occurs a user is associated with the listing
-- and the listing will appear in the billing utilities.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetBulkListings
       @Page			INT, 
       @NumberOfRecords	INT,
	   @ListingID		INT,
	   @BusinessName	NVARCHAR(250),
	   @FaxNumber		NVARCHAR(15),
	   @SortColumn		NVARCHAR(50)  = NULL,
	   @SortDirection	NVARCHAR(10)  = NULL
AS
BEGIN

	SET NOCOUNT ON;

	-- Calculate the paging Offset.
    DECLARE @OFFSET INT = (@Page - 1) * @NumberOfRecords;

    DECLARE @BulkListingTypeID INT;
    SELECT @BulkListingTypeID = ListingTypeID
    FROM dbo.ListingTypes
    WHERE Type = 'Bulk';

	DECLARE @BulkListings TABLE
    (
	   ListingID INT, 
	   BusinessName NVARCHAR(250),
	   FaxNumber NVARCHAR(15),
	   ListingCreateDate DATETIME2,
	   TotalRecords INT
    );

	INSERT INTO @BulkListings 
	(
		ListingID,
		BusinessName,
		FaxNumber,
		ListingCreateDate,
		TotalRecords
	)
    SELECT ListingID, 
           BusinessName, 
           FaxNumber, 
           CreateDate,
		   TotalRecords = COUNT(1) OVER()
    FROM dbo.Listings L
    WHERE ListingTypeID = @BulkListingTypeID
		AND (@ListingID IS NULL OR L.ListingID = @ListingID)
		AND (@BusinessName IS NULL OR (L.BusinessName LIKE '%'+ @BusinessName + '%'))
	ORDER BY
		CASE WHEN @SortColumn = 'ListingID' AND @SortDirection = 'Ascendant'
			  THEN L.ListingID END ASC, 
		CASE WHEN @SortColumn = 'ListingID' AND @SortDirection = 'Descendant' 
			  THEN L.ListingID END DESC,
		CASE WHEN ((@SortColumn IS NULL) OR (@SortColumn = 'BusinessName' AND @SortDirection = 'Ascendant'))
			  THEN L.BusinessName END ASC, 
		CASE WHEN @SortColumn = 'BusinessName' AND @SortDirection = 'Descendant' 
			  THEN L.BusinessName END DESC
	OFFSET @OFFSET ROWS FETCH NEXT @NumberOfRecords ROWS ONLY;

	-- Return the total number of records for paging.
    SELECT TOP 1 TotalRecords
    FROM @BulkListings;

    -- Return the listings.
    SELECT ListingID, 
           BusinessName,
		   FaxNumber,
		   ListingCreateDate
    FROM @BulkListings;

END;