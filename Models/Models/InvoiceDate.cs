﻿using System;

namespace Models.Models
{
    public class InvoiceDate
    {
        public DateTime CreateDate     { get; set; }
        public string   CreateDateDisplay { get { return CreateDate.ToShortDateString(); } }
        public int      CreateUser     { get; set; }
        public string   CreateUserName { get; set; }
        public string   CreateUserNameDisplay { get { return CreateUserName.ToUpper(); } }
        public int      InvoiceID { get; set; }
        public int      OrderID   { get; set; }
    }
}