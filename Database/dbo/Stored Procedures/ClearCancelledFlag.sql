﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure clears the "Cancelled" flag on a listing, given a specific
-- listing identifier.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.ClearCancelledFlag
       @ListingID INT
AS
BEGIN

    UPDATE dbo.Listings
           SET CancelledDate = NULL
    WHERE ListingID = @ListingID;

END;