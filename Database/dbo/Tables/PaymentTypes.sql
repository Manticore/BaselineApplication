﻿CREATE TABLE [dbo].[PaymentTypes]
(
	[PaymentTypeID] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[Type] NVARCHAR(50) NOT NULL, 
    [IsAdminType] BIT NULL
)
