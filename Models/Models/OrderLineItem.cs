﻿namespace Models.Models
{
    public class OrderLineItem
    {
        public int OrderLineItemID { get; set; }
        public int OrderID { get; set; }
        public string Description { get; set; }
        public int LineItemTotal { get; set; }

        public string LineItemTotalDisplay
        {
            get
            {
                return string.Format("${0:0.00}", (LineItemTotal * 0.01));
            }
        }
    }
}