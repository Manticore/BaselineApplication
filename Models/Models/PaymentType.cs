﻿namespace Models.Models
{
    public class PaymentType
    {
        public bool   IsAdminType   { get; set; }
        public int    PaymentTypeID { get; set; }
        public string Type          { get; set; }
    }
}