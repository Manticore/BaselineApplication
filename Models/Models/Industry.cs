﻿namespace Models.Models
{
    public class Industry
    {
        public int? IndustryID { get; set; }
        public string Name { get; set; }
        public int ListingID { get; set; }
        public int TradeLeadID { get; set; }
        public int IndustryCount { get; set; }
    }
}