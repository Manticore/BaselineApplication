﻿using System;

namespace Models
{
    public class OrdersParameterSet
    {
        public int Page { get; set; }
        public int NumberOfRecords { get; set; }
        public long? OrderNumber { get; set; }
        public int? ListingIdentifier { get; set; }
        public string BusinessName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? OrderStatusID { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
    }
}
