﻿using System;

namespace Models
{
    public class BillableListingParameterSet
    {
        public int Page { get; set; }
        public int NumberOfRecords { get; set; }
        public int? ListingIdentifier { get; set; }
        public string BusinessName { get; set; }
        public DateTime? ListingCreateDateStart { get; set; }
        public DateTime? ListingCreateDateEnd { get; set; }
        public DateTime? LastSubscriptionDateStart { get; set; }
        public DateTime? LastSubscriptionDateEnd { get; set; }
        public bool? OrderGenerated { get; set; }
        public DateTime? OrderGeneratedDateStart { get; set; }
        public DateTime? OrderGeneratedDateEnd { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
    }
}
