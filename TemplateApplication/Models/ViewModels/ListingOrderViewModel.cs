﻿using System.Collections.Generic;
using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class ListingOrderViewModel
    {
        public int? ListingIdentifier { get; set; }
        public Order Order { get; set; }
        public IEnumerable<State> States { get; set; }
        public Subscription CurrentSubscription { get; set; }
        public int ListingSubscriptionCost { get; set; }

        public string ListingSubscriptionCostDisplay
        {
            get
            {
                return string.Format("${0:0.00}", (ListingSubscriptionCost * 0.01));
            }
        }
    }
}