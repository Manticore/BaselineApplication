﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the unpaid order identifier associated with a specified 
-- listing.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetUnpaidOrderIdentifierForListing
       @ListingID INT
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @UnpaidOrderStatusID INT;

	-- Retrieve the unpaid order status identifier.
    SELECT @UnpaidOrderStatusID = OrderStatusID
    FROM dbo.OrderStatuses
    WHERE [Status] = 'Unpaid';

	-- Rerieve the unpaid order identifier for the listing.
    SELECT OrderID
    FROM dbo.Orders
    WHERE ListingID = @ListingID 
	AND OrderStatusID = @UnpaidOrderStatusID;

END;