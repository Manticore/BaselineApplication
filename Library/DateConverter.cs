﻿using System;

namespace Library
{
    public static class DateConverter
    {
        public static DateTime? ConvertDate(string date, bool isEndDate)
        {
            DateTime dateValue;
            bool isDate = DateTime.TryParse(date, out dateValue);
            if (isDate)
            {
                if (isEndDate)
                {
                    // Return the date with maximum time of 23:59:59.999.
                    return dateValue.AddDays(1).AddTicks(-1);
                }
                return dateValue;

            }
            return null;
        }
    }
}
