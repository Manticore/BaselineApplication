﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure retrieves a specified order payment.
-------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GetPayment]
	@PaymentID INT
AS
BEGIN

	SET NOCOUNT ON;

    -- Return the payment information if it exists.
    SELECT PaymentID, 
           PaymentDate, 
           PaymentDetails, 
           PT.PaymentTypeID, 
           PT.[Type],
		   OrderID, 
		   CreditCardName,
		   CreditCardNumber,
		   CreditCardExpiration,
		   CheckBankName,
		   CheckName,
		   CheckRoutingNumber,
		   CheckAccountNumber,
		   CheckNumber,
		   TransferBankName,
		   TransferName,
		   TransferRoutingNumber,
		   TransferAccountNumber,
		   TransferCheckNumber,
           CreateDate, 
           EditDate, 
           CreateUser, 
           EditUser
    FROM dbo.Payments P 
	   INNER JOIN dbo.PaymentTypes PT
             ON PT.PaymentTypeID = P.PaymentTypeID
    WHERE PaymentID = @PaymentID;

END;