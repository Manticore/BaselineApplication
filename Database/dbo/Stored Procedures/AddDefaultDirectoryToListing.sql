﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure adds an entry to the dbo.Listings_Directories table for the 
-- directory in the site that is marked as the "default" directory. This
-- procedure is mainly used when converting a "Bulk" listing to a "Manual"
-- listing in order to save some time.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.AddDefaultDirectoryToListing
       @ListingID INT
AS
BEGIN

    DECLARE @DefaultDirectoryID INT;
    
    -- Retrieve the directory ID for the default directory.
    SELECT @DefaultDirectoryID = DirectoryID
    FROM dbo.Directories
    WHERE IsDefault = 1;

    -- If the listing does not already have an entry for this directory, add it to the table.
    IF NOT EXISTS(SELECT 1
                  FROM dbo.Listings_Directories
                  WHERE ListingID = @ListingID AND DirectoryID = @DefaultDirectoryID)
        BEGIN
        
            INSERT INTO dbo.Listings_Directories
            (
				ListingID, 
				DirectoryID
            )
            VALUES
            (
				@ListingID, 
				@DefaultDirectoryID
            );
            
        END;
        
END;