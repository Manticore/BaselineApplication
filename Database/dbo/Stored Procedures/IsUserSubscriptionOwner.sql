﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns a boolean value indicating if a given subscription
-- identifier is associated with a specified user.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.IsUserSubscriptionOwner
       @SubscriptionID INT, 
       @UserID INT
AS
BEGIN

	-- Retrieve the listing ID from the subscription.
    DECLARE @ListingID INT;
    SELECT @ListingID = ListingID
    FROM dbo.Subscriptions
    WHERE SubscriptionID = @SubscriptionID;

	-- Determine if the specified user owns the listing. If so, they own the subscription too.
    EXEC IsUserListingOwner @ListingID = @ListingID, @UserID = @UserID;

END;