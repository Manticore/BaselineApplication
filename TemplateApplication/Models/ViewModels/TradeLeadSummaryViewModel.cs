﻿using System;
using Models.Models;
using System.Collections.Generic;

namespace TemplateApplication.Models.ViewModels
{
    public class TradeLeadSummaryViewModel
    {
        public string PassedKeyword { get; set; }
        public int? PassedIndustryID { get; set; }
        public DateTime? PassedStartDate { get; set; }
        public DateTime? PassedEndDate { get; set; }
        public IEnumerable<Industry> Industries { get; set; }
        public IEnumerable<TradeLead> TopTradeLeads { get; set; }
        public IEnumerable<TradeLead> TradeLeads { get; set; }
        public IEnumerable<Industry> IndustryCounts { get; set; }

        public string PassedStartDateValue
        {
            get
            {
                if (PassedStartDate.HasValue)
                {
                    return PassedStartDate.Value.Date.ToString("MM/dd/yyyy");
                }
                return null;
            }
        }

        public string PassedEndDateValue
        {
            get
            {
                if (PassedEndDate.HasValue)
                {
                    return PassedEndDate.Value.Date.ToString("MM/dd/yyyy");
                }
                return null;
            }
        }
    }
}