﻿using System.Net.Mail;
using System.Web.Mvc;
using TemplateApplication.Models.ViewModels;

namespace TemplateApplication.Controllers
{
    [ValidateAntiForgeryTokenOnAllPosts]
    public class InformationController : Controller
    {
        // Action method for the "About" page.
        public ActionResult About()
        {
            return View();
        }

        // Action method for the "Contact" page.
        public ActionResult Contact()
        {
            return View();
        }

        // Action method for the "Terms and Conditions" page.
        public ActionResult Terms()
        {
            return View();
        }

        // Action method for the "Privacy Policy" page.
        public ActionResult Privacy()
        {
            return View();
        }

        #region POST Methods

        // Post method to send an email to the default admin email address.
        [HttpPost]
        public void SendContactEmail(ContactFormMessage message)
        {
            // todo need to finish implementing this method.
            MailMessage mailMessage = new MailMessage
            {
                From    = new MailAddress(message.Email),
                Subject = message.Subject,
                Body    = message.Body
            };

            SmtpClient client = new SmtpClient
            {
                Port = 465,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = "hosts6.web-host.com"
            };

            client.Send(mailMessage);
            //mailMessage.To.Add(new MailAddress("bill.applegate@gmail.com"));

            //SmtpClient client = new SmtpClient();
            //client.Send(mailMessage);
        }

        #endregion
    }
}
