﻿using System;
using System.Collections.Generic;

namespace TemplateApplication.Models.ViewModels
{
    public class TradeLeadSearchViewModel
    {
        public string Keyword { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public List<int?> IndustryIDs { get; set; }
    }
}
