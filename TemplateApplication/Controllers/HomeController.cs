﻿using System.Text.RegularExpressions;
using System.Web.Mvc;
using Models.Interfaces;
using TemplateApplication.Models.ViewModels;

namespace TemplateApplication.Controllers
{
    [ValidateAntiForgeryTokenOnAllPosts]
    public class HomeController : Controller
    {
        private readonly IDatabaseContext _database;

        public HomeController(IDatabaseContext database)
        {
            _database = database;
        }

        // This is the default index route, the homepage of the site.
        public ActionResult Index()
        {
            IndexViewModel model = new IndexViewModel
            {
                Directories = _database.GetDirectories(),
                Industries  = _database.GetIndustries(),
                States      = _database.GetStates(),
                // Retrieve the industries associated with business listings.
                ListingIndustryCounts   = _database.GetListingIndustryCount(),
                // Retrieve the industries associated with trade leads.
                TradeLeadIndustryCounts = _database.GetTradeLeadIndustryCount()
            };
            return View(model);
        }

        #region POST Methods

        // This method saves an email address in the "Mailing List" database table.
        [HttpPost]
        public JsonResult SaveMailingListEmail(string emailAddress)
        {
            if (ValidateEmailAddress(emailAddress))
            {
                _database.AddMailingListEmailAddress(emailAddress);
                return Json(new { message = "Email saved successfully!" });
            }
            return Json(new { message = "Please provide a valid email address." });
        }

        #endregion

        #region Helper Method

        public bool ValidateEmailAddress(string emailAddress)
        {
            const string exp = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
            return Regex.IsMatch(emailAddress, exp, RegexOptions.IgnoreCase);
        }

        #endregion
    }
}