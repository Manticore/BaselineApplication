﻿using System;

namespace Models
{
    public class ManagedListingsModel
    {
        public int ListingID { get; set; }
        public string BusinessName { get; set; }
        public string BusinessContact { get; set; }
        public int UserID { get; set; }
        public int ListingTypeID { get; set; }
        public string Type { get; set; }
        public DateTime? CancelledDate { get; set; }
        public DateTime CreateDate { get; set; }
        public int? CurrentSubscriptionID { get; set; }

        public string BusinessContactDisplay
        {
            get { return BusinessContact.ToUpper(); }
        }

        public string CancelledDateDisplay
        {
            get
            {
                if (CancelledDate.HasValue)
                {
                    return CancelledDate.Value.ToShortDateString();
                }
                return null;
            }
        }
    }
}
