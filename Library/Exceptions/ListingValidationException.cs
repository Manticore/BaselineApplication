﻿using System;

namespace Library.Exceptions
{
    [Serializable]
    public class ListingValidationException : Exception
    {
        public ListingValidationException(string message) : base(message)
        {
        }
    }
}
