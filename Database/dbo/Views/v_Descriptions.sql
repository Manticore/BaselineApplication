﻿CREATE VIEW dbo.v_Descriptions
    WITH SCHEMABINDING
AS 
SELECT DescriptionID, 
       Code, 
       Body, 
       ListingID,
       ISNULL([Code], '') + ' ' + ISNULL([Body], '') AS ComputedText
FROM dbo.Descriptions;
