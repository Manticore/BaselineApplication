﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure is used to save a Stripe webhook that is received.The newly 
-- created StripeWebhookID is returned. This table is for informational and 
-- diagnosis purposes only.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.SaveStripeWebhook
    @StripeInvoiceID		 NVARCHAR(50),
    @Object					 NVARCHAR(100),
    @Type					 NVARCHAR(100),
    @CustomerID				 NVARCHAR(100),
    @ChargeID				 NVARCHAR(50),
    @WebhookDate			 DATETIME2,
    @WebhookDeliveredAtDate	 DATETIME2,
    @SubscriptionID			 NVARCHAR(50),
    @Paid					 BIT,
    @Total					 INT
AS
BEGIN

    SET NOCOUNT OFF;
    
    DECLARE @CurrentDate DATETIME2 = dbo.GetCurrentDate();

    INSERT INTO dbo.StripeWebhooks
    (
	   [StripeInvoiceID],
	   [Object],
	   [Type],
	   [CustomerID],
	   [ChargeID],
	   [WebhookDate],
	   [WebhookDeliveredAtDate],
	   [SubscriptionID],
	   [Paid],
	   [Total],
	   [LocalReceivedDate],
	   [ListingID],
	   [HandledSuccessfully]
    )
    VALUES
    (
	   @StripeInvoiceID, -- StripeInvoiceID
	   @Object,			 -- Object
	   @Type,			 -- Type
	   @CustomerID,		 -- CustomerID
	   @ChargeID,		 -- ChargeID
	   @WebhookDate,	 -- WebhookDate
	   @WebhookDeliveredAtDate,
	   @SubscriptionID,  -- SubscriptionID
	   @Paid,			 -- Paid
	   @Total,			 -- Total
	   @CurrentDate,	 -- LocalReceived -- This is the date we received the webhook.
	   NULL,
	   0
    );

	-- Return the newly created Stripe Webhook ID.
	DECLARE @NewStripeWebhookID INT;
	SET @NewStripeWebhookID = SCOPE_IDENTITY();
	SELECT @NewStripeWebhookID;

END;