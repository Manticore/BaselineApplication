﻿using System;
using Models.Models;
using Stripe;

namespace Models.Interfaces
{
    public interface IListingService
    {
        int? SaveListing(Listing listing, int userID);
        bool CancelSubscription(int listingIdentifier);
        bool ReactivateSubscription(int listingIdentifier);
        void CreateNewManualOrder(int listingIdentifier, int userIdentifier);
        Order CreateNewSubscriptionOrder(Listing listing);
        Order CreateProratedSubscriptionOrder(Listing listing, Subscription currentsubscription, int userID);
        void ConfigureSubscription(Listing listing, string creditCardToken, int ownerUserID, int currentUserID);
        void ConfigureNewSubscription(Listing listing, string creditCardToken, int userIdentifier);
        void ConfigureExistingSubscription(Listing listing, string creditCardToken, int ownerUserID, int currentUserID);
        bool ChargesAreRequired(Listing listing, Subscription currentSubscription);
        void CreateNewLocalSubscription(int listingIdentifier, int subscriptionCost, int directoryCount, int descriptionCount, int orderIdentifier, string stripeSubscriptionID, int userIdentifier, DateTime startDate, DateTime endDate);
        void UpdateCurrentLocalSubscription(Listing listing, Subscription currentSubscription, int? orderIdentifier, int? userIdentifier, string stripeSubscriptionID, int newSubscriptionCost);
        bool HasListingSubscriptionChanged(Listing listing);
        bool HandleStripeSubscriptionPaid(StripeInvoice stripeInvoice);
        int? PayOrder(Order order, Payment payment, bool isAdministrator, int initiatingUserIdentifier);
        int? GetWebhookListingID();
    }
}
