﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the Trade Leads of the site paged.
-------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GetTradeLeadsPaged]
	@Page				 INT		  = NULL,
	@NumberOfRecords	 INT		  = NULL,
	@TradeLeadID		 INT		  = NULL,
	@Title				 NVARCHAR(50) = NULL,
	@SelectedIndustryIDs IndustryIdentifierTable_UDT READONLY,
	@CreateDateStart	 DATETIME2	  = NULL,
	@CreateDateEnd		 DATETIME2	  = NULL,
	@UserName 			 NVARCHAR(56) = NULL,
	@SortColumn			 NVARCHAR(50) = NULL,
	@SortDirection		 NVARCHAR(10) = NULL
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @CurrentDate DATETIME = dbo.GetCurrentDate();

	DECLARE @OFFSET INT = ((@Page - 1) * @NumberOfRecords)

	DECLARE @TradeLeads TABLE 
	(
		[TradeLeadID]    INT,
		[Title]		     NVARCHAR(MAX),
		[Body]		     NVARCHAR(MAX),
		[Views]		     INT,
		[CreateDate]     DATETIME2,
		[CreateUser]     INT,
		[CreateUserName] NVARCHAR(56),
		[Total]			 INT
	);

	INSERT INTO @TradeLeads
	(
		[TradeLeadID], 
		[Title], 
		[Body], 
		[Views], 
		[CreateDate], 
		[CreateUser], 
		[CreateUserName], 
		[Total]
	)
	SELECT [TradeLeadID], 
		   [Title], 
		   [Body], 
		   [Views], 
		   [CreateDate], 
		   [CreateUser], 
		   UP.[UserName] AS CreateUserName, 
		   [Total] = COUNT(1) OVER()
	FROM dbo.TradeLeads T 
		INNER JOIN UserProfile UP
			 ON T.CreateUser = UP.UserId
	WHERE(@Title IS NULL OR Title LIKE '%' + @Title + '%') 
	AND (@TradeLeadID IS NULL OR T.TradeLeadID = @TradeLeadID) 
	AND (@CreateDateStart IS NULL OR CreateDate >= @CreateDateStart) 
	AND (@CreateDateEnd IS NULL OR CreateDate <= @CreateDateEnd) 
	AND (@UserName IS NULL OR UP.UserName LIKE '%' + @UserName + '%')
	AND (NOT EXISTS(SELECT 1
				 FROM @SelectedIndustryIDs) OR EXISTS(SELECT 'x'
				 FROM dbo.TradeLeads_Industries TI INNER JOIN @SelectedIndustryIDs SIDS
						ON TI.IndustryID = SIDS.IndustryID
				 WHERE T.TradeLeadID = TI.TradeLeadID))
	ORDER BY 
		CASE WHEN @SortColumn = 'CreateDateDisplay' AND @SortDirection = 'Ascendant'
			  THEN T.CreateDate END ASC, 
		CASE WHEN ((@SortColumn IS NULL) OR (@SortColumn = 'CreateDateDisplay' AND @SortDirection = 'Descendant'))
			  THEN T.CreateDate END DESC,
		CASE WHEN @SortColumn = 'TradeLeadID' AND @SortDirection = 'Ascendant'
			  THEN T.TradeLeadID END ASC, 
		CASE WHEN @SortColumn = 'TradeLeadID' AND @SortDirection = 'Descendant' 
			  THEN T.TradeLeadID END DESC,
		CASE WHEN @SortColumn = 'CreateUserNameDisplay' AND @SortDirection = 'Ascendant'
			  THEN UP.UserName END ASC, 
		CASE WHEN @SortColumn = 'CreateUserNameDisplay' AND @SortDirection = 'Descendant' 
			  THEN UP.UserName END DESC
	OFFSET @OFFSET ROWS FETCH NEXT @NumberOfRecords ROWS ONLY;
    
    
	-- Return the total number of records in the result set.
	SELECT TOP 1 Total FROM @TradeLeads;

	-- Return the trade lead objects.
	SELECT TradeLeadID,
		  Title,
		  Body,
		  [Views],
		  CreateDate,
		  CreateUser,
		  CreateUserName
	FROM @TradeLeads

	-- Return the industry tags for only the TradeLeads return above.
	SELECT I.IndustryID,
		   I.Name,
		   TI.TradeLeadID
	FROM dbo.TradeLeads_Industries TI
	INNER JOIN dbo.Industries I ON TI.IndustryID = I.IndustryID
	WHERE TI.TradeLeadID IN (SELECT TradeLeadID FROM @TradeLeads);


END;