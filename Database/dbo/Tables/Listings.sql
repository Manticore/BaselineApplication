﻿CREATE TABLE dbo.Listings
(
    ListingID INT IDENTITY(1, 1) PRIMARY KEY NOT NULL, 
    BusinessName NVARCHAR(250)NOT NULL, 
    BusinessContact NVARCHAR(250)NOT NULL, 
    ContactNumber NVARCHAR(15)NOT NULL, 
    ContactEmail NVARCHAR(320), 
    FaxNumber NVARCHAR(15), 
    Description NVARCHAR(MAX), 
    ProductDescription NVARCHAR(MAX), 
    Website NVARCHAR(100), 
    UserID INT, 
    ListingTypeID INT NOT NULL, 
    CancelledDate DATETIME2,
    CreateDate DATETIME2 DEFAULT SYSUTCDATETIME() NOT NULL, 
    EditDate DATETIME2 DEFAULT SYSUTCDATETIME() NOT NULL, 
    CreateUser INT NOT NULL, 
    EditUser INT NOT NULL
);