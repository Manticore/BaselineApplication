﻿using System;
using System.Collections.Generic;
using Stripe;

namespace Models.Interfaces
{
    public interface IPaymentRepository
    {
        bool HasStripeCustomerRecord(int userID);
        void SaveStripeCustomer(int userID, string stripeCustomerID);
        string GetStripeCustomerID(int userID);
        void UpdatePaymentMethod(int userID, string tokenID);
        string CreateStripeCustomer(int userID, string emailAddress);
        StripeSubscription CreateNewSubscription(string stripeCustomerID, int listingCost, string stripePlanID, DateTime? trialEnd, Dictionary<string, string> metadata);
        void UpdateStripeSubscription(string customerID, string subscriptionID, int listingCost, bool prorate);
        void CancelStripeSubscription(string customerID, string subscriptionID, bool cancelAtPeriodEnd);
        void ReactivateStripeSubscription(string customerID, string subscriptionID, string stripePlanID);
        string ChargeProrationFee(string stripeCustomerIdentifier, int amount);
        IEnumerable<StripeCard> GetUserDefaultPaymentMethod(int userIdentifier);
        IEnumerable<StripeCard> GetUserDefaultPaymentMethod(string stripeCustomerID);
        StripeSubscription GetStripeSubscription(string stripeCustomerID, string stripeSubscriptionID);
        void UpdateStripeCustomerEmail(int userIdentifier, string emailAddress);
    }
}
