﻿using Models.Interfaces;
using Models.Models;
using System.Web.Mvc;
using TemplateApplication.Filters;

namespace TemplateApplication.Controllers
{
    [Authorize(Roles = "Administrator")]
    [InitializeSimpleMembership]
    public class AdministrationController : Controller
    {
        private readonly IDatabaseContext _database;

        public AdministrationController(IDatabaseContext database)
        {
            _database = database;
        }

        // This is the default Action method for the admin dashbaord.
        // This page displays some quick statistics about the site.
        public ActionResult Index()
        {
            AdminDasboardStatisticsModel statistics = _database.RetrieveDashboardStatistics();
            return View(statistics);
        }
    }
}
