﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns a set of paged Trade Leads. This procedure is used
-- when a user is searching for Trade Leads in the site.
-------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GetTradeLeads] 
	@Page				 INT		  = NULL,
	@NumberOfRecords	 INT		  = NULL,
	@Keyword			 NVARCHAR(50) = NULL,
	@SelectedIndustryIDs IndustryIdentifierTable_UDT READONLY,
	@Start				 DATETIME2	  = NULL,
	@End				 DATETIME2	  = NULL
AS
BEGIN

	SET NOCOUNT ON;

	-- Grab the current date setting to be used in the stored procedure.
	DECLARE @CurrentDate DATETIME2 = dbo.GetCurrentDate();

	DECLARE @OFFSET INT = ((@Page - 1) * @NumberOfRecords)

	DECLARE @TradeLeads TABLE 
	(
		[TradeLeadID]    INT,
		[Title]		     NVARCHAR(MAX),
		[Body]		     NVARCHAR(MAX),
		[Views]		     INT,
		[CreateDate]     DATETIME2,
		[CreateUser]     INT,
		[CreateUserName] NVARCHAR(56)
	);

INSERT INTO @TradeLeads
(
    [TradeLeadID], 
    [Title], 
    [Body], 
    [Views], 
    [CreateDate], 
    [CreateUser], 
    [CreateUserName]
)
SELECT [TradeLeadID], 
       [Title], 
       [Body], 
       [Views], 
       [CreateDate], 
       [CreateUser], 
       UP.[UserName] AS CreateUserName
FROM dbo.v_TradeLeads T 
    INNER JOIN UserProfile UP
         ON T.CreateUser = UP.UserId
WHERE(@Keyword IS NULL OR ComputedText LIKE '%' + @Keyword + '%') 
AND (@Start IS NULL OR CreateDate >= @Start) 
AND (@End IS NULL OR CreateDate <= @End) 
AND (NOT EXISTS(SELECT 1
                FROM @SelectedIndustryIDs) OR EXISTS(SELECT 'x'
                                                     FROM dbo.TradeLeads_Industries TI 
														INNER JOIN @SelectedIndustryIDs SIDS
															ON TI.IndustryID = SIDS.IndustryID
                                                     WHERE T.TradeLeadID = TI.TradeLeadID))
ORDER BY CreateDate DESC
OFFSET @OFFSET ROWS FETCH NEXT @NumberOfRecords ROWS ONLY;
    
	-- Return the trade lead objects.
	SELECT TradeLeadID,
		  Title,
		  Body,
		  [Views],
		  CreateDate,
		  CreateUser,
		  CreateUserName
	FROM @TradeLeads

	-- Return the industry tags for only the TradeLeads return above.
	SELECT I.IndustryID,
		   I.Name,
		   TI.TradeLeadID
	FROM dbo.TradeLeads_Industries TI
	INNER JOIN dbo.Industries I ON TI.IndustryID = I.IndustryID
	WHERE TI.TradeLeadID IN (SELECT TradeLeadID FROM @TradeLeads);

END;