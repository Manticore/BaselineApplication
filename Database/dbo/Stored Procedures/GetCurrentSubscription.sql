﻿-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the current subscription record for a specified listing
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetCurrentSubscription
       @ListingID INT
AS
BEGIN

	SET NOCOUNT ON;

    SELECT SubscriptionID, 
           ListingID, 
           OrderID, 
           StripeSubscriptionID,
		   CurrentDirectoryCount,
		   CurrentDescriptionLinesCount,
           PaidDirectoryCount, 
           PaidDescriptionLinesCount, 
           StartDate, 
           EndDate, 
           CancelledDate, 
           SubscriptionCost, 
           MaxSubscriptionCost, 
           SubscriptionStatusID, 
           CreateDate, 
           EditDate, 
           CreateUser, 
           EditUser
    FROM dbo.GetCurrentSubscriptions()
    WHERE ListingID = @ListingID;

END;