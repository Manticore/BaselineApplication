﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure retrieves all non-admin users in the site.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetSiteUsers
       @Page			INT, 
       @NumberOfRecords INT, 
       @UserID			INT			  = NULL,
       @UserName		NVARCHAR(56)  = NULL,
       @EmailAddress	NVARCHAR(320) = NULL,
	   @SortColumn		NVARCHAR(50)  = NULL,
	   @SortDirection	NVARCHAR(10)  = NULL
AS
BEGIN

    SET NOCOUNT ON;

    -- Calculate the paging offset.
    DECLARE @OFFSET INT = (@Page - 1) * @NumberOfRecords;

    DECLARE @SiteUsers TABLE
    (
	    UserID INT, 
	    UserName NVARCHAR(56), 
	    EmailAddress NVARCHAR(320), 
	    Total INT
     );

    INSERT INTO @SiteUsers
    (
	   UserID, 
	   UserName, 
	   EmailAddress, 
	   Total
    )
	SELECT UP.UserId AS UserID, 
		   UP.UserName AS Username, 
		   EmailAddress, 
		   Total = COUNT(1) OVER()
	FROM UserProfile UP 
		INNER JOIN dbo.EmailAddresses EA
			 ON UP.UserId = EA.UserID
	WHERE (@UserID IS NULL OR UP.UserId = @UserID)
	  AND (@UserName IS NULL OR UP.UserName LIKE '%' + @UserName + '%')
	  AND (@EmailAddress IS NULL OR EA.EmailAddress LIKE '%' + @EmailAddress + '%')
	-- Do not return administrative users in the site for this report.
	  AND NOT EXISTS(SELECT 1
			  	     FROM webpages_UsersInRoles UIR 
				     INNER JOIN dbo.webpages_Roles R
						  ON UIR.RoleId = R.RoleId
				     WHERE RoleName = 'Administrator'
				     AND UserId = UP.UserId)
	ORDER BY 
		CASE WHEN @SortColumn = 'UserID' AND @SortDirection = 'ascendant'
			  THEN UP.UserId END ASC, 
		CASE WHEN @SortColumn = 'UserID' AND @SortDirection = 'descendant' 
			  THEN UP.UserId END DESC,
		CASE WHEN ((@SortColumn IS NULL) OR (@SortColumn = 'UserNameDisplay' AND @SortDirection = 'ascendant')) 
			  THEN UP.UserName END ASC, 
		CASE WHEN @SortColumn = 'UserNameDisplay' AND @SortDirection = 'descendant' 
			  THEN UP.UserName END DESC,
		CASE WHEN @SortColumn = 'EmailAddress' AND @SortDirection = 'ascendant'
			  THEN EA.EmailAddress END ASC, 
		CASE WHEN @SortColumn = 'EmailAddress' AND @SortDirection = 'descendant' 
			  THEN EA.EmailAddress END DESC
	OFFSET @OFFSET ROWS FETCH NEXT @NUMBEROFRECORDS ROWS ONLY;

    -- Return the total number of records.
    SELECT TOP 1 Total FROM @SiteUsers;

    -- Return the result set.
    SELECT UserID, 
           UserName, 
           EmailAddress
    FROM @SiteUsers;

END;