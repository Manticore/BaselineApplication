﻿CREATE TABLE dbo.Payments
(
    PaymentID INT NOT NULL PRIMARY KEY IDENTITY(1, 1), 
    PaymentDate DATETIME2 NOT NULL, 
    PaymentDetails NVARCHAR(500)NULL, 
    PaymentTypeID INT NOT NULL, 
    OrderID INT NOT NULL,

    -- Credit Card Specific Fields.
	CreditCardName NVARCHAR(100) NULL,
    CreditCardNumber NVARCHAR(100)NULL, 
    CreditCardExpiration NVARCHAR(20)NULL,

    -- Check Specific Fields
	CheckBankName NVARCHAR(100),
    CheckName NVARCHAR(20)NULL, 
	CheckRoutingNumber NVARCHAR(20) NULL,
    CheckAccountNumber NVARCHAR(20)NULL,
	CheckNumber NVARCHAR(20) NULL, 

    -- Transfer Specific Fields
	TransferBankName NVARCHAR(100),
    TransferName NVARCHAR(20)NULL, 
    TransferRoutingNumber NVARCHAR(20)NULL, 
    TransferAccountNumber NVARCHAR(20)NULL, 
    TransferCheckNumber NVARCHAR(20)NULL, 

    CreateDate DATETIME2 NOT NULL, 
    EditDate DATETIME2 NOT NULL, 
    CreateUser INT NOT NULL, 
    EditUser INT NOT NULL, 
    CONSTRAINT FK_PAYMENTS_ORDERID FOREIGN KEY(OrderID)REFERENCES dbo.Orders(OrderID), 
    CONSTRAINT FK_PAYMENTS_PAYMENTTYPEID FOREIGN KEY(PaymentTypeID) REFERENCES dbo.PaymentTypes(PaymentTypeID)
);
