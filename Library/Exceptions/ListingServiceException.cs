﻿using System;

namespace Library.Exceptions
{
    [Serializable]
    public class ListingServiceException : Exception
    {
        public ListingServiceException(string message) : base(message)
        {
        }
    }
}
