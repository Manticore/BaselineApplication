﻿using System.Collections.Generic;
using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class ListingViewModel
    {
        public Listing Listing { get; set; }
        public IEnumerable<State> States { get; set; }
        public IEnumerable<Directory> Directores { get; set; }
        public IEnumerable<Industry> Industries { get; set; }
        public string ListingFormAction { get; set; }
        public string SubmitButtonText { get; set; }
    }
}
