﻿using Models;
using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class SiteUsersModel
    {
        public PagedList<SiteUsersReportModel> SiteUsersPaged { get; set; }
    }
}