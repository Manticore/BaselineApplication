﻿
namespace Models.Models
{
    public class ListingType
    {
        public int ListingTypeID { get; set; }
        public string Type { get; set; }
    }
}
