﻿namespace Models
{
    public class ManagedListingsReportParameters
    {
        public int Page { get; set; }
        public int NumberOfRecords { get; set; }
        public int? ListingIdentifier { get; set; }
        public string BusinessName { get; set; }
        public int? ListingTypeID { get; set; }
        public bool? ShowCancelled { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
    }
}
