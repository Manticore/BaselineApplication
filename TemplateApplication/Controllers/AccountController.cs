﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Transactions;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Ajax.Utilities;
using Microsoft.Web.WebPages.OAuth;
using Services;
using Stripe;
using TemplateApplication.Models.ViewModels;
using WebMatrix.WebData;
using TemplateApplication.Filters;
using Models.Interfaces;

namespace TemplateApplication.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    [ValidateAntiForgeryTokenOnAllPosts]
    public class AccountController : Controller
    {
        private readonly IListingRepository _listingRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IDatabaseContext   _database;

        // Services, repositories, and database access are injected via dependency injection.
        public AccountController(IListingRepository listingRepository, IPaymentRepository paymentRepository, IDatabaseContext database)
        {
            _listingRepository = listingRepository;
            _paymentRepository = paymentRepository;
            _database          = database;
        }

        // Action method for the "Login" page.
        [AllowAnonymous]
        public ActionResult Login(string returnUrl, string message)
        {
            ViewBag.ReturnUrl = returnUrl;
            ViewBag.MessageType = message;
            return View();
        }

        // Post method used to authenticate a user and log them into the site.
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            // If the model is valid and the user is logged in successfully...
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                // Log their site access date and redirect them.
                var userIdentifier = WebSecurity.GetUserId(model.UserName);
                _database.LogUserAccessDate(userIdentifier, DateTime.Now.ToUniversalTime());
                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }

        // Action method that allows the user to register for the site.
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        // Post method that registers a user and logs them in.
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user.
                try
                {
                    // Make sure the email that is provided does not exist in the database.
                    if (!_database.EmailExists(model.Email))
                    {
                        // Create the user account.
                        WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                        // Save their email address associated to the new user.
                        var userID = WebSecurity.GetUserId(model.UserName);
                        _database.SaveEmailAddress(userID, model.Email);
                        // Log them in a redirect them to the home page.
                        WebSecurity.Login(model.UserName, model.Password);
                        return RedirectToAction("Index", "Home");
                    }
                    ModelState.AddModelError("", "A user with that email address already exists in the system.");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }
            // If we got this far something failed, redisplay form.
            return View(model);
        }

        // This Action method allows a user to reset their password.
        [AllowAnonymous]
        public ActionResult Forgot()
        {
            return View();
        }

        // This POST method generates a password reset token and sends the user an email.
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Forgot(ForgotModel model)
        {
            if (ModelState.IsValid)
            {
                string username = _database.GetUserFromEmail(model.EmailAddress);

                if (!string.IsNullOrEmpty(username))
                {
                    string resetToken = WebSecurity.GeneratePasswordResetToken(username);

                    // Email the user so they can reset their password.
                    var mailer = new Mailer();
                    mailer.SendForgotPasswordEmail(model.EmailAddress, resetToken);
                }
                return RedirectToAction("Login", "Account", new { message = "ForgotPasswordSent" });
            }
            return View();
        }

        // Action method used to reset a user's password.
        // The required token is generated and sent to the user in the "Forgot" Post method.
        [AllowAnonymous]
        public ActionResult Reset(string token)
        {
            if (!string.IsNullOrEmpty(token))
            {
                ResetPasswordModel model = new ResetPasswordModel
                {
                    ResetToken = token
                };
                return View(model);
            }
            return RedirectToAction("Login", "Account");
        }

        // Post method that is used to change a user's password if it has been forgotten.
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Reset(ResetPasswordModel model)
        {
            // Ensure the model is valid.
            if (ModelState.IsValid)
            {
                try
                {
                    // Reset the user's password associated with the reset token.
                    var passwordReset = WebSecurity.ResetPassword(model.ResetToken, model.NewPassword);
                    // If it was successful, redirect the user to the login page.
                    if (passwordReset)
                    {
                        return RedirectToAction("Login", "Account", new { message = "PasswordResetSuccess" });
                    }
                }
                catch (InvalidOperationException)
                {
                    ModelState.AddModelError("", "An error occured while resetting your password. Please try again.");
                    return View();
                }
            }
            // Return an error message if resetting the password was unsuccessful.
            ModelState.AddModelError("", "An error occured while resetting your password. Please try again.");
            return View();
        }

        // Post method used to log a user out of the site.
        [HttpPost]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();
            return RedirectToAction("Index", "Home");
        }

        // Action method for the user account page. Depending on the type of user the page will appear differently.
        public ActionResult Manage(string message)
        {
            // Retrieve the current user's identifier and determine if they are an admin.
            int currentUserIdentifier = WebSecurity.GetUserId(User.Identity.Name);
            bool isAdministrator = User.IsInRole("Administrator");

            // Set the layout of the manage account page depending on the user type.
            ViewBag.Layout = isAdministrator ? "~/Views/Shared/_AdminLayout.cshtml" : "~/Views/Shared/_Layout.cshtml";

            ManageAccountViewModel model;
            if (isAdministrator)
            {
                // Set up the model for an administrative user.
                model = new ManageAccountViewModel
                {
                    IsAdministrator     = true,
                    CurrentEmailAddress = _database.GetUserEmail(currentUserIdentifier),
                    LocalPasswordModel  = new LocalPasswordModel(),
                    ChangeEmailModel    = new ChangeEmailModel(),
                    Message             = message
                };
            }
            else
            {
                // Set up the model for a normal site user.
                model = new ManageAccountViewModel
                {
                    IsAdministrator     = false,
                    CurrentEmailAddress = _database.GetUserEmail(currentUserIdentifier),
                    LocalPasswordModel  = new LocalPasswordModel(),
                    ChangeEmailModel    = new ChangeEmailModel(),
                    // Retrieve the listings associated with the user.
                    Listings  = _listingRepository.GetUserListings(currentUserIdentifier),
                    // Retrieve credit cards stored in Stripe associated with the user.
                    UserCards = _paymentRepository.GetUserDefaultPaymentMethod(currentUserIdentifier),
                    // Get the states for the credit card update form.
                    States    = _database.GetStates(),
                    Message   = message
                };
            }

            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(currentUserIdentifier);
            return View(model);
        }

        #region POST Methods

        // Post method used to change a user's password.
        [HttpPost]
        public JsonResult ChangePassword(LocalPasswordModel password)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                        bool passwordChanged = WebSecurity.ChangePassword(User.Identity.Name, password.CurrentPassword, password.NewPassword);
                        if (passwordChanged)
                        {
                            return Json(new { message = "Password updated successfully." });
                        }
                        return Json(new { message = "Your password could not be updated." });
                    }
                    catch (InvalidOperationException exception)
                    {
                        return Json(new { message = exception.Message });
                    }
                }
            }
            return Json(new { message = "Your password could not be updated." });
        }

        // This POST method is used to update a user's email address.
        [HttpPost]
        public JsonResult UpdateEmailAddress(ChangeEmailModel emailModel)
        {
            if (ModelState.IsValid)
            {
                // If the email exists in the database, abandon update.
                if (_database.EmailExists(emailModel.NewEmailAddress))
                    return Json(new { Message = "The email address provided exists in our system already. Please try another.", Success = false });

                var currentUserIdentifier = WebSecurity.GetUserId(User.Identity.Name);
                // Save the new email address associated to the current user.
                _database.SaveEmailAddress(currentUserIdentifier, emailModel.NewEmailAddress);

                // If the user has an associated Stripe customer, update the email address in Stripe too.
                if (_paymentRepository.HasStripeCustomerRecord(currentUserIdentifier))
                {
                    _paymentRepository.UpdateStripeCustomerEmail(currentUserIdentifier, emailModel.NewEmailAddress);
                }
                return Json(new { Message = "Email address updated successfully!", Success = true});
            }
            // Retrieve a list of errors that occured.
            var validationErrors = ModelState.Values.SelectMany(v => v.Errors);
            var errorString = String.Join(",", validationErrors.Select(x => x.ErrorMessage));
            return Json(new { Message = errorString, Success = false });
        }
        
        // This POST method updates the default credit card in Stripe.
        [HttpPost]
        [Authorize]
        public JsonResult UpdatePaymentMethod(string cardToken)
        {
            // Update the payment method for the logged in user.
            int userID = WebSecurity.GetUserId(User.Identity.Name);
            try
            {
                _paymentRepository.UpdatePaymentMethod(userID, cardToken);
                return Json(new { message = "Payment method updated successfully!", success = true });
            }
            catch (StripeException exception)
            {
                // Updating the payment method may cause a Stripe exception to be thrown.
                // There are a few reasons this may occur, but usually it will be due to a card being declined.
                string exceptionMessage = exception.StripeError.Message;
                return Json(new { message = exceptionMessage, success = false });
            }
        }

        // POST method to return a users default credit card via AJAX.
        [HttpPost]
        [Authorize]
        public ActionResult GetUserCreditCards()
        {
            int userIdentifier = WebSecurity.GetUserId(User.Identity.Name);
            // Retrieve the user's Stripe customer record.
            string stripeCustomerID = _paymentRepository.GetStripeCustomerID(userIdentifier);
            if (stripeCustomerID != null)
            {
                var cardService = new StripeCardService();
                IEnumerable<StripeCard> response = cardService.List(stripeCustomerID);
                return PartialView("_CreditCard", response);
            }
            return null;
        }

        #endregion

        #region Helpers

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "The username supplied already exists. Please enter a different username.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        #endregion

        #region Unused Forms Authentication Method

        [HttpPost]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
            ManageMessageId? message = null;

            // Only disassociate the account if the currently logged in user is the owner
            if (ownerAccount == User.Identity.Name)
            {
                // Use a transaction to prevent the user from deleting their last login credential
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
                    if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
                    {
                        OAuthWebSecurity.DeleteAccount(provider, providerUserId);
                        scope.Complete();
                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        #endregion
    }
}
