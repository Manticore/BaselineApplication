﻿CREATE TABLE dbo.Orders
(
    OrderID INT NOT NULL PRIMARY KEY IDENTITY(1, 1),
	OrderNumber BIGINT NOT NULL,
    ListingID INT NOT NULL, 
    OrderTotal INT NOT NULL,
	OrderStatusID INT NOT NULL,
    CreateDate DATETIME2 NOT NULL DEFAULT GETDATE(), 
    EditDate DATETIME2 NOT NULL DEFAULT GETDATE(), 
    CreateUser INT NOT NULL, 
    EditUser INT NOT NULL, 
    CONSTRAINT FK_ORDERS_LISTINGID FOREIGN KEY(ListingID)REFERENCES Listings(ListingID),
	CONSTRAINT FK_ORDERS_ORDERSTATUSID FOREIGN KEY(OrderStatusID) REFERENCES dbo.OrderStatuses(OrderStatusID)
);
