﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TemplateApplication.Models.ViewModels
{
    public class ListingSearchViewModel
    {
        [StringLength(100, ErrorMessage = "Keyword string cannot be more than 100 characters.")]
        public string Keyword { get; set; }
        public int? DirectoryID { get; set; }
        public List<int?> IndustryIDs { get; set; }
        public int? StateID { get; set; }
    }
}