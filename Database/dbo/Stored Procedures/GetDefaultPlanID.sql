﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the Default Listing Plan ID from the Settings tables. 
-- The default plan ID is the Stripe plan ID used to create stripe subscriptions 
-- for listings in the site.
-------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GetDefaultPlanID]
AS
BEGIN

	SET NOCOUNT ON;

	SELECT Value 
	FROM dbo.Settings 
	WHERE SettingName = 'DefaultListingPlanID';

END;