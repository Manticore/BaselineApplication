﻿using Library.Exporter;

namespace Models
{
    public class SiteUsersReportModel
    {
        [CsvColumnName(Name = "EmailAddress", Order = 3)]
        public string EmailAddress { get; set; }

        [CsvColumnName(Name = "UserID", Order = 1)]
        public int UserID { get; set; }

        [CsvColumnName(Export = false)]
        public string UserName { get; set; }

        [CsvColumnName(Name = "UserName", Order = 2)]
        public string UserNameDisplay { get { return UserName.ToUpper(); } }
    }
}