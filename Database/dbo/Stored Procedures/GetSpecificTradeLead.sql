﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns a specific Trade Lead.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetSpecificTradeLead
       @TradeLeadID INT
AS
BEGIN
    SET NOCOUNT ON;

    SELECT TradeLeadID, 
           Title, 
           Body, 
           CreateDate, 
           EditDate
    FROM dbo.TradeLeads T
    WHERE T.TradeLeadID = @TradeLeadID;

    SELECT I.IndustryID, 
           I.Name
    FROM dbo.TradeLeads_Industries TI 
    INNER JOIN dbo.Industries I
	   ON I.IndustryID = TI.IndustryID
    WHERE TI.TradeLeadID = @TradeLeadID;

END;