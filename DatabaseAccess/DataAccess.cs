﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Models;
using Models.Interfaces;
using Models.Models;
using Stripe;

namespace DatabaseAccess
{
    public class DataAccess : IDatabaseContext
    {
        private readonly string _connectionString;

        // Connection string injected using dependency injection.
        public DataAccess(string connectionString)
        {
            _connectionString = connectionString;
        }

        #region Listing Repository Methods - Data Access

        public PagedList<ManagedListingsModel> GetManagedListings(ManagedListingsReportParameters parameters)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var sqlParameters = new DynamicParameters();
                sqlParameters.Add("@Page", parameters.Page);
                sqlParameters.Add("@NumberOfRecords", parameters.NumberOfRecords);
                sqlParameters.Add("@ListingIdentifier", parameters.ListingIdentifier);
                sqlParameters.Add("@BusinessName", parameters.BusinessName);
                sqlParameters.Add("@ListingTypeID", parameters.ListingTypeID);
                sqlParameters.Add("@IsCancelled", parameters.ShowCancelled);
                sqlParameters.Add("@SortColumn", parameters.SortColumn);
                sqlParameters.Add("@SortDirection", parameters.SortDirection);

                using (SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetManagedListings", sqlParameters, commandType: CommandType.StoredProcedure))
                {
                    int totalRecords = result.Read<int>().SingleOrDefault();
                    List<ManagedListingsModel> managedListings = result.Read<ManagedListingsModel>().ToList();
                    return new PagedList<ManagedListingsModel>(managedListings, totalRecords);
                }
            }
        }

        public IEnumerable<Industry> GetTradeLeadIndustryCount()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                return sqlConnection.Query<Industry>("dbo.GetTradeLeadIndustryCounts", null,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Industry> GetListingIndustryCount()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                return sqlConnection.Query<Industry>("dbo.GetListingIndustryCount", null,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public void ClearCancelledFlag(int listingIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ListingID", listingIdentifier);

                sqlConnection.Execute("dbo.ClearCancelledFlag", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Listing> GetListings(int page, int numberOfRecords, string keyword, int? directoryID, List<int?> industryIDs, int? stateID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                // Set up the DataTable for the Industry IDs that a user may search for.
                var dt = new DataTable();
                dt.Columns.Add("IndustryID", typeof(int));
                if (industryIDs != null)
                {
                    foreach (int? industryID in industryIDs)
                    {
                        if (industryID.HasValue)
                        {
                            dt.Rows.Add(industryID);
                        }
                    }
                }

                var parameters = new DynamicParameters();
                parameters.Add("@Page", page);
                parameters.Add("@NumberOfRecords", numberOfRecords);
                parameters.Add("@Keyword", keyword);
                parameters.Add("@DirectoryID", directoryID);
                parameters.Add("@SelectedIndustryIDs", dt.AsTableValuedParameter("IndustryIdentifierTable_UDT"));
                parameters.Add("@StateID", stateID);

                // Weave the returned TradeLead objects and assign them their correct Industry objects.
                using (SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetListings", parameters, commandType: CommandType.StoredProcedure))
                {
                    // Get the results from the first select in the stored procedure.
                    List<Listing> listings = result.Read<Listing>().ToList();
                    // Get the results from the second select in the stored procedure.
                    List<Industry> industries = result.Read<Industry>().ToList();
                    // Get the results from the third select in the stored procedure.
                    List<Description> descriptions = result.Read<Description>().ToList();
                    // Get the addresses returned for the set of listings.
                    List<Address> addresses = result.Read<Address>().ToList();
                    // Get the current subscriptions returned for the set of listings.
                    List<Subscription> currentSubscriptions = result.Read<Subscription>().ToList();
                    // Weave the results together.
                    foreach (Listing listing in listings)
                    {
                        listing.IndustryList = industries.Where(x => x.ListingID == listing.ListingID).ToList();
                        listing.DescriptionLines = descriptions.Where(x => x.ListingID == listing.ListingID).ToList();
                        listing.Address = addresses.FirstOrDefault(x => x.ListingID == listing.ListingID);
                        listing.CurrentSubscription = currentSubscriptions.FirstOrDefault(x => x.ListingID == listing.ListingID);
                    }
                    return listings;
                }
            }
        }

        public Listing GetSpecificListing(int? listingID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ListingID", listingID);

                Listing listing;
                using (
                    SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetSpecificListing", parameters,
                        commandType: CommandType.StoredProcedure))
                {
                    listing = result.Read<Listing>().FirstOrDefault();
                    List<Industry> industries = result.Read<Industry>().ToList();
                    Address address = result.Read<Address>().FirstOrDefault();
                    List<Directory> directories = result.Read<Directory>().ToList();
                    List<Description> descriptions = result.Read<Description>().ToList();

                    if (listing != null)
                    {
                        listing.IndustryList = industries;
                        listing.Address = address;
                        listing.Directories = directories;
                        listing.DescriptionLines = descriptions;
                    }
                }
                return listing;
            }
        }


        public Listing GetSpecificListingFull(int? listingID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ListingID", listingID);

                using (
                    SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetSpecificListingFull", parameters,
                        commandType: CommandType.StoredProcedure))
                {
                    Listing listing = result.Read<Listing>().FirstOrDefault();
                    List<Industry> industries = result.Read<Industry>().ToList();
                    Address address = result.Read<Address>().FirstOrDefault();
                    List<Directory> directories = result.Read<Directory>().ToList();
                    List<Description> descriptions = result.Read<Description>().ToList();
                    Subscription current = result.Read<Subscription>().FirstOrDefault();
                    List<Order> orders = result.Read<Order>().ToList();
                    List<OrderLineItem> lineItems = result.Read<OrderLineItem>().ToList();
                    List<Subscription> subscriptions = result.Read<Subscription>().ToList();
                    List<Note> notes = result.Read<Note>().ToList();

                    foreach (Order order in orders)
                    {
                        order.OrderLineItems = lineItems.Where(x => x.OrderID == order.OrderID).ToList();
                    }

                    if (listing != null)
                    {
                        listing.IndustryList = industries;
                        listing.CurrentSubscription = current;
                        listing.Address = address;
                        listing.Directories = directories;
                        listing.DescriptionLines = descriptions;
                        listing.Orders = orders;
                        listing.Subscriptions = subscriptions;
                        listing.Notes = notes;
                    }

                    return listing;
                }
            }
        }


        public int? UpsertListing(Listing listing, int userID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                // Create data table for the selected industry identifiers.
                var industryDataTable = new DataTable();
                industryDataTable.Columns.Add("IndustryID", typeof(int));
                foreach (Industry industry in listing.IndustryList)
                {
                    industryDataTable.Rows.Add(industry.IndustryID);
                }

                // Create data table for the selected industry identifiers.
                var directoryDataTable = new DataTable();
                directoryDataTable.Columns.Add("DirectoryID", typeof(int));
                foreach (Directory directory in listing.Directories)
                {
                    directoryDataTable.Rows.Add(directory.DirectoryID);
                }

                // Create a data table for the selected description lines.
                var descriptionDataTable = new DataTable();
                descriptionDataTable.Columns.Add("DescriptionCode", typeof(string));
                descriptionDataTable.Columns.Add("Description", typeof(string));
                foreach (Description description in listing.DescriptionLines)
                {
                    DataRow newRow = descriptionDataTable.NewRow();
                    newRow["DescriptionCode"] = description.Code;
                    newRow["Description"] = description.Body;
                    descriptionDataTable.Rows.Add(newRow);
                }

                var parameters = new DynamicParameters();

                parameters.Add("@ListingID", listing.ListingID);
                parameters.Add("@BusinessName", listing.BusinessName);
                parameters.Add("@BusinessContact", listing.BusinessContact);
                parameters.Add("@ContactEmail", listing.ContactEmail);
                parameters.Add("@ContactNumber", listing.ContactNumber);
                parameters.Add("@FaxNumber", listing.FaxNumber);
                parameters.Add("@Description", listing.Description);
                parameters.Add("@ProductDescription", listing.ProductDescription);
                parameters.Add("@WebsiteURL", listing.Website);
                parameters.Add("@Street1", listing.Address.Street1);
                parameters.Add("@Street2", listing.Address.Street2);
                parameters.Add("@City", listing.Address.City);
                parameters.Add("@StateID", listing.Address.StateID);
                parameters.Add("@Zip", listing.Address.Zip);
                parameters.Add("@CountryID", listing.Address.CountryID);
                parameters.Add("@SelectedIndustryIDs", industryDataTable.AsTableValuedParameter("IndustryIdentifierTable_UDT"));
                parameters.Add("@SelectedDirectoryIDs", directoryDataTable.AsTableValuedParameter("DirectoryIdentifierTable_UDT"));
                parameters.Add("@ListingDescriptionLines", descriptionDataTable.AsTableValuedParameter("DescriptionLinesTable_UDT"));
                parameters.Add("@UserID", userID);

                // Return the new listing identifier if successful.
                return sqlConnection.ExecuteScalar<int?>("dbo.UpsertListing", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void DeleteListing(int listingID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ListingID", listingID);

                sqlConnection.Execute("dbo.DeleteListing", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public PagedList<BillableListingsModel> GetBillableListings(BillableListingParameterSet parameters)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var sqlParameters = new DynamicParameters();
                sqlParameters.Add("@Page", parameters.Page);
                sqlParameters.Add("@NumberOfRecords", parameters.NumberOfRecords);
                sqlParameters.Add("@ListingID", parameters.ListingIdentifier);
                sqlParameters.Add("@BusinessName", parameters.BusinessName);
                sqlParameters.Add("@ListingCreateDateStart", parameters.ListingCreateDateStart, DbType.DateTime2);
                sqlParameters.Add("@ListingCreateDateEnd", parameters.ListingCreateDateEnd, DbType.DateTime2);
                sqlParameters.Add("@LastSubDateStart", parameters.LastSubscriptionDateStart, DbType.DateTime2);
                sqlParameters.Add("@LastSubDateEnd", parameters.LastSubscriptionDateEnd, DbType.DateTime2);
                sqlParameters.Add("@OrderGenerated", parameters.OrderGenerated);
                sqlParameters.Add("@OrderGenDateStart", parameters.OrderGeneratedDateStart, DbType.DateTime2);
                sqlParameters.Add("@OrderGenDateEnd", parameters.OrderGeneratedDateEnd, DbType.DateTime2);
                sqlParameters.Add("@SortColumn", parameters.SortColumn);
                sqlParameters.Add("@SortDirection", parameters.SortDirection);

                using (SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetBillableListings", sqlParameters, commandType: CommandType.StoredProcedure))
                {
                    // Grab the total for the number of records that appeared in the result set.
                    int totalRecords = result.Read<int>().SingleOrDefault();
                    // Get the results from the first select in the stored procedure.
                    var modelRecords = result.Read<BillableListingsModel>().ToList();
                    return new PagedList<BillableListingsModel>(modelRecords, totalRecords);
                }
            }
        }

        public PagedList<BulkListingsReportModel> GetBulkListings(BulkListingParameterSet parameters)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var sqlParameters = new DynamicParameters();
                sqlParameters.Add("@Page", parameters.Page);
                sqlParameters.Add("@NumberOfRecords", parameters.NumberOfRecords);
                sqlParameters.Add("@ListingID", parameters.ListingIdentifier);
                sqlParameters.Add("@BusinessName", parameters.BusinessName);
                sqlParameters.Add("@FaxNumber", parameters.FaxNumber);
                sqlParameters.Add("@SortColumn", parameters.SortColumn);
                sqlParameters.Add("@SortDirection", parameters.SortDirection);


                using (SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetBulkListings", sqlParameters, commandType: CommandType.StoredProcedure))
                {
                    // Grab the total for the number of records that appeared in the result set.
                    int totalRecords = result.Read<int>().SingleOrDefault();
                    // Get the results from the first select in the stored procedure.
                    var modelRecords = result.Read<BulkListingsReportModel>().ToList();
                    return new PagedList<BulkListingsReportModel>(modelRecords, totalRecords);
                }
            }
        }

        public bool IsUserListingOwner(int? listingID, int userID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ListingID", listingID);
                parameters.Add("@UserID", userID);

                return sqlConnection.ExecuteScalar<bool>("dbo.IsUserListingOwner", parameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public string GetDefaultPlanID()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                return sqlConnection.ExecuteScalar<string>("dbo.GetDefaultPlanID", null, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Listing> GetUserListings(int userID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserID", userID);

                using (SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetUserListings", parameters, commandType: CommandType.StoredProcedure))
                {
                    List<Listing> listings = result.Read<Listing>().ToList();
                    List<Subscription> subscriptions = result.Read<Subscription>().ToList();
                    List<Order> unpaidOrders = result.Read<Order>().ToList();

                    // Weave the results together.
                    foreach (Listing listing in listings)
                    {
                        listing.CurrentSubscription = subscriptions.FirstOrDefault(x => x.ListingID == listing.ListingID);
                        listing.Orders = unpaidOrders.Where(x => x.ListingID == listing.ListingID).ToList();
                    }
                    return listings;
                }
            }
        }

        public int? CreateSubscription(Subscription subscription, int userID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ListingID", subscription.ListingID);
                parameters.Add("@OrderID", subscription.OrderID);
                parameters.Add("@StripeSubscriptionID", subscription.StripeSubscriptionID);
                parameters.Add("@PaidDirectoryCount", subscription.PaidDirectoryCount);
                parameters.Add("@PaidDescriptionLinesCount", subscription.PaidDescriptionLinesCount);
                parameters.Add("@SubscriptionCost", subscription.SubscriptionCost);
                parameters.Add("@StartDate", subscription.StartDate);
                parameters.Add("@EndDate", subscription.EndDate);
                parameters.Add("@UserID", userID);

                var subscriptionID = sqlConnection.ExecuteScalar<int?>("dbo.CreateSubscription", parameters, commandType: CommandType.StoredProcedure);
                return subscriptionID;
            }
        }

        public bool DoesUnpaidOrderExist(int listingID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ListingIdentifier", listingID);

                return sqlConnection.ExecuteScalar<bool>("dbo.DoesUnpaidOrderExist", parameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public void UpdateLocalSubscription(int subscriptionID, int directoryCount, int descriptionLineCount,
            int? newSubscriptionCost, string stripeSubscriptionID, int userID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@SubscriptionID", subscriptionID);
                parameters.Add("@DirectoryCount", directoryCount);
                parameters.Add("@DescriptionLineCount", descriptionLineCount);
                parameters.Add("@NewSubscriptionCost", newSubscriptionCost);
                parameters.Add("@StripeSubscriptionID", stripeSubscriptionID);
                parameters.Add("@UserID", userID);

                sqlConnection.Execute("dbo.UpdateLocalSubscription", parameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public Subscription GetCurrentSubscription(int listingIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ListingID", listingIdentifier);

                var subscription = sqlConnection.Query<Subscription>("dbo.GetCurrentSubscription", parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return subscription;
            }
        }

        public int? GetListingOwnerUserID(int listingIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ListingID", listingIdentifier);

                return sqlConnection.ExecuteScalar<int?>("dbo.GetListingOwnerUserID", parameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public void CancelSubscription(int subscriptionID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SubscriptionID", subscriptionID);

                sqlConnection.Execute("dbo.CancelSubscription", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void CancelListing(int listingIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ListingID", listingIdentifier);

                sqlConnection.Execute("dbo.CancelListing", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void ReactivateSubscription(int subscriptionID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SubscriptionID", subscriptionID);

                sqlConnection.Execute("dbo.ReactivateSubscription", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public Subscription GetSubscription(int subscriptionIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SubscriptionID", subscriptionIdentifier);

                Subscription subscription =
                    sqlConnection.Query<Subscription>("dbo.GetSubscription", parameters,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                return subscription;
            }
        }

        #endregion

        #region Trade Lead Repository - Data Access
        // Create it's own efficient procedure for this, similar to get listings.
        public IEnumerable<TradeLead> GetTradeLeads(int page, int numberOfRecords, string keyword, List<int?> industryIDs, DateTime? start, DateTime? end)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                // Set up the DataTable for the Industry IDs that a user may search for.
                var dt = new DataTable();
                dt.Columns.Add("IndustryID", typeof(int));
                if (industryIDs != null)
                {
                    foreach (int? industryID in industryIDs)
                    {
                        if (industryID.HasValue)
                        {
                            dt.Rows.Add(industryID);
                        }
                    }
                }

                var sqlParameters = new DynamicParameters();
                sqlParameters.Add("@Page", page);
                sqlParameters.Add("@NumberOfRecords", numberOfRecords);
                sqlParameters.Add("@Keyword", keyword);
                sqlParameters.Add("@SelectedIndustryIDs", dt.AsTableValuedParameter("IndustryIdentifierTable_UDT"));
                sqlParameters.Add("@Start", start);
                sqlParameters.Add("@End", end);

                // Weave the returned TradeLead objects and assign them their correct Industry objects.
                using (SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetTradeLeads", sqlParameters, commandType: CommandType.StoredProcedure))
                {
                    // Get the results from the first select in the stored procedure.
                    List<TradeLead> tradeLeads = result.Read<TradeLead>().ToList();
                    // Get the results from the second select in the stored procedure.
                    List<Industry> industries = result.Read<Industry>().ToList();

                    // Weave the results together.
                    foreach (TradeLead lead in tradeLeads)
                    {
                        lead.IndustryList = industries.Where(x => x.TradeLeadID == lead.TradeLeadID).ToList();
                    }
                    return tradeLeads;
                }
            }
        }
        // convert this to a procedure to grab the listings for th editor. Paged.
        public PagedList<TradeLead> GetTradeLeadsPaged(TradeLeadParameterSet parameters)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                // Set up the DataTable for the Industry IDs that a user may search for.
                var dt = new DataTable();
                dt.Columns.Add("IndustryID", typeof(int));
                if (parameters.IndustryIdentifiers != null)
                {
                    foreach (int industryID in parameters.IndustryIdentifiers)
                    {
                        dt.Rows.Add(industryID);
                    }
                }

                var sqlParameters = new DynamicParameters();
                sqlParameters.Add("@Page", parameters.Page);
                sqlParameters.Add("@NumberOfRecords", parameters.NumberOfRecords);
                sqlParameters.Add("@TradeLeadID", parameters.TradeLeadID);
                sqlParameters.Add("@Title", parameters.Title);
                sqlParameters.Add("@SelectedIndustryIDs", dt.AsTableValuedParameter("IndustryIdentifierTable_UDT"));
                sqlParameters.Add("@CreateDateStart", parameters.CreateDateStart, DbType.DateTime2);
                sqlParameters.Add("@CreateDateEnd", parameters.CreateDateEnd, DbType.DateTime2);
                sqlParameters.Add("@UserName", parameters.UserName);
                sqlParameters.Add("@SortColumn", parameters.SortColumn);
                sqlParameters.Add("@SortDirection", parameters.SortDirection);

                // Weave the returned TradeLead objects and assign them their correct Industry objects.
                using (SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetTradeLeadsPaged", sqlParameters, commandType: CommandType.StoredProcedure))
                {
                    // Grab the total for the number of records that appeared in the result set.
                    int totalRecords = result.Read<int>().SingleOrDefault();
                    // Get the results from the first select in the stored procedure.
                    List<TradeLead> tradeLeads = result.Read<TradeLead>().ToList();
                    // Get the results from the second select in the stored procedure.
                    List<Industry> industries = result.Read<Industry>().ToList();

                    // Weave the results together.
                    foreach (TradeLead lead in tradeLeads)
                    {
                        lead.IndustryList = industries.Where(x => x.TradeLeadID == lead.TradeLeadID).ToList();
                    }
                    return new PagedList<TradeLead>(tradeLeads, totalRecords);
                }
            }
        }

        public TradeLead GetSpecificTradeLead(int tradeLeadID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TradeLeadID", tradeLeadID);

                TradeLead tradeLead;
                using (
                    SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetSpecificTradeLead", parameters,
                        commandType: CommandType.StoredProcedure))
                {
                    tradeLead = result.Read<TradeLead>().FirstOrDefault();
                    List<Industry> industries = result.Read<Industry>().ToList();

                    if (tradeLead != null)
                    {
                        tradeLead.IndustryList = industries;
                    }
                }
                return tradeLead;
            }
        }


        public void SaveTradeLeadView(int tradeLeadID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@TradeleadID", tradeLeadID);

                sqlConnection.Execute("dbo.SaveTradeLeadView", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<TradeLead> GetTopTradeLeads()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                List<TradeLead> tradeLeads;

                using (
                    SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetTopTradeLeads",
                        commandType: CommandType.StoredProcedure))
                {
                    tradeLeads = result.Read<TradeLead>().ToList();
                    List<Industry> industries = result.Read<Industry>().ToList();

                    foreach (TradeLead lead in tradeLeads)
                    {
                        lead.IndustryList = industries.Where(x => x.TradeLeadID == lead.TradeLeadID).ToList();
                    }
                }
                return tradeLeads;
            }
        }

        public void UpsertTradeLead(TradeLead tradeLead, int userID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                // Create the DataTable for the selcted IndustryIDs that were selected.
                var dt = new DataTable();
                dt.Columns.Add("IndustryID", typeof(int));
                foreach (Industry industry in tradeLead.IndustryList)
                {
                    dt.Rows.Add(industry.IndustryID);
                }

                var parameters = new DynamicParameters();
                parameters.Add("@TradeLeadID", tradeLead.TradeLeadID);
                parameters.Add("@Title", tradeLead.Title);
                parameters.Add("@Body", tradeLead.Body);
                parameters.Add("@UserID", userID);
                parameters.Add("@SelectedIndustryIDs", dt.AsTableValuedParameter("IndustryIdentifierTable_UDT"));
                sqlConnection.Execute("dbo.UpsertTradeLead", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void DeleteTradeLead(int tradeLeadID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TradeLeadID", tradeLeadID);
                sqlConnection.Execute("dbo.DeleteTradeLead", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        #endregion

        #region Report Repository Methods - Data Access

        public string GetUserEmail(int userID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserID", userID);

                return sqlConnection.ExecuteScalar<string>("dbo.GetUserEmail", parameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public string GetUserFromEmail(string emailAddress)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@EmailAddress", emailAddress);

                return sqlConnection.ExecuteScalar<string>("dbo.GetUserFromEmail", parameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public UserDetail GetUserDetails(int userIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserID", userIdentifier);
                return sqlConnection.Query<UserDetail>("dbo.GetUserDetails", parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public void LogUserAccessDate(int userIdentifier, DateTime date)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@UserID", userIdentifier);
                parameters.Add("@Date", date);

                sqlConnection.Execute("dbo.LogUserAccessDate", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public PricingModel GetCurrentListingPricingModel()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                return sqlConnection.Query<PricingModel>("dbo.GetCurrentListingPricingModel", commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public void SaveNewPricingModel(PricingModel model, int userIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@CostPerDirectory", model.CostPerDirectory);
                parameters.Add("@CostPerDescription", model.CostPerDescription);
                parameters.Add("@UserID", userIdentifier);

                sqlConnection.Execute("dbo.SaveNewPricingModel", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<PricingModel> GetPricingHistory()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                return sqlConnection.Query<PricingModel>("dbo.GetPricingHistory", commandType: CommandType.StoredProcedure);
            }
        }

        public void UpsertDirectory(Directory directory, int userIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@DirectoryID", directory.DirectoryID);
                parameters.Add("@Name", directory.Name);
                parameters.Add("@Description", directory.Description);
                parameters.Add("@IsDefault", directory.IsDefault);
                parameters.Add("@UserID", userIdentifier);

                sqlConnection.Execute("dbo.UpsertDirectory", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void DeleteDirectory(int directoryID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@DirectoryID", directoryID);

                sqlConnection.Execute("dbo.DeleteDirectory", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void AddMailingListEmailAddress(string emailAddress)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@EmailAddress", emailAddress);
                sqlConnection.Execute("dbo.AddMailingListEmailAddress", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public AdminDasboardStatisticsModel RetrieveDashboardStatistics()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                return sqlConnection.Query<AdminDasboardStatisticsModel>("dbo.RetrieveDashboardStatistics", commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public void ConvertUserToAdmin(int userIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@UserID", userIdentifier);
                sqlConnection.Execute("dbo.ConvertUserToAdmin", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void AddDefaultDirectoryToListing(int listingIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@ListingID", listingIdentifier);
                sqlConnection.Execute("dbo.AddDefaultDirectoryToListing", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void LinkListingToUserID(int listingIdentifier, int userIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@ListingID", listingIdentifier);
                parameters.Add("@UserID", userIdentifier);

                sqlConnection.Execute("dbo.LinkListingToUserID", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public int? SaveStripeWebhook(string eventType, StripeInvoice invoice)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@StripeInvoiceID", invoice.Id);
                parameters.Add("@Object", invoice.Object);
                parameters.Add("@Type", eventType);
                parameters.Add("@CustomerID", invoice.CustomerId);
                parameters.Add("@ChargeID", invoice.ChargeId);
                parameters.Add("@WebhookDate", invoice.Date);
                parameters.Add("@WebhookDeliveredAtDate", invoice.Date);
                parameters.Add("@SubscriptionID", invoice.SubscriptionId);
                parameters.Add("@Paid", invoice.Paid);
                parameters.Add("@Total", invoice.Total);

                return sqlConnection.ExecuteScalar<int?>("dbo.SaveStripeWebhook", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void SaveWebhookStatus(int stripeWebhookID, bool success, int? listingIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@StripeWebhookID", stripeWebhookID);
                parameters.Add("@Success", success);
                parameters.Add("@ListingID", listingIdentifier);

                sqlConnection.Execute("dbo.SaveWebhookStatus", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void SaveIndustry(Industry industry)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@IndustryID", industry.IndustryID);
                parameters.Add("@IndustryName", industry.Name);

                sqlConnection.Execute("dbo.SaveIndustry", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void DeleteIndustry(int industryIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@IndustryID", industryIdentifier);
                sqlConnection.Execute("dbo.DeleteIndustry", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public string GetUserEmailByStripeCustomerID(string stripeCustomerID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StripeCustomerID", stripeCustomerID);

                return sqlConnection.ExecuteScalar<string>("dbo.GetUserEmailByStripeCustomerID", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public PagedList<SiteError> GetSiteErrorsPaged(int page, int numberOfRecords)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Page", page);
                parameters.Add("@NumberOfRecords", numberOfRecords);

                using (
                    SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetSiteErrors", parameters,
                        commandType: CommandType.StoredProcedure))
                {
                    int totalRecords = result.Read<int>().SingleOrDefault();
                    List<SiteError> siteErrors = result.Read<SiteError>().ToList();
                    return new PagedList<SiteError>(siteErrors, totalRecords);
                }
            }
        }

        public PagedList<SiteUsersReportModel> GetSiteUsers(int page, int numberOfRecords, int? userID, string userName,
            string emailAddress, string sortColumn, string sortDirection)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Page", page);
                parameters.Add("@NumberOfRecords", numberOfRecords);
                parameters.Add("@UserID", userID);
                parameters.Add("@UserName", userName);
                parameters.Add("@EmailAddress", emailAddress);
                parameters.Add("@SortColumn", sortColumn);
                parameters.Add("@SortDirection", sortDirection);

                using (
                    SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetSiteUsers", parameters,
                        commandType: CommandType.StoredProcedure))
                {
                    int totalRecords = result.Read<int>().SingleOrDefault();
                    List<SiteUsersReportModel> siteUsers = result.Read<SiteUsersReportModel>().ToList();
                    return new PagedList<SiteUsersReportModel>(siteUsers, totalRecords);
                }
            }
        }

        #endregion

        #region Payment Repository Methods - Data Access

        public void UpdateListingType(int listingIdentifier, string listingType)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@ListingID", listingIdentifier);
                parameters.Add("@ListingType", listingType);

                sqlConnection.Execute("dbo.UpdateListingType", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public bool IsUserSubscriptionOwner(int subscriptionIdentifier, int userIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@SubscriptionID", subscriptionIdentifier);
                parameters.Add("@UserID", userIdentifier);

                return sqlConnection.ExecuteScalar<bool>("dbo.IsUserSubscriptionOwner", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public bool DoesSubscriptionExist(string stripeSubscriptionID, DateTime subscriptionStart, DateTime subscriptionEnd)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StripeSubscriptionID", stripeSubscriptionID);
                parameters.Add("@StartDate", subscriptionStart);
                parameters.Add("@EndDate", subscriptionEnd);

                return sqlConnection.ExecuteScalar<bool>("dbo.DoesSubscriptionExist", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public bool HasStripeCustomerRecord(int userID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserID", userID);

                return sqlConnection.ExecuteScalar<bool>("dbo.HasStripeCustomerRecord", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        ///     This procedure will save a stripe customer identifier, associated with a local user in the site.
        /// </summary>
        /// <param name="userID">The user ID to be associated withe the stripe customer identifier.</param>
        /// <param name="stripeCustomerID">Stripe customer identifier.</param>
        public void SaveStripeCustomer(int userID, string stripeCustomerID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@UserID", userID);
                parameters.Add("@StripeCustomerID", stripeCustomerID);

                sqlConnection.Execute("dbo.SaveStripeCustomer", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public string GetStripeCustomerID(int userID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserID", userID);

                return sqlConnection.ExecuteScalar<string>("dbo.GetStripeCustomerID", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        #endregion

        #region Utility Methods - Data Access

        public IEnumerable<OrderStatus> GetOrderStatuses()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                return sqlConnection.Query<OrderStatus>("dbo.GetOrderStatuses", commandType: CommandType.StoredProcedure);
            }
        }

        public PagedList<PrintOrderReportModel> GetUnpaidOrdersForPrinting(PrintOrdersParameterSet parameters)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var sqlParameters = new DynamicParameters();
                sqlParameters.Add("@Page", parameters.Page);
                sqlParameters.Add("@NumberOfRecords", parameters.NumberOfRecords);
                sqlParameters.Add("@OrderID", parameters.OrderID);
                sqlParameters.Add("@OrderNumber", parameters.OrderNumber);
                sqlParameters.Add("@ListingID", parameters.ListingIdentifier);
                sqlParameters.Add("@BusinessName", parameters.BusinessName);
                sqlParameters.Add("@CreateDateStart", parameters.CreateDateStart, DbType.DateTime2);
                sqlParameters.Add("@CreateDateEnd", parameters.CreateDateEnd, DbType.DateTime2);
                sqlParameters.Add("@OrderPrinted", parameters.OrderPrinted);
                sqlParameters.Add("@LastPrintDateStart", parameters.LastPrintDateStart, DbType.DateTime2);
                sqlParameters.Add("@LastPrintDateEnd", parameters.LastPrintDateEnd, DbType.DateTime2);
                sqlParameters.Add("@SortColumn", parameters.SortColumn);
                sqlParameters.Add("@SortDirection", parameters.SortDirection);

                using (SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetUnpaidOrdersForPrinting", sqlParameters, commandType: CommandType.StoredProcedure))
                {
                    int totalRecords = result.Read<int>().SingleOrDefault();
                    List<PrintOrderReportModel> orders = result.Read<PrintOrderReportModel>().ToList();
                    return new PagedList<PrintOrderReportModel>(orders, totalRecords);
                }
            }
        }

        public void SaveOrderPrintedDate(int orderIdentifier, int userIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@OrderID", orderIdentifier);
                parameters.Add("@PrintUser", userIdentifier);

                sqlConnection.Execute("dbo.SaveOrderPrintedDate", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public OrderBillDataModel GetOrderDetailsForBill(int orderIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@OrderID", orderIdentifier);

                // Weave the returned TradeLead objects and assign them their correct Industry objects.
                using (SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetOrderDetailsForBill", parameters, commandType: CommandType.StoredProcedure))
                {
                    // Get the results from the first select in the stored procedure.
                    OrderBillDataModel model = result.Read<OrderBillDataModel>().FirstOrDefault();
                    // Get the results from the second select.
                    Order order = result.Read<Order>().FirstOrDefault();
                    // Get the results for the second select
                    List<OrderLineItem> lineItems = result.Read<OrderLineItem>().ToList();

                    // Associate the line items to the order.
                    if (order != null)
                    {
                        order.OrderLineItems = lineItems;
                    }

                    // Add the full order to the model.
                    if (model != null)
                    {
                        model.Order = order;
                    }
                    return model;
                }
            }
        }

        public Payment GetPayment(int paymentIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PaymentID", paymentIdentifier);

                return sqlConnection.Query<Payment>("dbo.GetPayment", parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public int? GetUnpaidOrderIdentifierForListing(int listingIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ListingID", listingIdentifier);
                return sqlConnection.ExecuteScalar<int?>("dbo.GetUnpaidOrderIdentifierForListing", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public int? GetCreditCardPaymentTypeID()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                return sqlConnection.ExecuteScalar<int?>("dbo.GetCreditCardPaymentTypeID", commandType: CommandType.StoredProcedure);
            }
        }

        public bool HasOrderBeenInvoiced(int orderIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@OrderID", orderIdentifier);
                return sqlConnection.ExecuteScalar<bool>("dbo.HasOrderBeenInvoiced", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void DeleteUnpaidOrder(int orderIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@OrderID", orderIdentifier);

                sqlConnection.Execute("dbo.DeleteUnpaidOrder", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Industry> GetIndustries()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                return sqlConnection.Query<Industry>("dbo.GetIndustries", commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<State> GetStates()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                return sqlConnection.Query<State>("dbo.GetStates", commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Country> GetCountries()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                return sqlConnection.Query<Country>("dbo.GetCountries", commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Directory> GetDirectories()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                return sqlConnection.Query<Directory>("dbo.GetDirectories", commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<ListingType> GetListingTypes()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                return sqlConnection.Query<ListingType>("dbo.GetListingTypes", commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<PaymentType> GetPaymentTypes(bool isAdmin)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@IsAdmin", isAdmin);
                return sqlConnection.Query<PaymentType>("dbo.GetPaymentTypes", parameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public void SaveEmailAddress(int userID, string emailAddress)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@UserID", userID);
                parameters.Add("@EmailAddress", emailAddress);

                sqlConnection.Execute("dbo.UpsertEmailAddress", parameters, commandType: CommandType.StoredProcedure);
            }
        }


        public void SaveListingNote(Note note, int userIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ListingID", note.ListingID);
                parameters.Add("@NoteText", note.NoteText);
                parameters.Add("@UserID", userIdentifier);

                sqlConnection.Execute("dbo.SaveListingNote", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Note> GetListingNotes(int listingIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ListingID", listingIdentifier);

                return sqlConnection.Query<Note>("dbo.GetListingNotes", parameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public bool EmailExists(string emailAddress)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@EmailAddress", emailAddress);
                return sqlConnection.ExecuteScalar<bool>("dbo.EmailExists", parameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        #endregion

        #region Order Repository Methods - Data Access

        public PagedList<Order> GetOrdersPaged(OrdersParameterSet parameters)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var sqlParameters = new DynamicParameters();
                sqlParameters.Add("@Page", parameters.Page);
                sqlParameters.Add("@NumberOfRecords", parameters.NumberOfRecords);
                sqlParameters.Add("@OrderNumber", parameters.OrderNumber);
                sqlParameters.Add("@ListingID", parameters.ListingIdentifier);
                sqlParameters.Add("@BusinessName", parameters.BusinessName);
                sqlParameters.Add("@StartDate", parameters.StartDate);
                sqlParameters.Add("@EndDate", parameters.EndDate);
                sqlParameters.Add("@OrderStatusID", parameters.OrderStatusID);
                sqlParameters.Add("@SortColumn", parameters.SortColumn);
                sqlParameters.Add("@SortDirection", parameters.SortDirection);

                using (
                    SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetOrders", sqlParameters,
                        commandType: CommandType.StoredProcedure))
                {
                    int totalRecords = result.Read<int>().SingleOrDefault();
                    List<Order> orders = result.Read<Order>().ToList();
                    List<Payment> payments = result.Read<Payment>().ToList();

                    // Weave the order and payment records together.
                    foreach (Order order in orders)
                        order.Payment = payments.FirstOrDefault(x => x.OrderID == order.OrderID);

                    return new PagedList<Order>(orders, totalRecords);
                }
            }
        }

        public Order GetOrder(int orderID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@OrderID", orderID);

                using (
                    SqlMapper.GridReader result = sqlConnection.QueryMultiple("dbo.GetOrder", parameters,
                        commandType: CommandType.StoredProcedure))
                {
                    Order order = result.Read<Order>().FirstOrDefault();
                    List<OrderLineItem> lineItems = result.Read<OrderLineItem>().ToList();
                    Payment payment = result.Read<Payment>().FirstOrDefault();
                    List<InvoiceDate> invoiceDates = result.Read<InvoiceDate>().ToList();

                    if (order != null)
                    {
                        order.OrderLineItems = lineItems;
                        order.Payment = payment;
                        order.InvoiceDates = invoiceDates;
                    }
                    return order;
                }
            }
        }

        /// <summary>
        ///     This procedure is used to save an order in the database.
        /// </summary>
        /// <param name="order">The order model to be saved.</param>
        /// <param name="isPaid">Boolean parameter signifying the order as paid or unpiad.</param>
        /// <param name="userID">The identifier of the user calling this procedure.</param>
        /// <returns>The newly created order identifier.</returns>
        public int? SaveOrder(Order order, bool isPaid, int userID)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var orderLineItemTable = new DataTable();
                orderLineItemTable.Columns.Add("Description", typeof(string));
                orderLineItemTable.Columns.Add("LineItemTotal", typeof(int));

                foreach (OrderLineItem orderItem in order.OrderLineItems)
                {
                    DataRow newRow = orderLineItemTable.NewRow();
                    newRow["Description"] = orderItem.Description;
                    newRow["LineItemTotal"] = orderItem.LineItemTotal;
                    orderLineItemTable.Rows.Add(newRow);
                }

                var parameters = new DynamicParameters();
                parameters.Add("@ListingID", order.ListingID);
                parameters.Add("@OrderTotal", order.OrderTotal);
                parameters.Add("@OrderLineItems", orderLineItemTable.AsTableValuedParameter("OrderLineItemTable_UDT"));
                parameters.Add("@IsPaid", isPaid);
                parameters.Add("@UserID", userID);

                return sqlConnection.ExecuteScalar<int?>("dbo.SaveOrder", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public int? GetOrderOwnerUserID(int orderIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@OrderID", orderIdentifier);

                return sqlConnection.ExecuteScalar<int?>("dbo.GetOrderOwnerUserID", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public int SaveOrderPayment(Payment payment, int orderIdentifier, int userIdentifier)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@PaymentDetails", payment.PaymentDetails);
                parameters.Add("@PaymentTypeID", payment.PaymentTypeID);
                parameters.Add("@OrderID", orderIdentifier);
                parameters.Add("@CreditCardName", payment.CreditCardName);
                parameters.Add("@CreditCardNumber", payment.CreditCardNumber);
                parameters.Add("@CreditCardExpiration", payment.CreditCardExpiration);
                parameters.Add("@CheckBankName", payment.CheckBankName);
                parameters.Add("@CheckName", payment.CheckName);
                parameters.Add("@CheckRoutingNumber", payment.CheckRoutingNumber);
                parameters.Add("@CheckAccountNumber", payment.CheckAccountNumber);
                parameters.Add("@CheckNumber", payment.CheckNumber);
                parameters.Add("@TransferBankName", payment.TransferBankName);
                parameters.Add("@TransferName", payment.TransferName);
                parameters.Add("@TransferRoutingNumber", payment.TransferRoutingNumber);
                parameters.Add("@TransferAccountNumber", payment.TransferAccountNumber);
                parameters.Add("@TransferCheckNumber", payment.TransferCheckNumber);
                parameters.Add("@UserIdentifier", userIdentifier);

                return sqlConnection.ExecuteScalar<int>("dbo.SaveOrderPayment", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        #endregion
    }
}