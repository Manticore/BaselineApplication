﻿using System;
using System.IO;
using System.Net;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Models.Interfaces;
using Newtonsoft.Json.Linq;
using Services;
using Stripe;

namespace TemplateApplication.Controllers
{
    public class StripeWebhooksController : Controller
    {
        private readonly IListingService  _listingService;
        private readonly IDatabaseContext _database;

        // Service and database access injected via dependency injection.
        public StripeWebhooksController(IListingService listingService, IDatabaseContext database)
        {
            _listingService = listingService;
            _database       = database;
        }

        [HttpPost]
        public ActionResult Index()
        {
            // Since Content-Type is application/json in HTTP POST from Stripe we need to pull POST body from request stream directly
            Stream req = Request.InputStream;
            req.Seek(0, SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();

            // Parse the json body to a new StripeEvent object.
            StripeEvent stripeEvent;
            try
            {
                stripeEvent = StripeEventUtility.ParseEvent(json);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Unable to parse incoming event.");
            }

            if (stripeEvent == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Incoming event empty.");

            var obj = JObject.Parse(json);
            // Retrieve the event data object payload only.
            var dataObj = obj.SelectToken("data.object"); 

            // Depending on the type of event, we want to handle it differently.
            switch (stripeEvent.Type)
            {
                // Webhook used to handle a Stripe subscription that has been successfully paid.
                // (We want to create a local subscription in our database with an associated order and payment.)
                case "invoice.payment_succeeded":
                    try
                    {
                        // Retrieve the Stripe Invoice sent with the webhook.
                        var stripeInvoice = Mapper<StripeInvoice>.MapFromJson(dataObj.ToString());
                        // Log the Stripe event that has been sent. Return the new webhook identifier.
                        var loggedWebhookID  = _database.SaveStripeWebhook(stripeEvent.Type, stripeInvoice);
                        // Handle the webhook. This will create a new subscription based on the webhook information.
                        var success          = _listingService.HandleStripeSubscriptionPaid(stripeInvoice);
                        // Retrieve the Listing ID that was associated with the webhook.
                        var listingIDHandled = _listingService.GetWebhookListingID();

                        if (loggedWebhookID.HasValue)
                        {
                            // Update the webhook record's status in the database. 
                            // Setting the listing identifier and whether the webhook processing completed successfully.
                            _database.SaveWebhookStatus(loggedWebhookID.Value, success, listingIDHandled);  
                        }
                    }
                    catch (Exception)
                    {
                        // Return the status of the Webhook to Stripe as failed.
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    break;

                // Webhook to handle when a Stripe subscription payment fails.
                case "invoice.payment_failed":
                    var invoice = Mapper<StripeInvoice>.MapFromJson(dataObj.ToString());
                    // If the invoice contains a Stripe customer ID send the user an email letting them know their payment failed.
                    if (!string.IsNullOrEmpty(invoice.CustomerId))
                    {
                        // Retrieve the local user's email address given their Stripe customer identifier.
                        string emailAddress = _database.GetUserEmailByStripeCustomerID(invoice.CustomerId);
                        // If there is an email address associated with the subscription, send the payment failed email.
                        if (!String.IsNullOrEmpty(emailAddress))
                        {
                            // Send the payment failed email.
                            var mailer = new Mailer();
                            mailer.SendSubscriptionPaymentFailedEmail(emailAddress); 
                        }
                    }
                    break;
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}