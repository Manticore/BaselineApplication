﻿using System.Collections.Generic;
using System.Linq;

namespace Models
{
    public class ModelStateDictionary
    {
        private readonly Dictionary<string, string> _modelState;

        public ModelStateDictionary()
        {
            _modelState = new Dictionary<string, string>();
        }

        public Dictionary<string, string> ModelState { get { return _modelState; } }

        public void AddError(string key, string value)
        {
            _modelState.Add(key, value);
        }

        public bool IsValid()
        {
            return !_modelState.Any();
        }
    }
}