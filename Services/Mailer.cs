﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using RazorEngine;
using RazorEngine.Templating;

namespace Services
{
    public class Mailer
    {
        private readonly string _templatePath;
        private const string TemplateDirectory = "EmailTemplates";
        private readonly SmtpClient _client;

        public Mailer()
        {
            // Set the email template path.
            _templatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, TemplateDirectory);
            // SMTP server settings set up using the web config settings.
            _client = new SmtpClient("mailtrap.io", 2525)
            {
                Credentials = new NetworkCredential("45110bb8351feda53", "3bd97ada33d541"),
                EnableSsl = true
            };
        }
        
        /// <summary>
        /// This method sends an email to a given recipient which will allow them to reset their password.
        /// </summary>
        /// <param name="recipientEmail">The email address to send the email.</param>
        /// <param name="token">Reset token needed to reset their password.</param>
        /// <returns></returns>
        public void SendForgotPasswordEmail(string recipientEmail, string token)
        {
            // Set the name of the template to use.
            const string template = "ForgotPassword.cshtml";
            // Retrieve the razor view template as text.
            var forgotPasswordTemplate = File.ReadAllText(Path.Combine(_templatePath, template));

            var model = new ForgotPasswordModel
            {
               Token = token
            };

            // Merge the view template and the model.
            string html = Engine.Razor.RunCompile(forgotPasswordTemplate, "forgot-password", typeof(ForgotPasswordModel), model);
            // Inline the CSS contained in the view.
            var finishedHtml = InlineHtmlCss(html);
            // Send the email asynchronously.
            SendEmail(recipientEmail, "NA Trade Registry - Password Reset", finishedHtml);
        }

        /// <summary>
        /// Email templated used to alert a user when their Stripe subscription payment has failed.
        /// </summary>
        public void SendSubscriptionPaymentFailedEmail(string recipientEmail)
        {
            // Set the name of the template to use.
            const string template = "StripeSubscriptionPaymentFailed.cshtml";
            // Retrieve the razor view template as text.
            var paymentFailedTemplate = File.ReadAllText(Path.Combine(_templatePath, template));

            // Retrieve the template.
            string html = Engine.Razor.RunCompile(paymentFailedTemplate, "stripe-subscription-failed");
            // Inline the CSS contained in the view.
            var finishedHtml = InlineHtmlCss(html);
            // Send the email asynchronously.
            SendEmail(recipientEmail, "NA Trade Registry - Subscription payment failed.", finishedHtml);
        }

        /// <summary>
        /// Sends an email given message parameters.
        /// </summary>
        /// <returns></returns>
        private void SendEmail(string to, string subject, string body)
        {
            MailMessage message = new MailMessage
            {
                Body       = body,
                IsBodyHtml = true,
                Subject    = subject
            };
            message.To.Add(new MailAddress(to));
            _client.Send(message);
        }

        /// <summary>
        /// This methods takes all the CSS that is found in an email template and inlines it.
        /// This will help improve performance and ensure emails look as they should.
        /// </summary>
        /// <param name="html">The HTML to inline.</param>
        /// <returns></returns>
        private static string InlineHtmlCss(string html)
        {
            var result = PreMailer.Net.PreMailer.MoveCssInline(html);
            return result.Html;
        }
    }

    /// <summary>
    /// This model is used for the forgot password model.
    /// </summary>
    public class ForgotPasswordModel
    {
        public string Token { get; set; }
    }
}
