﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the listings associated with a user, and the subscription
-- for those listings.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetUserListings
       @UserID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @OrderStatusID INT
	SELECT @OrderStatusID = OrderStatusID 
	FROM dbo.OrderStatuses 
	WHERE [Status] = 'Unpaid';

    DECLARE @UserListings TABLE
    (
	   ListingID INT,
	   BusinessName NVARCHAR(250)
    );

    -- Retrieve all the listings for a given user.
    INSERT INTO @UserListings
    (
        ListingID,
        BusinessName
    )
    SELECT ListingID, 
           BusinessName
    FROM dbo.Listings
    WHERE UserID = @UserID;

    -- Return the user's listings.
    SELECT ListingID, 
		 BusinessName 
    FROM @UserListings 

    -- Retrieve the current subscription for the lisitng.
	SELECT	[SubscriptionID],
			[ListingID],
			[OrderID],
			[StripeSubscriptionID],
			[CurrentDirectoryCount],
			[PaidDirectoryCount],
			[CurrentDescriptionLinesCount],
			[PaidDescriptionLinesCount],
			[StartDate],
			[EndDate],
			[CancelledDate],
			[SubscriptionCost],
			[MaxSubscriptionCost],
			[SubscriptionStatusID],
			[CreateDate],
			[EditDate],
			[CreateUser],
			[EditUser]
	FROM dbo.GetCurrentSubscriptions() 
	WHERE ListingID IN (SELECT ListingID FROM @UserListings);

	-- Retrieve the unpaid order associated with a user's listing, if applicable.
	-- We don't need the line items in this case, just need the Order details.
	SELECT ListingID,
		   OrderID, 
		   OrderTotal 
	FROM dbo.Orders 
	WHERE OrderStatusID = @OrderStatusID
	AND ListingID IN (SELECT ListingID FROM @UserListings);

END;