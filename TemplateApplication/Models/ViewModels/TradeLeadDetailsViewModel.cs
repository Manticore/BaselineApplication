﻿using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class TradeLeadDetailsViewModel
    {
        public TradeLead TradeLead { get; set; }
    }
}