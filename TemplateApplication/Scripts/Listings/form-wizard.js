
var FormWizard = function () {
    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            // jQuery validation will not validate hidden controls. So, we need to modify the settings for validation to validate hidden controls.
            // The reason we need to do this is because the bootstrap select plugin hides the actual select elements and builds a pretty select
            // in its place. We need to validate the actual hidden select, not the new fancy select. Because we are using the bootstrap wizard,
            // an issue arises with this fix. Selects on other pages of the wizard not displayed yet should not be validated. So, we add the .ignore
            // class to the element until the user reaches the page that the select is actually on. Then we remove the .ignore class so it can
            // be validated correctly. Whew...
            $("#industry-select").addClass('ignore');
            $("#directory-select").addClass('ignore');

            var form = $('#listing-form');
            var error = $('.alert-danger', form);

            form.validate({
                // This option changes the default ignore functionality so bootstrap selects can be validated.
                ignore: ":not(select:hidden, input:visible, textarea:visible), .ignore",
                // This option enables to show the error/success messages on tab switch.
                doNotHideMessage: true,
                // Default input error message container,
                errorElement: 'span',
                // Default input error message class.
                errorClass: 'help-block help-block-error',
                // Do not focus the last invalid input.
                focusInvalid: false,
                rules: {
                    'Listing[BusinessName]': {
                        required: true
                    },
                    'Listing[BusinessContact]': {
                        required: true
                    },
                    'Listing[ContactNumber]': {
                        required: true,
                    },
                    'Listing[ContactEmail]': {
                        required: true,
                    },
                    'Listing[Website]': {
                        required: true,
                    },
                    'Listing[Address][Street1]': {
                        required: true
                    },
                    'Listing[Address][City]': {
                        required: true
                    },
                    'Listing[Address][StateID]': {
                        required: true
                    },
                    'Listing[Address][Zip]': {
                        required: true
                    },
                    'Listing[Description]': {
                        required: true
                    },
                    'Listing[ProductDescription]': {
                        required: true
                    },
                    'Listing[IndustryList][][IndustryID]': {
                        required: true
                    },
                    'Listing[Directories][][DirectoryID]' : {
                        required: true
                    }
                },

                messages: {
                    'Listing[BusinessName]': {
                        required: "Please provide your businesses name."
                    },
                    'Listing[BusinessContact]': {
                        required: "Please provide a name for your business contact."
                    },
                    'Listing[ContactNumber]': {
                        required: "Please provide a contact number for your business.",
                    },
                    'Listing[ContactEmail]': {
                        required: "Please provide a contact email for your business.",
                    },
                    'Listing[Website]': {
                        required: "Please provide a url to your business wensite.",
                    },
                    'Listing[Address][Street1]': {
                        required: "Please provide your street address."
                    },
                    'Listing[Address][City]': {
                        required: "Please provide your City."
                    },
                    'Listing[Address][StateID]': {
                        required: "Please select a state."
                    },
                    'Listing[Address][Zip]': {
                        required: "Please provide your zip code."
                    },
                    'Listing[Description]': {
                        required: "Please provide a description of your business."
                    },
                    'Listing[ProductDescription]': {
                        required: "Please provide a description of the products / services your business offers."
                    },
                    'Listing[IndustryList][][IndustryID]': {
                        required: "Please select at least one industry."
                    },
                    'Listing[Directories][][DirectoryID]': {
                        required: "Please select at least one directory for your business listing."
                    }
                },

                errorPlacement: function (error, element) {
                    if (element.attr("name") == "Listing[Address][StateID]")
                        error.insertAfter("#state-select-container .bootstrap-select");
                    else if (element.attr("name") == "Listing[IndustryList][][IndustryID]")
                        error.insertAfter("#industry-select-container .bootstrap-select");
                    else if (element.attr("name") == "Listing[Directories][][DirectoryID]")
                        error.insertAfter("#directory-select-container .bootstrap-select");
                    else {
                        // For all other inputs, the default behavior.
                        error.insertAfter(element);
                    }
                },

                invalidHandler: function (event, validator) {
                    // Display error alert on form submit.
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                highlight: function (element) {
                    // Hightlight error inputs.
                    // Set error class to the control group.
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },

                unhighlight: function (element) {
                    // Revert the change done by hightlight.
                    // Set error class to the control group.
                    $(element).closest('.form-group').removeClass('has-error');
                },

                success: function (label) {
                    // Mark the current input as valid and display OK icons.
                    // Set success class to the control group
                    label.addClass('valid').closest('.form-group').removeClass('has-error').addClass('has-success');
                },

                submitHandler: function (form) {
                    error.hide();
                    form.submit();
                }
            });

            //todo THIS NEEDS TO BE CLEANED UP.
            var displayConfirm = function () {
                $('#tab4 .form-control-static', form).each(function () {
                    var input = $('[name="' + $(this).attr("data-display") + '"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="' + $(this).attr("data-display") + '"]:checked', form);
                    }
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        var selectedOptionsString = "";
                        input.find('option:selected').each(function () {
                            if (selectedOptionsString != "")
                                selectedOptionsString += ", ";
                            selectedOptionsString += $(this).text();
                        });
                        $(this).html(selectedOptionsString);
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment[]') {
                        var payment = [];
                        $('[name="payment[]"]:checked', form).each(function () {
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    } else if ($(this).attr("data-display") == 'payment[]') {
                        
                    }
                });
            }

            var handleTitle = function (tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // Set done steps.
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var liList = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(liList[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }
                Metronic.scrollTo($('.page-title'));
            }

            // Form Wizard Declaration.
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    return false;
                },
                onNext: function (tab, navigation, index) {
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    error.hide();
                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                    // Logic needed to ignore the bootstrap select input controls.
                    // Because they hide the select, we need to modify the validation options to include hidden selects.
                    // This becomes a problem because bootstrap selects on other wizard tab pages would be validated.
                    // Add and remove the ignore class to stop this from happening.
                    if (index == 0) {
                        $("#industry-select").addClass('ignore');
                        $("#directory-select").addClass('ignore');
                        $("#state-select").removeClass('ignore');
                    }
                    if (index == 1) {
                        $("#industry-select").removeClass('ignore');
                        $("#state-select").addClass('ignore');
                        $("#directory-select").addClass('ignore');
                    }
                    if (index == 2) {
                        $("#industry-select").addClass('ignore');
                        $("#state-select").addClass('ignore');
                        $("#directory-select").removeClass('ignore');
                    }
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();

            $('#form_wizard_1 .button-submit').click(function () {
                $("#listing-form").submit();
            }).hide();

            // Needed to validate the bootstrap selects correctly.
            $('#listing-form select').on('change', function (e) {
                $('#listing-form').validate().element($(this));
            });
        }
    };
}();
