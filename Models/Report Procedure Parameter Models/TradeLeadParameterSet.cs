﻿using System;
using System.Collections.Generic;

namespace Models
{
    public class TradeLeadParameterSet
    {
        public int Page { get; set; }
        public int NumberOfRecords { get; set; }
        public int? TradeLeadID { get; set; }
        public string Title { get; set; }
        public IEnumerable<int> IndustryIdentifiers { get; set; }
        public DateTime? CreateDateStart { get; set; }
        public DateTime? CreateDateEnd { get; set; }
        public string UserName { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
    }
}
