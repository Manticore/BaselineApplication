﻿CREATE VIEW dbo.v_TradeLeads
    WITH SCHEMABINDING
AS 
SELECT TradeLeadID, 
       [Title], 
       [Body], 
       [CreateDate], 
       [CreateUser], 
       [Views],
       [Title] + ' ' + [Body] AS [ComputedText]
FROM dbo.TradeLeads;