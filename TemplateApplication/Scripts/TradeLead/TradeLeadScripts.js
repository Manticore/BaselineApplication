﻿
var TradeLeadAdministration = function () {
    return {

        init: function () {
            $('#tradelead-body').summernote({ height: 300 });

            $('#industry-select').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check'
            });

            var SaveTradeLead = function (form) {
                // Set the value of the textarea to be serialized.
                $("#tradelead-body").val($("#tradelead-body").code());
                // Serialize our form to an object.
                var formObject = form.serializeObject();
                AjaxMethods.UpsertTradeLead(formObject)
                    .done(function () {
                        window.location.replace("/SiteUtilities/TradeLeadEditor");
                    })
                    .fail(function () {
                        bootbox.alert("Oops, something went wrong.");
                    });
            }

            var validatedForm = $("#save-trade-lead-form");
            validatedForm.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    'TradeLead[Title]': {
                        required: true
                    },
                    'TradeLead[Body]': {
                        required: true
                    },
                    'TradeLead[IndustryList][][IndustryID]': {
                        required: true,
                    },
                },

                messages: {
                    'TradeLead[Title]': {
                        required: "Please enter a title."
                    },
                    'TradeLead[Body]': {
                        required: "Please enter a title."
                    },
                    'TradeLead[IndustryList][][IndustryID]': {
                        required: "Please select at least one industry."
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "TradeLead[IndustryList][][IndustryID]") {
                        error.insertAfter(".bootstrap-select");
                    } else {
                        error.insertAfter(element);
                    }
                },

                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label.addClass('valid') // mark the current input as valid and display OK icon
                    .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                },

                submitHandler: function (form) {
                    SaveTradeLead($(form));
                }
            });

            $('#save-trade-lead-form').validate().settings.ignore = ':not(select:hidden, input:visible, textarea:hidden)';
        }
    };
}();

