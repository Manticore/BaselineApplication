﻿using System.Collections.Generic;
using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class IndexViewModel
    {
        public IEnumerable<Directory> Directories { get; set; }
        public IEnumerable<Industry> Industries { get; set; }
        public IEnumerable<State> States { get; set; }
        public IEnumerable<Industry> ListingIndustryCounts { get; set; }
        public IEnumerable<Industry> TradeLeadIndustryCounts { get; set; }
    }
}