﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure is used to save a new pricing model in the database.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.SaveNewPricingModel
	@CostPerDirectory   INT,
	@CostPerDescription INT,
	@UserID				INT
AS
BEGIN

	INSERT INTO dbo.Pricing 
	(
		CostPerDirectory,
		CostPerDescription,
		CreateDate,
		CreateUser
	)
	VALUES
	(
		@CostPerDirectory,
		@CostPerDescription,
		SYSUTCDATETIME(),
		@UserID
	);
	
END;