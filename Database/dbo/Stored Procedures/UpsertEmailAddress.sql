﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure saves an email address associated to a given user.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.UpsertEmailAddress
       @UserID INT, 
       @EmailAddress NVARCHAR(320)
AS
BEGIN

	SET NOCOUNT ON;

    UPDATE dbo.EmailAddresses
           SET EmailAddress = @EmailAddress
    WHERE UserID = @UserID;

    -- The User ID doesn not exist in the EmailAddresses table.
    IF @@ROWCOUNT = 0
        BEGIN
            INSERT INTO dbo.EmailAddresses
			(
				UserID, 
				EmailAddress
			)
            VALUES
			(
				@UserID, 
				@EmailAddress
			 );
        END;

END;