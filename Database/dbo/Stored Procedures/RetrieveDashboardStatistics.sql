﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns a set of statistics for actions in the site.
-- This procedure is used to drive the admin dashboard statistics.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.RetrieveDashboardStatistics
AS
BEGIN

    -- Get the current date.
    DECLARE @CurrentDate	 DATETIME2 = dbo.GetCurrentDate();
    DECLARE @CurrentDateStart DATETIME2 = DATEADD(DAY, DATEDIFF(DAY, 0, @CurrentDate), 0);

    -- Get the start date of the current week.
    DECLARE @WeekStart DATETIME2;
    SET @WeekStart = DATEADD(WEEK, DATEDIFF(WEEK, '19050101', @CurrentDate), '19050101');

    -- Get the start date of the current month.
    DECLARE @MonthStart DATETIME2;
    SET @MonthStart = DATEADD(MONTH, DATEDIFF(MONTH, 0, @CurrentDate), 0);

	-- Retrieve the identifiers for the "Manual" and "Online" listing types.
	-- We only want to use these types of listings for the statistic. 
	-- "Bulk" listings should not be included.
    DECLARE @ListingTypeID_Manual INT;
    DECLARE @ListingTypeID_Online INT;

    SELECT @ListingTypeID_Manual = ListingTypeID FROM dbo.ListingTypes WHERE [Type] = 'Manual';
    SELECT @ListingTypeID_Online = ListingTypeID FROM dbo.ListingTypes WHERE [Type] = 'Online';


    SELECT

    /* RETURN PAYMENT STATISTICS */
    -- Return the number of payments made today.
    (
    SELECT COUNT(*) FROM Payments 
    WHERE Payments.PaymentDate >= @CurrentDateStart 
    AND Payments.PaymentDate <= @CurrentDate
    ) AS PaymentsPerDay,

    -- Return the number of payments made in the current week.
    (
    SELECT COUNT(*)
    FROM Payments
    WHERE Payments.PaymentDate >= @WeekStart 
    AND Payments.PaymentDate <= @CurrentDate
    ) AS PaymentsPerWeek,

    -- Return the number of payments made in the current month.
    (
    SELECT COUNT(*)
    FROM Payments
    WHERE Payments.PaymentDate >= @MonthStart 
    AND Payments.PaymentDate <= @CurrentDate
    ) AS PaymentsPerMonth,


    /* RETURN NEW USER STATISTICS */
    -- Return the number of users created today.\
    (
    SELECT COUNT(*)
    FROM dbo.webpages_Membership  
    WHERE CreateDate >= @CurrentDateStart 
    AND CreateDate <= @CurrentDate
    ) AS NewUsersPerDay,

    -- Return the number of users created in the current week.
    (
    SELECT COUNT(*)
    FROM dbo.webpages_Membership 
    WHERE CreateDate >= @WeekStart 
    AND CreateDate <= @CurrentDate
    ) AS NewUsersPerWeek,

    -- Return the number of users created in the current month.
    (
    SELECT COUNT(*)
    FROM dbo.webpages_Membership 
    WHERE CreateDate >= @MonthStart 
    AND CreateDate <= @CurrentDate
    ) AS NewUsersPerMonth,


    /* RETURN NEW LISTING STATISTICS */
    -- Return the number of listings created today.
    (
    SELECT COUNT(*)
    FROM dbo.Listings  
    WHERE ListingTypeID IN (@ListingTypeID_Manual, @ListingTypeID_Online)
    AND (CreateDate >= @CurrentDateStart AND CreateDate <= @CurrentDate)
    ) AS NewListingsPerDay,

    -- Return the number of listings created in the current week.
    (
    SELECT COUNT(*)
    FROM dbo.Listings 
    WHERE ListingTypeID IN (@ListingTypeID_Manual, @ListingTypeID_Online)
    AND (CreateDate >= @WeekStart AND CreateDate <= @CurrentDate)
    ) AS NewListingsPerWeek,

    -- Return the number of listings created in the current month.
    (
    SELECT COUNT(*)
    FROM dbo.Listings 
    WHERE ListingTypeID IN (@ListingTypeID_Manual, @ListingTypeID_Online)
    AND (CreateDate >= @MonthStart AND CreateDate <= @CurrentDate)
    ) AS NewListingsPerMonth

END;