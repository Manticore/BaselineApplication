﻿
$(document).ready(function () {
    // Initialize the industry select as a bootstrap select.
    $("#industry-select").selectpicker({
        iconBase: 'fa',
        tickIcon: 'fa-check',
        size: 8
    });

    // Initialize the state select as a bootstrap select.
    $("#directory-select").selectpicker({
        iconBase: 'fa',
        tickIcon: 'fa-check',
        size: 8
    });

    // Initialize the state select as a bootstrap select.
    $("#state-select").selectpicker({
        iconBase: 'fa',
        tickIcon: 'fa-check',
        size: 8
    });

    // This click event is used to add more fields to the description lines section of the form.
    $("#add-description-button").click(function () {
        var lineCount = $("#description-lines-container").find(".description-line-row").length;
        if (lineCount < 10) {
            var descriptionLineRowTemplate = $([
            "<div class=\"row description-line-row\">",
                "<div class=\"col-md-6\">",
                    "<div class=\"form-group\">",
                        "<label class=\"control-label col-md-4\">",
                            "<strong>Code</strong>",
                        "</label>",
                        "<div class=\"col-md-8\">",
                            "<input type=\"text\" class=\"form-control\" name=\"Listing[DescriptionLines][][Code]\" />",
                        "</div>",
                    "</div>",
                "</div>",
                "<div class=\"col-md-6\">",
                    "<div class=\"form-group\">",
                        "<label class=\"control-label col-md-4\"><strong>Description</strong></label>",
                        "<div class=\"col-md-8\">",
                            "<input class=\"form-control\" type=\"text\" name=\"Listing[DescriptionLines][][Body]\" />",
                        "</div>",
                    "</div>",
                "</div>",
            "</div>"].join("\n"));

            $("#description-lines-container").append(descriptionLineRowTemplate);
        } else {
            bootbox.alert("Maximum number of description lines reached.");
        }
    });

    // When the remove description line button is clicked, the most recently added row is removed.
    $("#remove-description-button").click(function () {
        $("#description-lines-container").find(".description-line-row").last().remove();
    });
});