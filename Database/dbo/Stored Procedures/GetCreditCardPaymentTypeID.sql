﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the payment type identifier for the "Credit Card"
-- payment method.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetCreditCardPaymentTypeID
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT PaymentTypeID
    FROM dbo.PaymentTypes
    WHERE Type = 'Credit Card';

END;