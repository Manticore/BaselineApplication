﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Models.Models
{
    public class Listing
    {
        public int? ListingID { get; set; }
        public string BusinessName { get; set; }
        public string Description { get; set; }
        public string ProductDescription { get; set; }
        public Address Address { get; set; }
        public string BusinessContact { get; set; }
        public string ContactEmail { get; set; }
        public string ContactNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Website { get; set; }
        public int ListingTypeID { get; set; }
        public string Type { get; set; }

        private IEnumerable<Description> _descriptionLines;
        public IEnumerable<Description> DescriptionLines
        {
            get
            {
                if (_descriptionLines == null)
                {
                    return new List<Description>();
                }
                return _descriptionLines.Where(x => !string.IsNullOrWhiteSpace(x.Code) || !string.IsNullOrWhiteSpace(x.Body));
            }
            set { _descriptionLines = value; }
        }

        private List<Directory> _directories;
        public List<Directory> Directories
        {
            get
            {
                if (_directories == null)
                {
                    return new List<Directory>();
                }
                return _directories;
            }
            set { _directories = value; }
        }

        private List<Industry> _industryList;

        public List<Industry> IndustryList
        {
            get
            {
                if (_industryList == null)
                {
                    return new List<Industry>();
                }
                return _industryList;
            }
            set { _industryList = value; }
        }

        public Subscription CurrentSubscription { get; set; }
        public IEnumerable<Subscription> Subscriptions { get; set; }
        public List<Order> Orders { get; set; }
        public IEnumerable<Note> Notes { get; set; }
        public int? UserID { get; set; }
        public string OwnerUserName { get; set; }
        public string StripeCustomerID { get; set; }
        public DateTime? CancelledDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public string CreateUser { get; set; }
        public string EditUser { get; set; }

        public string CancelledDateDisplay
        {
            get
            {
                return CancelledDate.HasValue ? CancelledDate.Value.ToShortDateString() : null;
            }
        }

        public string BusinessContactDisplay
        {
            get { return BusinessContact.ToUpper(); }
        }

        // Returns a boolean value indicating whether the listing contains any directory selections. 
        // A listing must have at least one directory selection for an order to be generated.
        public bool IsListingBillable
        {
            get
            {
                return (Directories.Any());
            }
        }
    }
}
