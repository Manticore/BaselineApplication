﻿namespace Models.Models
{
    public class OrderStatus
    {
        public int    OrderStatusID { get; set; }
        public string Status        { get; set; }
    }
}