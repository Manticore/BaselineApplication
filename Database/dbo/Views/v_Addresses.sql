﻿CREATE VIEW dbo.v_Addresses
AS SELECT A.AddressID, 
          A.Street1, 
          A.Street2, 
          A.City, 
		  S.StateID,
          S.Name AS State,
          A.Zip
   FROM Addresses A 
	   INNER JOIN dbo.States S
            ON A.StateID = S.StateID;
