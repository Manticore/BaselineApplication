﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- For a Stripe subscription to be created a user first needs to have a Stripe
-- customer created and associated with their user identifier. This procedure 
-- returns the Stripe customer identifier for the given user.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetStripeCustomerID
       @UserID INT
AS
BEGIN

	SET NOCOUNT ON;

    SELECT StripeCustomerID
    FROM dbo.StripeCustomers
    WHERE UserID = @UserID;

END;