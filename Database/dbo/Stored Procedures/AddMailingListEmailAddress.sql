﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure adds a given email address to the dbo.MailingListEmailAddresses
-- table. If the email address that is passed to the procedure exists in the 
-- database already it will not be inserted.
-------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[AddMailingListEmailAddress]
	@EmailAddress NVARCHAR(320)
AS
BEGIN

	IF NOT EXISTS(SELECT 1 FROM dbo.MailingListEmailAddresses WHERE EmailAddress = @EmailAddress)
	BEGIN

		INSERT INTO dbo.MailingListEmailAddresses
		(
			EmailAddress
		)
		VALUES
		(
			@EmailAddress
		);

	END;

END;