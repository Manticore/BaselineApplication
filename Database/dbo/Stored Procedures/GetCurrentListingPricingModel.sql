﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the most recent record in the Pricing table, which
-- corresponds to the current pricing for listings.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetCurrentListingPricingModel
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT TOP 1
		PricingID, 
		CostPerDirectory, 
		CostPerDescription 
	FROM dbo.Pricing
	ORDER BY PricingID DESC;

END;