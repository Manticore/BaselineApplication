﻿using Models.Models;
using System;
using System.Collections.Generic;
using Stripe;

namespace Models.Interfaces
{
    public interface IDatabaseContext
    {
        /*
         * 
         Trade Lead Repository Methods
         -----------------------------   
         * 
         */
        IEnumerable<TradeLead> GetTradeLeads(int page, int numberOfRecords, string keyword, List<int?> industryIDs, DateTime? starTime, DateTime? end);
        PagedList<TradeLead> GetTradeLeadsPaged(TradeLeadParameterSet parameters);
        TradeLead GetSpecificTradeLead(int tradeLeadID);
        void SaveTradeLeadView(int tradeLeadID);
        IEnumerable<TradeLead> GetTopTradeLeads();
        void UpsertTradeLead(TradeLead tradeLead, int userID);
        void DeleteTradeLead(int tradeLeadID);
        PagedList<ManagedListingsModel> GetManagedListings(ManagedListingsReportParameters parameters);
        IEnumerable<Industry> GetTradeLeadIndustryCount();

        /*
         * 
         Listing Repository Methods
         -----------------------------   
         * 
         */
        IEnumerable<Listing> GetListings(int page, int numberOfRecords, string keyword, int? directoryID, List<int?> industryIDs, int? stateID);
        Listing GetSpecificListing(int? listingID);
        int? UpsertListing(Listing listing, int userID);
        void DeleteListing(int listingID);
        PagedList<BillableListingsModel> GetBillableListings(BillableListingParameterSet parameters);
        PagedList<BulkListingsReportModel> GetBulkListings(BulkListingParameterSet parameters);
        bool IsUserListingOwner(int? listingID, int userID);
        IEnumerable<Listing> GetUserListings(int userID);
        int? CreateSubscription(Subscription subscription, int userID);
        Listing GetSpecificListingFull(int? listingID);
        void CancelSubscription(int subscriptionID);
        void ReactivateSubscription(int subscriptionID);
        Subscription GetSubscription(int subscriptionIdentifier);
        string GetDefaultPlanID();
        bool DoesUnpaidOrderExist(int listingID);
        void UpdateLocalSubscription(int subscriptionID, int directoryCount, int descriptionLineCount, int? newSubscriptionCost, string stripeSubscriptionID, int userID);
        Subscription GetCurrentSubscription(int listingIdentifier);
        int? GetListingOwnerUserID(int listingIdentifier);
        IEnumerable<Note> GetListingNotes(int listingIdentifier);
        void UpdateListingType(int listingIdentifier, string listingType);
        bool IsUserSubscriptionOwner(int subscriptionIdentifier, int userIdentifier);
        bool DoesSubscriptionExist(string stripeSubscriptionID, DateTime subscriptionStart, DateTime subscriptionEnd);
        IEnumerable<Industry> GetListingIndustryCount();
        void ClearCancelledFlag(int listingIdentifier);
        void CancelListing(int listingIdentifier);

        /*
         * 
         Payment Repository Methods
         -----------------------------   
         * 
         */
        bool HasStripeCustomerRecord(int userID);
        void SaveStripeCustomer(int userID, string stripeCustomerID);
        string GetStripeCustomerID(int userID);

        /*
         * 
         Order Repository Methods
         -----------------------------   
         * 
         */
        PagedList<Order> GetOrdersPaged(OrdersParameterSet parameters);
        Order GetOrder(int orderID);
        int? SaveOrder(Order order, bool isPaid, int userID);
        IEnumerable<OrderStatus> GetOrderStatuses();
        IEnumerable<PaymentType> GetPaymentTypes(bool isAdmin);
        int SaveOrderPayment(Payment payment, int orderIdentifier, int userIdentifier);
        PagedList<PrintOrderReportModel> GetUnpaidOrdersForPrinting(PrintOrdersParameterSet parameters);
        int? GetOrderOwnerUserID(int orderIdentifier);
        void SaveOrderPrintedDate(int orderIdentifier, int userIdentifier);
        OrderBillDataModel GetOrderDetailsForBill(int orderIdentifier);
        Payment GetPayment(int paymentIdentifier);
        int? GetUnpaidOrderIdentifierForListing(int listingIdentifier);
        int? GetCreditCardPaymentTypeID();
        bool HasOrderBeenInvoiced(int orderIdentifier);
        void DeleteUnpaidOrder(int orderIdentifier);

        /*
         * 
         Utility Methods
         -----------------------------   
         * 
         */
        IEnumerable<Industry> GetIndustries();
        IEnumerable<State> GetStates();
        IEnumerable<Country> GetCountries();
        IEnumerable<Directory> GetDirectories();
        IEnumerable<ListingType> GetListingTypes();
        void SaveEmailAddress(int userID, string emailAddress);
        string GetUserEmail(int userID);
        string GetUserFromEmail(string emailAddress);
        bool EmailExists(string emailAddress);
        void SaveListingNote(Note note, int userIdentifier);
        UserDetail GetUserDetails(int userIdentifier);
        void LogUserAccessDate(int userIdentifier, DateTime date);
        PricingModel GetCurrentListingPricingModel();
        void SaveNewPricingModel(PricingModel model, int userIdentifier);
        IEnumerable<PricingModel> GetPricingHistory();
        void UpsertDirectory(Directory directory, int userIdentifier);
        void DeleteDirectory(int directoryID);
        void AddMailingListEmailAddress(string emailAddress);
        AdminDasboardStatisticsModel RetrieveDashboardStatistics();
        void ConvertUserToAdmin(int userIdentifier);
        void AddDefaultDirectoryToListing(int listingIdentifier);
        void LinkListingToUserID(int listingIdentifier, int userIdentifier);
        int? SaveStripeWebhook(string eventType, StripeInvoice invoice);
        void SaveWebhookStatus(int stripeWebhookID, bool success, int? listingIdentifier);
        void SaveIndustry(Industry industry);
        void DeleteIndustry(int industryIdentifier);
        string GetUserEmailByStripeCustomerID(string stripeCustomerID);

        PagedList<SiteError> GetSiteErrorsPaged(int page, int numberOfRecords);
        PagedList<SiteUsersReportModel> GetSiteUsers(int page, int numberOfRecords, int? userID, string userName, string emailAddress, string sortColumn, string sortDirection);
    }
}
