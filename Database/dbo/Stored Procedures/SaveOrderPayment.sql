﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure saves an order payment associated with a specified order.
-------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SaveOrderPayment]
	@PaymentDetails		   NVARCHAR(500),
	@PaymentTypeID		   INT,
	@OrderID			   INT,
	@CreditCardName		   NVARCHAR(100) = NULL,
	@CreditCardNumber	   NVARCHAR(100) = NULL,
	@CreditCardExpiration  NVARCHAR(20)  = NULL,
	@CheckBankName		   NVARCHAR(100) = NULL,
	@CheckName			   NVARCHAR(20)  = NULL,
	@CheckRoutingNumber	   NVARCHAR(20)  = NULL,
	@CheckAccountNumber	   NVARCHAR(20)  = NULL,
	@CheckNumber		   NVARCHAR(20)  = NULL,
	@TransferBankName	   NVARCHAR(100) = NULL,
	@TransferName		   NVARCHAR(20)  = NULL,
	@TransferRoutingNumber NVARCHAR(20)  = NULL,
	@TransferAccountNumber NVARCHAR(20)  = NULL,
	@TransferCheckNumber   NVARCHAR(20)  = NULL,
	@UserIdentifier		   INT
AS
BEGIN

	BEGIN TRY
		BEGIN TRANSACTION;

		DECLARE @CurrentDate DATETIME = dbo.GetCurrentDate();

		-- Retrieve the paid order status identifier.
		DECLARE @PaidOrderStatusID INT;
		SELECT @PaidOrderStatusID = OrderStatusID
		FROM dbo.OrderStatuses
		WHERE [Status] = 'Paid';

		-- Mark the order as paid.
		UPDATE dbo.Orders
		SET OrderStatusID = @PaidOrderStatusID
		WHERE OrderID = @OrderID;

		-- Create the new payment associated with the order.
		INSERT INTO dbo.Payments
		(
			PaymentDate,
			PaymentDetails,
			PaymentTypeID,
			OrderID,
			CreditCardName,
			CreditCardNumber,
			CreditCardExpiration,
			CheckBankName,
			CheckName,
			CheckRoutingNumber,
			CheckAccountNumber,
			CheckNumber,
			TransferBankName,
			TransferName,
			TransferRoutingNumber,
			TransferAccountNumber,
			TransferCheckNumber,
			CreateDate,
			EditDate,
			CreateUser,
			EditUser
		)
		VALUES
		(
			@CurrentDate,		    -- PaymentDate
			@PaymentDetails,	    -- PaymentDetails
			@PaymentTypeID,		    -- PaymentTypeID
			@OrderID,			    -- OrderID
			@CreditCardName,		-- CreditCardName
			@CreditCardNumber,	    -- CreditCardNumber
			@CreditCardExpiration,  -- CreditCardExpiration
			@CheckBankName,			-- CheckBankName
			@CheckName,			    -- CheckName
			@CheckRoutingNumber,	-- CheckRoutingNumber
			@CheckAccountNumber,    -- CheckAccountNumber
			@CheckNumber,		    -- CheckNumber
			@TransferBankName,		-- TransferBankName
			@TransferName,		    -- TransferName
			@TransferRoutingNumber, -- TransferRoutingNumber
			@TransferAccountNumber, -- TransferAccountNumber
			@TransferCheckNumber,	-- TransferCheckNumber
			@CurrentDate,		    -- CreateDate
			@CurrentDate,		    -- EditDate
			@UserIdentifier,		-- CreateUser
			@UserIdentifier		    -- EditUser
		);

		-- Return the newly created payment identifier.
		DECLARE @PaymentIdentifier INT = SCOPE_IDENTITY();
		SELECT @PaymentIdentifier;

	COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH

		DECLARE @ErrorMessage  NVARCHAR(4000) = ERROR_MESSAGE();
		DECLARE @ErrorSeverity INT			  = ERROR_SEVERITY();
		DECLARE @ErrorState    INT			  = ERROR_STATE();

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

		ROLLBACK TRANSACTION;

	END CATCH;
END;