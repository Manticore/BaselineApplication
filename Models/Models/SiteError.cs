﻿using System;

namespace Models.Models
{
    public class SiteError
    {
        public string   Application { get; set; }
        public Guid     ErrorID     { get; set; }
        public string   Host        { get; set; }
        public string   Message     { get; set; }
        public string   Source      { get; set; }
        public int      StatusCode  { get; set; }
        public DateTime Time        { get; set; }
        public string   TimeDisplay { get { return Time.ToShortDateString(); } }
        public string   User        { get; set; }

        public string UserDisplay
        {
            get { return User.ToUpper(); }
        }
    }
}