﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the order statuses available in the database.
-------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GetOrderStatuses]
AS
BEGIN

	SET NOCOUNT ON;

	SELECT [OrderStatusID], [Status] 
	FROM dbo.OrderStatuses;

END;