﻿CREATE TYPE [dbo].[OrderLineItemTable_UDT] AS TABLE
(
	[Description] NVARCHAR(300),
	LineItemTotal INT
)
