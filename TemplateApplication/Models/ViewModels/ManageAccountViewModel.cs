﻿using System.Collections.Generic;
using Models.Models;
using Stripe;

namespace TemplateApplication.Models.ViewModels
{
    public class ManageAccountViewModel
    {
        public bool IsAdministrator { get; set; }
        public string CurrentEmailAddress { get; set; }
        public IEnumerable<Listing> Listings { get; set; }
        public LocalPasswordModel LocalPasswordModel { get; set; }
        public ChangeEmailModel ChangeEmailModel { get; set; }
        public IEnumerable<StripeCard> UserCards { get; set; }
        public IEnumerable<State> States { get; set; }
        public string Message { get; set; }
    }
}