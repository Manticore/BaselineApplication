﻿using System.Linq;
using Library.Exceptions;
using Models;
using Models.Interfaces;
using Models.Models;

namespace Services
{
    /// <summary>
    /// Trade Lead Service class.
    /// </summary>
    public class TradeLeadService
    {
        private readonly ITradeLeadRepository _tradeLeadRepository;

        private readonly ModelStateDictionary _modelState;

        public TradeLeadService(ITradeLeadRepository tradeLeadRepository)
        {
            _tradeLeadRepository = tradeLeadRepository;
            _modelState = new ModelStateDictionary();
        }

        public void SaveTradeLead(TradeLead tradeLead, int userID)
        {
            if (!ValidateTradeLead(tradeLead))
            {
                throw new TradeLeadValidationException();
            }
            _tradeLeadRepository.UpsertTradeLead(tradeLead, userID);
        }

        public bool ValidateTradeLead(TradeLead tradeLead)
        {
            if (tradeLead.Title.Trim().Length == 0)
                _modelState.AddError("Title", "Title is required.");
            if (tradeLead.Body.Trim().Length == 0)
                _modelState.AddError("Body", "Title is required.");
            if (!tradeLead.IndustryList.Any())
                _modelState.AddError("Industries", "Please select at least one industry.");
            return _modelState.IsValid();
        }
    }
}
