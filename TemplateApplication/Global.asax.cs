﻿using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace TemplateApplication
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            // Register custom view engine.
            ViewEngines.Engines.Add(new PartialViewEngine());
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }
    }

    /// <summary>
    /// This is a custom view engine used to search within the "Partial" subfolder in the "Views" subfolder..
    /// </summary>
    public class PartialViewEngine : RazorViewEngine
    {
        public PartialViewEngine()
        {
            var newLocationFormat = new[] { "~/Views/{1}/Partial/{0}.cshtml" };
            PartialViewLocationFormats = PartialViewLocationFormats.Union(newLocationFormat).ToArray();
        }
    }
}