﻿using System.Collections.Generic;
using Models.Models;

namespace Models.Interfaces
{
    public interface IValidationService
    {
        bool ValidateDirectory(Directory directory);
        bool ValidatePricingModel(PricingModel pricing);
        bool ValidateIndustry(Industry industry);
        Dictionary<string, string> GetErrorDictionary();
        string GetErrorString();
    }
}
