﻿using System;

namespace Models.Models
{
    public class OrderBillDataModel
    {
        public string BusinessName { get; set; }
        public string BusinessContact { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string StateAbbreviation { get; set; }
        public string StateName { get; set; }
        public string Zip { get; set; }

        /// <summary>
        /// Contains all the information for the order.
        /// </summary>
        public Order Order { get; set; }
    }
}
