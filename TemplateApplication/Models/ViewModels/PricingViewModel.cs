﻿using System.Collections.Generic;
using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class PricingViewModel
    {
        public PricingModel CurrentPricingModel { get; set; }
        public IEnumerable<PricingModel> HistoricalPricingModels { get; set; }
    }
}