﻿namespace Models.Models
{
    public class Address
    {
        public int    AddressID { get; set; }
        public string City      { get; set; }
        public string Country   { get; set; }
        public int    CountryID { get; set; }
        public int    ListingID { get; set; }
        public string State     { get; set; }
        public int?   StateID   { get; set; }
        public string Street1   { get; set; }
        public string Street2   { get; set; }
        public string Zip       { get; set; }
    }
}