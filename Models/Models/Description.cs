﻿
namespace Models.Models
{
    public class Description
    {
        public int? ListingID { get; set; }
        public string Code { get; set; }
        public string Body { get; set; }
    }
}
