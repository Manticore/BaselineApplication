﻿using Enyim.Caching;
using Enyim.Caching.Memcached;
using Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memcache
{
    public class MemcacheDataAccess : ICacheContext
    {
        private MemcachedClient _client;

        public MemcacheDataAccess()
        {
            //_client = new MemcachedClient();
        }

        public object Get(string key)
        {
            object value = _client.Get(key);
            return value;
        }

        public bool KeyExists(string key)
        {
            throw new NotImplementedException();
        }

        public void Set(string key, object value, int time)
        {
            _client.Store(StoreMode.Set, key, value, DateTime.Now.AddDays(1));
        }
    }
}
