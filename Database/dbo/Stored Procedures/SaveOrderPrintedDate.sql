﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure saves the date an order was printed and the user that printed it.
-------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SaveOrderPrintedDate]
	@OrderID     INT,
	@PrintUser   INT
AS
BEGIN

    DECLARE @CurrentDate DATETIME2 = dbo.GetCurrentDate();

    INSERT INTO dbo.InvoiceDates
    (
	   OrderID,
	   CreateDate,
	   CreateUser
    )
    VALUES
    (
	   @OrderID,
	   @CurrentDate,
	   @PrintUser
    );

END;