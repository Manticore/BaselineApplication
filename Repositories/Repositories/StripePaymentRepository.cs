﻿using System;
using System.Collections.Generic;
using Models.Interfaces;
using Stripe;

namespace Repositories.Repositories
{
    public class StripePaymentRepository : IPaymentRepository
    {
        private readonly IDatabaseContext _databaseAccess;

        public StripePaymentRepository(IDatabaseContext databaseAccess)
        {
            _databaseAccess = databaseAccess;
        }

        public bool HasStripeCustomerRecord(int userID)
        {
            return _databaseAccess.HasStripeCustomerRecord(userID);
        }

        public void SaveStripeCustomer(int userID, string stripeCustomerID)
        {
            _databaseAccess.SaveStripeCustomer(userID, stripeCustomerID);
        }

        public string GetStripeCustomerID(int userID)
        {
            return _databaseAccess.GetStripeCustomerID(userID);
        }

        /// <summary>
        /// Updates the default payment method for the selected user.
        /// </summary>
        /// <param name="userID">Identifier for the current user.</param>
        /// <param name="creditCardToken">Credit card token created by stripe.js</param>
        public void UpdatePaymentMethod(int userID, string creditCardToken)
        {
            string emailAddress = _databaseAccess.GetUserEmail(userID);
            string stripeCustomerID = this.CreateStripeCustomer(userID, emailAddress);
            var myCustomer = new StripeCustomerUpdateOptions
            {
                Card = new StripeCreditCardOptions { TokenId = creditCardToken }
            };

            var customerService = new StripeCustomerService();
            customerService.Update(stripeCustomerID, myCustomer);
        }

        /// <summary>
        /// Creates a stripe customer record for the selected user identifier.
        /// If the user already has a stripe customer object, their existing ID will be returned.
        /// </summary>
        /// <param name="userID">Identifier for the current user.</param>
        /// <param name="emailAddress">Email address for the current user.</param>
        /// <returns>The existing or new stripe customer identifier for the current user.</returns>
        public string CreateStripeCustomer(int userID, string emailAddress)
        {
            string stripeCustomerID;
            // If the user already has a customer record in stripe return the stripe identifier.
            if (HasStripeCustomerRecord(userID))
            {
                stripeCustomerID = GetStripeCustomerID(userID);
                return stripeCustomerID;
            }
            // If they don't create a new customer and return the identifier.
            var myCustomer = new StripeCustomerCreateOptions
            {
                Email = emailAddress,
                Description = string.Format("Local User ID: {0} Local Email Address: {1}", userID, emailAddress)
            };

            var customerService = new StripeCustomerService();
            StripeCustomer stripeCustomer = customerService.Create(myCustomer);
            stripeCustomerID = stripeCustomer.Id;
            this.SaveStripeCustomer(userID, stripeCustomerID);
            return stripeCustomerID;
        }

        /// <summary>
        /// Creates a new subscription for the stripe customer, returning the ID of the new subscription.
        /// </summary>
        public StripeSubscription CreateNewSubscription(string stripeCustomerID, int listingCost, string stripePlanID, DateTime? trialEnd, Dictionary<string, string> metadata)
        {
            StripeSubscriptionCreateOptions options = new StripeSubscriptionCreateOptions
            {
                Quantity = listingCost,
                TrialEnd = trialEnd,
                Metadata = metadata
            };

            var subscriptionService = new StripeSubscriptionService();
            StripeSubscription stripeSubscription = subscriptionService.Create(stripeCustomerID, stripePlanID, options);
            return stripeSubscription;
        }

        public void UpdateStripeSubscription(string customerID, string subscriptionID, int listingCost, bool prorate)
        {
            var subscriptionService = new StripeSubscriptionService();
            StripeSubscriptionUpdateOptions options = new StripeSubscriptionUpdateOptions
            {
                Quantity = listingCost,
                Prorate = prorate
            };
            subscriptionService.Update(customerID, subscriptionID, options);
            // If proration is set to true, we want them to pay immediately.
            // Creating the invoice below charges them the difference between subscription costs immediately.
            if (prorate)
            {
                var invoiceService = new StripeInvoiceService();
                invoiceService.Create(customerID);
            }
        }

        public void CancelStripeSubscription(string customerID, string subscriptionID, bool cancelAtPeriodEnd)
        {
            var subscriptionService = new StripeSubscriptionService();
            subscriptionService.Cancel(customerID, subscriptionID, cancelAtPeriodEnd);
        }

        public void ReactivateStripeSubscription(string customerID, string subscriptionID, string stripePlanID)
        {
            var subscriptionService = new StripeSubscriptionService();
            var subscriptionOptions = new StripeSubscriptionUpdateOptions
            {
                PlanId = stripePlanID
            };
            subscriptionService.Update(customerID, subscriptionID, subscriptionOptions);
        }

        public string ChargeProrationFee(string stripeCustomerIdentifier, int amount)
        {
            var charge = new StripeChargeCreateOptions
            {
                Amount = amount,
                Currency = "usd",
                Description = "Proration fee for listing subscription.",
                CustomerId = stripeCustomerIdentifier
            };
            var chargeService = new StripeChargeService();
            StripeCharge stripeCharge = chargeService.Create(charge);
            return stripeCharge.Id;
        }

        public IEnumerable<StripeCard> GetUserDefaultPaymentMethod(int userIdentifier)
        {
            var cardService = new StripeCardService();
            string stripeCustomerID = this.GetStripeCustomerID(userIdentifier);
            if (stripeCustomerID != null)
            {
                return cardService.List(stripeCustomerID);
            }
            return null;
        }

        public IEnumerable<StripeCard> GetUserDefaultPaymentMethod(string stripeCustomerID)
        {
            var cardService = new StripeCardService();
            if (stripeCustomerID != null)
            {
                return cardService.List(stripeCustomerID);
            }
            return null;
        }

        public StripeSubscription GetStripeSubscription(string stripeCustomerID, string stripeSubscriptionID)
        {
            var subscriptionService = new StripeSubscriptionService();
            StripeSubscription stripeSubscription = subscriptionService.Get(stripeCustomerID, stripeSubscriptionID);
            return stripeSubscription;
        }

        public void UpdateStripeCustomerEmail(int userIdentifier, string emailAddress)
        {
            if (!this.HasStripeCustomerRecord(userIdentifier)) return;

            var customer = new StripeCustomerUpdateOptions
            {
                Email = emailAddress,
                Description = string.Format("Local User ID: {0} Local Email Address: {1}", userIdentifier, emailAddress)
            };

            string stripeCustomerID = this.GetStripeCustomerID(userIdentifier);
            var customerService = new StripeCustomerService();
            customerService.Update(stripeCustomerID, customer);
        }
    }
}
