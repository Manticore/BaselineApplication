﻿using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class DowngradeSubscriptionModel
    {
        public Listing PreviewListing { get; set; }
        public int NewSubscriptionCost { get; set; }

        public string NewSubscriptionCostDisplay
        {
            get { return string.Format("${0:0.00}", (NewSubscriptionCost * 0.01)); }
        }
    }
}
