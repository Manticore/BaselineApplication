﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure updates a local subscription. 
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.UpdateLocalSubscription
       @SubscriptionID		 INT,
	   @DirectoryCount		 INT,
       @DescriptionLineCount INT = NULL,
	   @NewSubscriptionCost  INT,
	   @StripeSubscriptionID NVARCHAR(50) = NULL,
	   @UserID				 INT
AS
BEGIN
	
	SET NOCOUNT ON;

	-- Retrieve the current date.
    DECLARE @CurrentDate DATETIME2 = dbo.GetCurrentDate();

	-- Only need to update paid description lines when we receive a greater count.
	-- This is because we need to maintain the highest amount the user has paid for.
	-- The same goes for MaxSubscriptionCost. We need to keep track of this information in situations like below:
	-- Lets say a user pays for 10 description lines, but 6 months later they decide they only want 5 lines.
	-- If we were to update description lines to 5, we'd lose the fact that the user has actually paid for 10,
	-- and is entitled for 10 lines to show until the end of their subscription period. The same goes for
	-- the MaxSubscriptionCost field.
    UPDATE dbo.Subscriptions
           SET StripeSubscriptionID			= ISNULL(@StripeSubscriptionID, StripeSubscriptionID),
			   CurrentDirectoryCount		= @DirectoryCount,
			   CurrentDescriptionLinesCount = @DescriptionLineCount,
			   PaidDirectoryCount			= dbo.InlineMax(PaidDirectoryCount, @DirectoryCount),
			   PaidDescriptionLinesCount	= dbo.InlineMax(PaidDescriptionLinesCount, @DescriptionLineCount),
			   SubscriptionCost				= ISNULL(@NewSubscriptionCost, SubscriptionCost),
			   MaxSubscriptionCost			= dbo.InlineMax(MaxSubscriptionCost, @NewSubscriptionCost),
			   EditDate  = @CurrentDate,
			   EditUser  = @UserID
    WHERE SubscriptionID = @SubscriptionID;

END;