﻿CREATE TYPE [dbo].[DescriptionLinesTable_UDT] AS TABLE
(
	[DescriptionCode] NVARCHAR(50),
	[Description] NVARCHAR(250)
)

