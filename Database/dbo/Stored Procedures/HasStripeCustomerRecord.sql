﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns a boolean value indicating whether the user has an
-- associated Stripe customer ID.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.HasStripeCustomerRecord
       @UserID INT
AS
BEGIN

	SET NOCOUNT ON;

    IF EXISTS(SELECT '1' FROM dbo.StripeCustomers WHERE UserID = @UserID)
		SELECT 1;
    ELSE
		 SELECT 0;
        
END; 