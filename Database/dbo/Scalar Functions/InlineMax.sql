﻿
-------------------------------------------------------------------------------
-- Scalar-Valued Function Description						  
-------------------------------------------------------------------------------
-- This scalar valued function returns the greater of two numbers passed in.
-------------------------------------------------------------------------------
CREATE FUNCTION dbo.InlineMax(@val1 INT, @val2 INT)
RETURNS INT
AS
BEGIN
    IF @val1 > @val2
        BEGIN
            RETURN @val1
        END;
    RETURN ISNULL(@val2, @val1);
END;