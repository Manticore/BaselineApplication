﻿CREATE TABLE dbo.TradeLeads
(
    [TradeLeadID] INT NOT NULL PRIMARY KEY IDENTITY(1, 1), 
    [Title] NVARCHAR(MAX), 
    [Body] NVARCHAR(MAX), 
    [CreateDate] DATETIME2 DEFAULT GETDATE(), 
    [EditDate] DATETIME2 DEFAULT GETDATE(), 
    [CreateUser] INT NOT NULL, 
    [EditUser] INT NOT NULL, 
    [Views] INT NOT NULL DEFAULT 0
);
