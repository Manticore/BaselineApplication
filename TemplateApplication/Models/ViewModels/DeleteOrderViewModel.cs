﻿using Models.Models;

namespace TemplateApplication.Models.ViewModels
{
    public class DeleteOrderViewModel
    {
        public Order Order { get; set; }
        public bool OrderInvoiced { get; set; }
    }
}