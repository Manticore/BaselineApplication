﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure logs the most recent date a user has logged into the system.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.LogUserAccessDate
       @UserID INT, 
       @Date   DATETIME2
AS
BEGIN

    UPDATE dbo.AccessDates
           SET LastLoginDate = @Date
    WHERE UserID = @UserID;

    IF @@ROWCOUNT = 0
        BEGIN

          INSERT INTO dbo.AccessDates
		  (
			 UserID, 
			 LastLoginDate
		  )
          VALUES
		  (
			 @UserID, 
			 @Date
		  );

        END;
END;