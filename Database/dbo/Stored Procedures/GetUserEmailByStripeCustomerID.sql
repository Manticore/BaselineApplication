﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This stored procedure retrieves a local user's email address given their
-- Stripe customer identifier.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetUserEmailByStripeCustomerID
       @StripeCustomerID NVARCHAR(500)
AS
BEGIN

    SELECT EA.EmailAddress
    FROM StripeCustomers SC 
	   INNER JOIN EmailAddresses EA
		   ON EA.UserID = SC.UserID
    WHERE SC.StripeCustomerID = @StripeCustomerID;

END;