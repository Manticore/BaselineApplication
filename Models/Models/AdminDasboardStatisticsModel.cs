﻿
namespace Models.Models
{
    public class AdminDasboardStatisticsModel
    {
        public int PaymentsPerDay { get; set; }
        public int PaymentsPerWeek { get; set; }
        public int PaymentsPerMonth { get; set; }
        public int NewUsersPerDay { get; set; }
        public int NewUsersPerWeek { get; set; }
        public int NewUsersPerMonth { get; set; }
        public int NewListingsPerDay { get; set; }
        public int NewListingsPerWeek { get; set; }
        public int NewListingsPerMonth { get; set; }
    }
}
