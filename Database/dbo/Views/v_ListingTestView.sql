﻿
CREATE VIEW dbo.v_ListingTestView
    WITH SCHEMABINDING
AS 

SELECT 
    [ListingID],
    [BusinessName],
    [Description], 
    [ProductDescription],
	[Website],
	[ListingTypeID],
	ISNULL([BusinessName], '') + ' ' + ISNULL([Description], '') + ' ' + ISNULL([ProductDescription], '') + ' ' AS [ComputedText]
FROM dbo.Listings;