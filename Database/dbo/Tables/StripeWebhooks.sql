﻿CREATE TABLE [dbo].[StripeWebhooks]
(
	[StripeWebhookID] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[StripeInvoiceID] NVARCHAR(50),
	[Object] NVARCHAR(100),
	[Type] NVARCHAR(100),
	[CustomerID] NVARCHAR(50),
	[ChargeID] NVARCHAR(50),
	[WebhookDate] DATETIME2,
	[WebhookDeliveredAtDate] DATETIME2,
	[SubscriptionID] NVARCHAR(50),
	[Paid] BIT,
	[Total] INT, 
    [LocalReceivedDate] DATETIME2 NOT NULL,
	[ListingID] INT,
	[HandledSuccessfully] BIT
);
