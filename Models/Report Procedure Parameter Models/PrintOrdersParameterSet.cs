﻿using System;

namespace Models
{
    public class PrintOrdersParameterSet
    {
        public int Page { get; set; }
        public int NumberOfRecords { get; set; }
        public int? OrderID { get; set; }
        public long? OrderNumber { get; set; }
        public int? ListingIdentifier { get; set; }
        public string BusinessName { get; set; }
        public DateTime? CreateDateStart { get; set; }
        public DateTime? CreateDateEnd { get; set; }
        public bool? OrderPrinted { get; set; }
        public DateTime? LastPrintDateStart { get; set; }
        public DateTime? LastPrintDateEnd { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
    }
}
