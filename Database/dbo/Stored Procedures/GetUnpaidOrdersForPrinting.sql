﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns a paged set of orders that are marked unpaid to be 
-- displayed in the "Print Bills" utility.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetUnpaidOrdersForPrinting
       @Page			   INT, 
       @NumberOfRecords    INT,
	   @OrderID			   INT			 = NULL,
	   @OrderNumber		   BIGINT		 = NULL,
	   @ListingID		   INT			 = NULL,
	   @BusinessName	   NVARCHAR(250) = NULL,
	   @CreateDateStart    DATETIME2	 = NULL,
	   @CreateDateEnd	   DATETIME2	 = NULL,
	   @OrderPrinted	   BIT			 = NULL,
	   @LastPrintDateStart DATETIME2	 = NULL,
	   @LastPrintDateEnd   DATETIME2	 = NULL,
	   @SortColumn		   NVARCHAR(50)  = NULL,
	   @SortDirection	   NVARCHAR(10)  = NULL
AS
BEGIN

    SET NOCOUNT ON;
    
	DECLARE @UnpaidOrderStatus INT;
	SELECT @UnpaidOrderStatus = OS.OrderStatusID 
	FROM dbo.OrderStatuses OS 
	WHERE [Status] = 'Unpaid'

	DECLARE @ManualListingTypeID INT;
	SELECT @ManualListingTypeID = ListingTypeID 
	FROM dbo.ListingTypes 
	WHERE [Type] = 'Manual';

    -- Calculate the paging offset.
    DECLARE @OFFSET INT = (@Page - 1) * @NumberOfRecords;

    DECLARE @PrintingOrders TABLE
    (
	   OrderID		   INT, 
	   OrderNumber	   BIGINT, 
	   ListingID	   INT,
	   BusinessName	   NVARCHAR(250),
	   OrderTotal	   INT, 
	   OrderStatusID   INT, 
	   [Status]		   NVARCHAR(50), 
	   CreateDate	   DATETIME2, 
	   LastPrintedDate DATETIME2, 
	   IsPrinted	   NVARCHAR(10),
	   Total		   INT
    );

	INSERT INTO @PrintingOrders
	(
		[OrderID], 
		[OrderNumber], 
		[ListingID],
		[BusinessName],
		[OrderTotal], 
		[OrderStatusID], 
		[Status], 
		[CreateDate], 
		[LastPrintedDate], 
		[IsPrinted],
		[Total]
	)
	SELECT O.OrderID, 
		   O.OrderNumber, 
		   O.ListingID,
		   L.BusinessName,
		   O.OrderTotal, 
		   O.OrderStatusID, 
		   OS.[Status], 
		   O.CreateDate, 
		   LastPrintedDate,
		   CASE 
		   WHEN LastPrintedDate.OrderID IS NULL THEN 'No'
		   ELSE 'Yes' END AS IsPrinted, 
		   -- This calculates the total number of records in the set.
		   -- This is used for paging in order to calculate the number of pages.
		   Total = COUNT(1) OVER()
	FROM dbo.Orders O 
		LEFT JOIN(SELECT OrderID, 
						 MAX(CreateDate) AS LastPrintedDate
				  FROM dbo.InvoiceDates
				  GROUP BY OrderID) AS LastPrintedDate
			 ON O.OrderID = LastPrintedDate.OrderID
		INNER JOIN dbo.OrderStatuses OS
			 ON O.OrderStatusID = OS.OrderStatusID
		INNER JOIN dbo.Listings L 
			 ON (L.ListingID = O.ListingID AND L.ListingTypeID = @ManualListingTypeID)
	WHERE O.OrderStatusID = @UnpaidOrderStatus 
	AND (@OrderID IS NULL OR O.OrderID = @OrderID) 
	AND (@OrderNumber IS NULL OR O.OrderNumber = @OrderNumber) 
	AND (@ListingID IS NULL OR O.ListingID = @ListingID) 
	AND (@BusinessName IS NULL OR L.BusinessName LIKE '%' + @BusinessName + '%')
	AND (@CreateDateStart IS NULL OR (O.CreateDate >= @CreateDateStart)) 
	AND (@CreateDateEnd IS NULL OR (O.CreateDate <= @CreateDateEnd))
	AND (@OrderPrinted IS NULL OR (@OrderPrinted = CASE WHEN LastPrintedDate.OrderID IS NULL THEN 0 ELSE 1 END))
	AND (@LastPrintDateStart IS NULL OR LastPrintedDate >= @LastPrintDateStart) 
	AND (@LastPrintDateEnd IS NULL OR LastPrintedDate <= @LastPrintDateEnd)
		ORDER BY
			CASE WHEN @SortColumn = 'OrderID' AND @SortDirection = 'Ascendant'
				  THEN O.OrderID END ASC, 
			CASE WHEN @SortColumn = 'OrderID' AND @SortDirection = 'Descendant' 
				  THEN O.OrderID END DESC,
			CASE WHEN @SortColumn = 'OrderNumber' AND @SortDirection = 'Ascendant'
				  THEN O.OrderNumber END ASC, 
			CASE WHEN @SortColumn = 'OrderNumber' AND @SortDirection = 'Descendant' 
				  THEN O.OrderNumber END DESC,
			CASE WHEN @SortColumn = 'ListingID' AND @SortDirection = 'Ascendant'
				  THEN O.ListingID END ASC, 
			CASE WHEN @SortColumn = 'ListingID' AND @SortDirection = 'Descendant' 
				  THEN O.ListingID END DESC,
			CASE WHEN @SortColumn = 'BusinessName' AND @SortDirection = 'Ascendant'
				  THEN L.BusinessName END ASC, 
			CASE WHEN @SortColumn = 'BusinessName' AND @SortDirection = 'Descendant' 
				  THEN L.BusinessName END DESC,
			CASE WHEN @SortColumn = 'OrderTotalDisplay' AND @SortDirection = 'Ascendant'
				  THEN O.OrderTotal END ASC, 
			CASE WHEN @SortColumn = 'OrderTotalDisplay' AND @SortDirection = 'Descendant'
				  THEN O.OrderTotal END DESC,
			CASE WHEN @SortColumn = 'CreateDateDisplay' AND @SortDirection = 'Ascendant'
				  THEN O.CreateDate END ASC, 
			CASE WHEN ((@SortColumn IS NULL) OR (@SortColumn = 'CreateDateDisplay' AND @SortDirection = 'Descendant'))
				  THEN O.CreateDate END DESC,
			CASE WHEN @SortColumn = 'IsPrinted' AND @SortDirection = 'Ascendant'
				  THEN LastPrintedDate.OrderID END ASC, 
			CASE WHEN @SortColumn = 'IsPrinted' AND @SortDirection = 'Descendant' 
				  THEN LastPrintedDate.OrderID END DESC,
			CASE WHEN @SortColumn = 'LastPrintedDateDisplay' AND @SortDirection = 'Ascendant'
				  THEN LastPrintedDate.LastPrintedDate END ASC, 
			CASE WHEN @SortColumn = 'LastPrintedDateDisplay' AND @SortDirection = 'Descendant' 
				  THEN LastPrintedDate.LastPrintedDate END DESC
		OFFSET @OFFSET ROWS FETCH NEXT @NumberOfRecords ROWS ONLY;

    -- Return the total number of records.
    SELECT TOP 1 Total FROM @PrintingOrders;
    
    -- Return the result set.
    SELECT OrderID, 
           OrderNumber, 
           ListingID,
		   BusinessName,
           OrderTotal, 
           OrderStatusID, 
           [Status], 
           CreateDate, 
           LastPrintedDate,
		   IsPrinted
    FROM @PrintingOrders;

END;