﻿using System;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using DataTables.Mvc;
using Models.Interfaces;
using Models.Models;
using TemplateApplication.Filters;

namespace TemplateApplication.Controllers
{
    [Authorize(Roles = "Administrator")]
    [InitializeSimpleMembership]
    public class ReportsController : Controller
    {
        private const int StartingPage   = 1;
        private const int RecordsPerPage = 10;

        private readonly IDatabaseContext _database;

        public ReportsController(IDatabaseContext database)
        {
            _database = database;
        }

        // This is the Action method for the "Site Errors" page.
        // This report displays errors captured by ELMAH.
        public ActionResult SiteErrorsReport()
        {
            PagedList<SiteError> errors = _database.GetSiteErrorsPaged(StartingPage, RecordsPerPage);
            return View(errors);
        }

        #region Site Errors Datatable Code

        // POST Request used by the Site Errors datatable for paging.
        [HttpPost]
        public string GetSiteErrorsPaged([ModelBinder(typeof(DataTablesJsonBinder))] DefaultDataTablesRequest requestModel)
        {
            int start = requestModel.Start;
            int length = requestModel.Length;
            int page = (int)Math.Ceiling((double)start / length) + 1;

            var results = _database.GetSiteErrorsPaged(page, length);
            return new JavaScriptSerializer().Serialize(new DataTablesResponse(requestModel.Draw, results.PagedListItems, results.TotalRecords, results.TotalRecords));
        }

        #endregion
    }
}