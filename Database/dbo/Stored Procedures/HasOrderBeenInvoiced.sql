﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns a boolean value indicating if the specified order
-- has been invoiced. i.e. Has it been printed using the "Print Bills" utility.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.HasOrderBeenInvoiced
       @OrderID INT
AS
BEGIN

	SET NOCOUNT ON;

    IF EXISTS(SELECT 1
              FROM dbo.InvoiceDates
              WHERE OrderID = @OrderID)
		  SELECT 1;
    ELSE
		  SELECT 0;

END;