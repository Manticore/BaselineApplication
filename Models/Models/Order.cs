﻿using System;
using System.Collections.Generic;

namespace Models.Models
{
    public class Order
    {
        public Order()
        {
            if (OrderLineItems == null)
            {
                OrderLineItems = new List<OrderLineItem>();
            }
        }

        public DateTime CreateDate { get; set; }

        public string   CreateDateDisplay { get { return CreateDate.ToShortDateString(); } }

        public IEnumerable<InvoiceDate> InvoiceDates { get; set; }
        public int?     ListingID { get; set; }
        public string BusinessName { get; set; }
        public int      OrderID { get; set; }
        public List<OrderLineItem> OrderLineItems { get; set; }
        public long     OrderNumber { get; set; }
        public int      OrderStatusID { get; set; }
        public int      OrderTotal { get; set; }

        public string   OrderTotalDisplay { get { return string.Format("${0:0.00}", (OrderTotal * 0.01)); } }

        public Payment  Payment { get; set; }
        public string   Status { get; set; }
        public int      UserID { get; set; }
        public string   UserName { get; set; }
    }
}