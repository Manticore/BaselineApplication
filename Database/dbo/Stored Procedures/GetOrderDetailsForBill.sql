﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the details necessary for the PDF bill.
-------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GetOrderDetailsForBill]
	@OrderID INT
AS
BEGIN

	SET NOCOUNT ON;

	-- Return details about the listing the order is associated with.
	SELECT L.[BusinessName], 
		   L.[BusinessContact], 
		   A.[Street1], 
		   A.[Street2], 
		   A.[City], 
		   S.[Abbreviation] AS StateAbbreviation,
		   S.[Name] AS StateName,
		   A.Zip
	FROM dbo.Orders O 
		 INNER JOIN [dbo].[Listings] L
			 ON O.ListingID = L.ListingID
		 INNER JOIN [dbo].[Listings_Addresses] LA
			 ON LA.ListingID = L.ListingID
		 INNER JOIN [dbo].[Addresses] A
			 ON A.AddressID = LA.AddressID
		 INNER JOIN [dbo].[States] S
			 ON S.StateID = A.StateID
	WHERE O.OrderID = @OrderID;

	-- Get the order information.
    SELECT OrderID, 
           ListingID,
		   OrderNumber,
           OrderTotal,
		   OS.OrderStatusID,
		   OS.Status,
           CreateDate
    FROM dbo.Orders O 
		INNER JOIN dbo.OrderStatuses OS 
			ON OS.OrderStatusID = O.OrderStatusID
    WHERE O.OrderID = @OrderID;

	-- Return the line items for the specified order.
	SELECT [OrderLineItemID],
		   [OrderID],
		   [Description],
		   [LineItemTotal]
	FROM dbo.OrderLineItems 
	WHERE OrderID = @OrderID

END;