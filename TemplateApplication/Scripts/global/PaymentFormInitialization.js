﻿
(function () {
    $(document).ready(function () {
        // Convert the expiration month select to a bootstrap select.
        $('#expiration-month-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check',
            size: 8
        });

        // Convert the expiration year select to a bootstrap select.
        $('#expiration-year-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check',
            size: 8
        });

        $('#state-select').selectpicker({
            width: "100%"
        });

        function addInputNames() {
            // Not ideal, but jQuery's validate plugin requires fields to have names
            // so we add them at the last possible minute, in case any javascript 
            // exceptions have caused other parts of the script to fail.
            $("#number").attr("name", "card-number");
            $("#cvc").attr("name", "card-cvc");
            $("#expiration-month-select").attr("name", "card-expiration-month");
            $("#expiration-year-select").attr("name", "card-expiration-year");
            $("#name").attr("name", "card-name");
            $("#street1").attr("name", "card-street1");
            $("#city").attr("name", "card-city");
            $("#state-select").attr("name", "card-state");
            $("#zip").attr("name", "card-zip");
        }

        function removeInputNames() {
            $("#number").removeAttr("name", "card-number");
            $("#cvc").removeAttr("name", "card-cvc");
            $("#expiration-month-select").removeAttr("name", "card-expiration-month");
            $("#expiration-year-select").removeAttr("name", "card-expiration-year");
            $("#name").removeAttr("name", "card-name");
            $("#street1").removeAttr("name", "card-street1");
            $("#city").removeAttr("name", "card-city");
            $("#state-select").removeAttr("name", "card-state");
            $("#zip").removeAttr("name", "card-zip");
        }

        // Add custom rules for credit card validating.
        // Requires Stripe.js.
        jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card number");
        jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
        jQuery.validator.addMethod("cardExpiry", function () {
            return Stripe.validateExpiry($("#expiration-month-select").val(), $("#expiration-year-select").val());
        }, "Please enter a valid expiration");

        var paymentForm = $('#payment-form');
        paymentForm.validate({
            ignore: ":not(select:hidden, input:visible, textarea:visible), .ignore",
            // This option enables to show the error/success messages on tab switch.
            doNotHideMessage: true,
            // Default input error message container.
            errorElement: 'span',
            // Default input error message class.
            errorClass: 'help-block help-block-error',
            // Do not focus the last invalid input.
            focusInvalid: false,
            rules: {
                'card-number': {
                    required: true,
                    cardNumber: true
                },
                'card-cvc': {
                    required: true,
                    cardCVC: true
                },
                'card-expiration-month': {
                    required: true
                },
                'card-expiration-year': {
                    required: true,
                    cardExpiry: true
                },
                'card-name': {
                    required: true,
                },
                'card-street1': {
                    required: true,
                },
                'card-city': {
                    required: true,
                },
                'card-state': {
                    required: true,
                },
                'card-zip': {
                    required: true,
                }
            },
            messages: {
                'card-number': {
                    required: "Please provide your card number."
                },
                'card-cvc': {
                    required: "Please provide your CVC code."
                },
                'card-expiration-month': {
                    required: "Please provide your card's expiration month."
                },
                'card-expiration-year': {
                    required: "Please provide your card's expiration year.",
                    cardExpiry: "Please provide a valid expiration date."
                },
                'card-name': {
                    required: "Please provide your name."
                },
                'card-street1': {
                    required: "Please provide your billing street."
                },
                'card-city': {
                    required: "Please provide your billing city"
                },
                'card-state': {
                    required: "Please provide your billing state."
                },
                'card-zip': {
                    required: "Please provide your billing zip code."
                }
            },

            errorPlacement: function (error, element) {
                if (element.attr("name") == "card-expiration-month")
                    error.insertAfter("#expiration-month-container .bootstrap-select");
                else if (element.attr("name") == "card-expiration-year")
                    error.insertAfter("#expiration-year-container .bootstrap-select");
                else if (element.attr("name") == "card-state")
                    error.insertAfter("#state-select-container .bootstrap-select");
                else
                    error.insertAfter(element); // For all other inputs, the default behavior.
            },

            highlight: function (element) {
                // Hightlight error inputs.
                // Set error class to the control group.
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },

            unhighlight: function (element) {
                // Revert the change done by hightlight.
                // Set error class to the control group.
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                // Mark the current input as valid and display OK icons.
                // Set success class to the control group
                label.addClass('valid').closest('.form-group').removeClass('has-error').addClass('has-success');
            },

            submitHandler: function (form) {
                removeInputNames();
                $.event.trigger({
                    type: "paymentFormSubmitted",
                    message: "Payment form has been submitted.",
                    time: new Date()
                });
                addInputNames();
            }
        });

        // Needed to validate the bootstrap selects correctly.
        $('#payment-form select').on('change', function (e) {
            $('#payment-form').validate().element($(this));
        });

        addInputNames();
    });
}());