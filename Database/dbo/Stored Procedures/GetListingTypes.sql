﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns all the listing types available in the database.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetListingTypes
AS
BEGIN

	SET NOCOUNT ON;

    SELECT ListingTypeID,
		   [Type]
    FROM dbo.ListingTypes;

END;
