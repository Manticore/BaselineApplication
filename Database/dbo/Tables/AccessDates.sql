﻿CREATE TABLE [dbo].[AccessDates]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	UserID INT NOT NULL,
	LastLoginDate DATETIME2 NOT NULL
)
