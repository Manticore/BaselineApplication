﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns all the industries available in the site.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetIndustries
AS
BEGIN

    SET NOCOUNT ON;

    SELECT IndustryID, 
           Name
    FROM dbo.Industries;

END;
