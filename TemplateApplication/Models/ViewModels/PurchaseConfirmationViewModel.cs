﻿
namespace TemplateApplication.Models.ViewModels
{
    public class PurchaseConfirmationViewModel
    {
        public int ListingIdentifier { get; set; }
        public int SubscriptionCost { get; set; }

        public string SubscriptionCostDisplay
        {
            get { return string.Format("${0:0.00}", (SubscriptionCost * 0.01)); }
        }
    }
}