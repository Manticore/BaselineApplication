﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- After a stripe webhook is received, it is saved in the database using the 
-- SaveStripeWebhook procedure. After the webhook is processed, the StripeWebhook
-- informational record is updated in the system if the process that consumes
-- the webhooks was successful. Also, a listing identifier may be associated with
-- the webhook is the listing identifier is passed.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.SaveWebhookStatus
    @StripeWebhookID INT,
    @Success		 BIT,
    @ListingID		 INT
AS
BEGIN

    UPDATE dbo.StripeWebhooks
           SET ListingID = @ListingID, 
               HandledSuccessfully = @Success
    WHERE StripeWebhookID = @StripeWebhookID;

END;