﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure determins whether a specified email address is already
-- associated with a user in the database.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.EmailExists
       @EmailAddress NVARCHAR(320)
AS
BEGIN

	SET NOCOUNT ON;

    IF EXISTS(SELECT 1 FROM dbo.EmailAddresses WHERE EmailAddress = @EmailAddress)
            SELECT 1;
    ELSE
            SELECT 0;

END;