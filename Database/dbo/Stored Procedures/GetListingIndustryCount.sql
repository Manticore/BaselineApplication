﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns all the industries that are associated with a listing
-- along with the number of listings that industry is associated with.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetListingIndustryCount
AS
BEGIN

	SET NOCOUNT ON;

    SELECT I.IndustryID, 
           I.Name, 
           COUNT(*) AS IndustryCount
    FROM dbo.Listings_Industries LI 
	   INNER JOIN dbo.Industries I
             ON LI.IndustryID = I.IndustryID
    GROUP BY I.IndustryID, 
             Name;

END;