﻿using System;
using Library.Exporter;

namespace Models
{
    public class BillableListingsModel
    {
        [CsvColumnName(Name = "ListingID", Order = 1)]
        public int ListingID { get; set; }

        [CsvColumnName(Name = "BusinessName", Order = 2)]
        public string BusinessName { get; set; }

        public DateTime ListingCreateDate { get; set; }
        public DateTime? LastSubscriptionDate { get; set; }

        [CsvColumnName(Name = "OrderGenerated", Order = 5)]
        public string OrderGenerated { get; set; }

        public DateTime? GeneratedDate { get; set; }

        [CsvColumnName(Name = "ListingCreateDate", Order = 3)]
        public string ListingCreateDateDisplay
        {
            get { return ListingCreateDate.ToShortDateString(); }
        }

        [CsvColumnName(Name = "LastSubscriptionDate", Order = 4)]
        public string LastSubscriptionDateDisplay
        {
            get
            {
                if (LastSubscriptionDate.HasValue)
                {
                    return LastSubscriptionDate.Value.ToShortDateString();
                }
                return null;
            }
        }

        [CsvColumnName(Name = "GeneratedDate", Order = 6)]
        public string GeneratedDateDisplay
        {
            get
            {
                if (GeneratedDate.HasValue)
                    return GeneratedDate.Value.ToShortDateString();
                return null;
            }
        }
    }
}
