﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns a set of listings broken up into pages. This procedure
-- is what is used when users of the site are searching for listings. They are
-- able to filter the listings results using a number of parameters.
-------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GetListings]
	@Page			 INT		   = NULL,
	@NumberOfRecords INT		   = NULL,
	@Keyword     	 NVARCHAR(100) = NULL,
	@DirectoryID	 INT		   = NULL,
	@SelectedIndustryIDs IndustryIdentifierTable_UDT READONLY,
	@StateID		 INT		   = NULL
AS
BEGIN 

	SET NOCOUNT ON;

	-- Grab the current date setting to be used in the stored procedure.
	DECLARE @CurrentDate DATETIME2 = dbo.GetCurrentDate();

	DECLARE @OFFSET INT = ((@Page - 1) * @NumberOfRecords)

	DECLARE @Listings TABLE 
	(
		[ListingID]			 INT,
		[BusinessName]		 NVARCHAR(250),
		[BusinessContact]	 NVARCHAR(250),
		[Description]		 NVARCHAR(MAX),
		[ProductDescription] NVARCHAR(MAX),
		[Website]			 NVARCHAR(100),
		[ListingTypeID]		 INT
	);

	INSERT INTO @Listings 
	(
		[ListingID],
		[BusinessName],
		[Description],
		[ProductDescription],
		[Website],
		[ListingTypeID]
	)
	SELECT LV.[ListingID],
		   [BusinessName],
		   [Description],
		   [ProductDescription],
		   [Website],
		   [ListingTypeID]
	FROM dbo.v_ListingTestView LV
	LEFT JOIN dbo.Listings_Addresses LA
		ON LA.ListingID = LV.ListingID
	LEFT JOIN dbo.Addresses A 
		ON LA.AddressID = A.AddressID
	WHERE ((NOT EXISTS(SELECT 1
				    FROM @SelectedIndustryIDs) 
		OR EXISTS(SELECT 1
				 FROM dbo.Listings_Industries LI 
				 INNER JOIN @SelectedIndustryIDs SIDS
				 ON LI.IndustryID = SIDS.IndustryID
				 WHERE LV.ListingID = LI.ListingID
				 )
		))
	AND ((@Keyword IS NULL OR LV.ComputedText LIKE '%' + @Keyword + '%') 
	OR (@Keyword IS NULL OR EXISTS(SELECT 1 FROM v_Descriptions D 
									WHERE D.ComputedText LIKE '%' + @Keyword + '%'
									AND D.ListingID = LV.ListingID)))
	AND (@DirectoryID IS NULL OR EXISTS(SELECT 1 FROM dbo.Listings_Directories LD 
										WHERE DirectoryID = @DirectoryID 
										AND LD.ListingID = LV.ListingID))
	AND (@StateID IS NULL OR A.StateID = @StateID)
	ORDER BY LV.BusinessName DESC 
	OFFSET @OFFSET ROWS
	FETCH NEXT @NumberOfRecords ROWS ONLY;

	SELECT [ListingID], 
		   [BusinessName], 
		   [Description], 
		   [ProductDescription], 
		   [Website],
		   L.[ListingTypeID],
		   LT.Type
	FROM @Listings L
	INNER JOIN dbo.ListingTypes LT 
		ON L.ListingTypeID = LT.ListingTypeID

	-- Return all of the industry tags for the listings that are returned.
	SELECT I.IndustryID,
		   I.Name,
		   LI.ListingID
	FROM dbo.Listings_Industries LI
		INNER JOIN dbo.Industries I 
			ON LI.IndustryID = I.IndustryID
	-- Return the industry tags for only the TradeLeads return above.
	WHERE LI.ListingID IN (SELECT ListingID FROM @Listings);

	-- Return all the description lines for the listings that are returned.
	SELECT D.ListingID,
		   D.DescriptionID,
		   D.Code,
		   D.[Body]
    FROM dbo.Descriptions D
    WHERE D.ListingID IN (SELECT ListingID FROM @Listings);

	-- Return the addresses for the selected listings.
	SELECT LA.ListingID,
		   A.AddressID, 
		   A.Street1, 
		   A.Street2, 
		   A.City, 
		   A.State, 
		   A.Zip
	FROM dbo.Listings_Addresses LA 
		INNER JOIN dbo.v_Addresses A
			 ON LA.AddressID = A.AddressID
	WHERE LA.ListingID IN((SELECT ListingID FROM @Listings)); 

	-- Return the current subscriptions for the listings returned.
	SELECT * FROM dbo.GetCurrentSubscriptions() 
	WHERE GetCurrentSubscriptions.ListingID IN (SELECT ListingID FROM @Listings)

END;