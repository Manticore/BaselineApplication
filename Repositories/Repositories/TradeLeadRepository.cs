﻿using Models;
using Models.Interfaces;
using System;
using System.Collections.Generic;
using Models.Models;

namespace Repositories.Repositories
{
    public class TradeLeadRepository : ITradeLeadRepository
    {
        private readonly IDatabaseContext _databaseAccess;

        public TradeLeadRepository(IDatabaseContext databaseAccess)
        {
            _databaseAccess = databaseAccess;
        }

        public TradeLead GetSpecificTradeLead(int tradeLeadID)
        {
            return _databaseAccess.GetSpecificTradeLead(tradeLeadID);
        }

        public IEnumerable<TradeLead> GetTradeLeads(int page, int numberOfRecords, string keyword, List<int?> industryIDs, DateTime? start, DateTime? end)
        {
            return _databaseAccess.GetTradeLeads(page, numberOfRecords, keyword, industryIDs, start, end);
        }

        public PagedList<TradeLead> GetTradeLeadsPaged(TradeLeadParameterSet parameters)
        {
            return _databaseAccess.GetTradeLeadsPaged(parameters);
        }

        public void SaveTradeLeadView(int tradeLeadID)
        {
            _databaseAccess.SaveTradeLeadView(tradeLeadID);
        }

        public IEnumerable<TradeLead> GetTopTradeLeads()
        {
            return _databaseAccess.GetTopTradeLeads();
        }

        public void UpsertTradeLead(TradeLead tradeLead, int userID)
        {
            _databaseAccess.UpsertTradeLead(tradeLead, userID);
        }

        public void DeleteTradeLead(int tradeLeadID)
        {
            _databaseAccess.DeleteTradeLead(tradeLeadID);
        }
    }
}