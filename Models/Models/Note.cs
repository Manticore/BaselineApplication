﻿using System;

namespace Models.Models
{
    public class Note
    {
        public int NoteID { get; set; }
        public int ListingID { get; set; }
        public string NoteText { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateUser { get; set; }
        public string CreateUsername { get; set; }
        public int EditUser { get; set; }
    }
}
