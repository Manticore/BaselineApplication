﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure associates a given user identifier with a listing.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.LinkListingToUserID
       @ListingID INT, 
       @UserID	  INT
AS
BEGIN

    UPDATE dbo.Listings
           SET UserID = @UserID
    WHERE ListingID = @ListingID;

END;