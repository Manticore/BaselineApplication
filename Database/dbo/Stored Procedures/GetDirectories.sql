﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the Directories available in the site.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetDirectories
AS
BEGIN

    SET NOCOUNT ON;

    SELECT [DirectoryID], 
           [Name], 
           [Description],
		   [IsDefault]
    FROM dbo.Directories;

END; 
