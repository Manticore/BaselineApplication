﻿
-------------------------------------------------------------------------------
-- Scalar-Valued Function Description						  
-------------------------------------------------------------------------------
-- This function returns the current date for the site. If the setting
-- "CurrentDate" has a value, that date is used for the system date.
-- If not, the current universal time date is used instead. The "CurrentDate"
-- setting is used for testing purposes ONLY.
-------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[GetCurrentDate]
(
)
RETURNS DATETIME2
AS
BEGIN

	-- Grab the current date setting to be used in the stored procedure.
	DECLARE @CurrentDate DATETIME2 
	SELECT @CurrentDate = Value 
	FROM dbo.Settings 
	WHERE SettingName = 'CurrentDate';

	IF ((@CurrentDate IS NULL) OR (RTRIM(LTRIM(@CurrentDate)) = ''))
		SET @CurrentDate = SYSUTCDATETIME();

	RETURN @CurrentDate

END
