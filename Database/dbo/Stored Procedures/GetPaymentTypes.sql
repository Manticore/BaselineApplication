﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the payment types available in the database. If the 
-- @IsAdmin flag is set, additional payment types are returned that an 
-- administrator can utilize.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetPaymentTypes
	@IsAdmin BIT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT [PaymentTypeID], 
		   [Type], 
		   [IsAdminType]
	FROM dbo.PaymentTypes
	WHERE @IsAdmin IS NULL OR (IsAdminType = @IsAdmin OR PaymentTypes.IsAdminType = 0);

END;