﻿namespace Models
{
    public class BulkListingParameterSet
    {
        public int Page { get; set; }
        public int NumberOfRecords { get; set; }
        public int? ListingIdentifier { get; set; }
        public string BusinessName { get; set; }
        public string FaxNumber { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }
    }
}
