﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns a minimal ammount of the Listing. This procedure
-- is primarily used to return a specified listing for display in the site.
-- i.e. When a user of the site is browsing the listings in the site. They 
-- just need to see the listing's basic information. The procedure 
-- "GetSpecifiedListingFull" is used for administrative tasks. The listing object
-- returned from this procedure contains very detailed information.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetSpecificListing
       @ListingID INT
AS
BEGIN 

	SET NOCOUNT ON;

	-- Grab the current date setting to be used in the stored procedure.
	DECLARE @CurrentDate DATETIME2 = dbo.GetCurrentDate();

    -- Return the listing.
    SELECT L.ListingID, 
           L.BusinessName,
		   L.BusinessContact,
		   L.ContactEmail,
		   L.ContactNumber,
		   L.FaxNumber,
           L.[Description], 
           L.ProductDescription, 
           L.Website, 
           L.CreateDate, 
           L.CreateUser, 
           L.EditDate, 
           L.EditUser
    FROM dbo.Listings L
    WHERE L.ListingID = @ListingID;

    -- Return the industry tags associated with this listing.
    SELECT I.IndustryID, 
           I.Name
    FROM dbo.Listings_Industries LI 
    INNER JOIN dbo.Industries I
	   ON I.IndustryID = LI.IndustryID
    WHERE LI.ListingID = @ListingID;

	-- Return the addresses for the selected listings.
	SELECT LA.ListingID,
		   A.AddressID, 
		   A.Street1, 
		   A.Street2, 
		   A.City, 
		   A.State,
		   A.StateID,
		   A.Zip
	FROM dbo.Listings_Addresses LA 
		INNER JOIN dbo.v_Addresses A
			 ON LA.AddressID = A.AddressID
	WHERE LA.ListingID = @ListingID

	-- Return the Directories for this listing.
    SELECT LD.ListingID,
		   LD.DirectoryID,
		   D.Name
    FROM dbo.Listings_Directories LD
	INNER JOIN Directories D 
		ON LD.DirectoryID = D.DirectoryID
    WHERE LD.ListingID = @ListingID;

    -- Return the Description lines for this listing.
    SELECT D.DescriptionID,
		   D.Code,
		   D.[Body]
    FROM dbo.Descriptions D
    WHERE D.ListingID = @ListingID;

END;