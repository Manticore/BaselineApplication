﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure retrieves the listing records in the site that need to be
-- managed by administrators. This procedure does not include any of the free 
-- listing objects. All the listings returned are either managed by stripe, or
-- they are manual listings. The reason other listing procedures weren't used
-- is they utilize many other unnecessary tables.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetManagedListings
       @Page			  INT			= NULL, 
       @NumberOfRecords	  INT			= NULL,
	   @ListingIdentifier INT			= NULL,
	   @BusinessName	  NVARCHAR(250) = NULL,
	   @ListingTypeID	  INT			= NULL,
	   @IsCancelled		  BIT			= NULL,
	   @SortColumn		  NVARCHAR(50)  = NULL,
	   @SortDirection	  NVARCHAR(10)  = NULL
AS
BEGIN

    SET NOCOUNT ON;

    -- Grab the current date setting to be used in the stored procedure.
    DECLARE @CurrentDate DATETIME = dbo.GetCurrentDate();

    DECLARE @OFFSET INT = (@Page - 1) * @NumberOfRecords;

    DECLARE @OnlineListingTypeID INT;
    DECLARE @ManualListingTypeID INT;

	DECLARE @ManagedListings TABLE
    (
	    ListingID			  INT, 
        BusinessName		  NVARCHAR(250), 
        BusinessContact		  NVARCHAR(250),
		UserID				  INT,
		ListingTypeID		  INT,
		[Type]				  NVARCHAR(50),
		CancelledDate		  DATETIME,
		CreateDate			  DATETIME,
		CurrentSubscriptionID INT,
        TotalRecords		  INT
    );

	-- Return only the 'Online' or 'Manual' type listings.
    SELECT @OnlineListingTypeID = ListingTypeID
    FROM dbo.ListingTypes
    WHERE [Type] = 'Online';
    
    SELECT @ManualListingTypeID = ListingTypeID
    FROM dbo.ListingTypes
    WHERE [Type] = 'Manual';

	INSERT INTO @ManagedListings 
	(
		ListingID, 
        BusinessName, 
        BusinessContact,
		UserID,
		ListingTypeID,
		[Type],
		CancelledDate,
		CreateDate,
		CurrentSubscriptionID,
        TotalRecords
	)
    SELECT L.ListingID, 
           BusinessName, 
           BusinessContact, 
           UserID, 
           LT.ListingTypeID,
		   LT.[Type],
           L.CancelledDate, 
           L.CreateDate,
		   CurrentSubscriptions.SubscriptionID,
		   TotalRecords = COUNT(1) OVER()
    FROM dbo.Listings L
		INNER JOIN dbo.ListingTypes LT 
			ON L.ListingTypeID = LT.ListingTypeID
		LEFT JOIN dbo.GetCurrentSubscriptions() CurrentSubscriptions
			ON L.ListingID = CurrentSubscriptions.ListingID
    WHERE LT.ListingTypeID IN (@OnlineListingTypeID, @ManualListingTypeID)
	AND (@ListingIdentifier IS NULL OR L.ListingID = @ListingIdentifier)
	AND (@BusinessName IS NULL OR (L.BusinessName LIKE '%'+ @BusinessName + '%'))
	AND (@ListingTypeID IS NULL OR LT.ListingTypeID = @ListingTypeID)
	AND (@IsCancelled IS NULL OR @IsCancelled = CASE WHEN L.CancelledDate IS NULL THEN 0 ELSE 1 END)
	ORDER BY 
		CASE WHEN @SortColumn = 'ListingID' AND @SortDirection = 'Ascendant'
			  THEN L.ListingID END ASC, 
		CASE WHEN @SortColumn = 'ListingID' AND @SortDirection = 'Descendant' 
			  THEN L.ListingID END DESC,
		CASE WHEN  (@SortColumn IS NULL) OR (@SortColumn = 'BusinessName' AND @SortDirection = 'Ascendant')
			  THEN L.BusinessName END ASC, 
		CASE WHEN @SortColumn = 'BusinessName' AND @SortDirection = 'Descendant' 
			  THEN L.BusinessName END DESC,
		CASE WHEN @SortColumn = 'BusinessContactDisplay' AND @SortDirection = 'Ascendant'
			  THEN L.BusinessContact END ASC, 
		CASE WHEN @SortColumn = 'BusinessContactDisplay' AND @SortDirection = 'Descendant' 
			  THEN L.BusinessContact END DESC,
		CASE WHEN @SortColumn = 'Type' AND @SortDirection = 'Ascendant'
			  THEN LT.[Type] END ASC, 
		CASE WHEN @SortColumn = 'Type' AND @SortDirection = 'Descendant' 
			  THEN LT.[Type] END DESC,
		CASE WHEN @SortColumn = 'CancelledDateDisplay' AND @SortDirection = 'Ascendant'
			  THEN L.CancelledDate END ASC, 
		CASE WHEN @SortColumn = 'CancelledDateDisplay' AND @SortDirection = 'Descendant' 
			  THEN L.CancelledDate END DESC,
		CASE WHEN @SortColumn = 'CurrentSubscriptionID' AND @SortDirection = 'Ascendant'
			  THEN CurrentSubscriptions.SubscriptionID END ASC, 
		CASE WHEN @SortColumn = 'CurrentSubscriptionID' AND @SortDirection = 'Descendant' 
			  THEN CurrentSubscriptions.SubscriptionID END DESC
    OFFSET @OFFSET ROWS FETCH NEXT @NumberOfRecords ROWS ONLY;

	-- Return the total number of records for paging.
    SELECT TOP 1 TotalRecords FROM @ManagedListings;

    -- Return the listings.
    SELECT * FROM @ManagedListings;

END;