using Models.Interfaces;
using Services;
[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(TemplateApplication.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(TemplateApplication.NinjectWebCommon), "Stop")]

// Dependency injection configuration. Ninject is used.
namespace TemplateApplication
{
    using System;
    using System.Web;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Common;
    using Repositories.Repositories;
    using DatabaseAccess;
    using System.Configuration;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            Bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            Bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            // Retrieve the database connection string.
            var databaseConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

            // The database access class requires a connection string parameter.
            kernel.Bind<IDatabaseContext>().To<DataAccess>().WithConstructorArgument("connectionString", databaseConnectionString);
            kernel.Bind<IListingRepository>().To<ListingRepository>();
            kernel.Bind<ITradeLeadRepository>().To<TradeLeadRepository>();
            kernel.Bind<IPaymentRepository>().To<StripePaymentRepository>();
            kernel.Bind<IListingService>().To<ListingService>();
            kernel.Bind<IOrderRepository>().To<OrderRepository>();
            kernel.Bind<IListingCostCalculator>().To<ListingCostCalculator>();
            kernel.Bind<IValidationService>().To<ValidationService>();
        }        
    }
}
