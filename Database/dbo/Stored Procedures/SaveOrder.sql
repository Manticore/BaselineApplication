﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure is responsible for saving an order record in the database.
-------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SaveOrder]
	@ListingID INT,
	@OrderTotal INT,
	@OrderLineItems OrderLineItemTable_UDT READONLY,
	@IsPaid BIT,
	@UserID INT
AS
BEGIN

	-- Grab the current date setting to be used in the stored procedure.
	DECLARE @CurrentDate DATETIME2 = dbo.GetCurrentDate();

	DECLARE @OrderStatusID INT
	IF @IsPaid = 1
		SELECT @OrderStatusID = OrderStatusID FROM dbo.OrderStatuses WHERE [Status] = 'Paid'
	ELSE 
		SELECT @OrderStatusID = OrderStatusID FROM dbo.OrderStatuses WHERE [Status] = 'Unpaid'

		-- Save the Order.
		INSERT INTO dbo.Orders
		(
		   ListingID,
		   OrderNumber,
		   OrderTotal,
		   OrderStatusID,
		   CreateDate,
		   EditDate,
		   CreateUser,
		   EditUser
		)
		VALUES
		(
		   @ListingID,
		   dbo.GenerateOrderNumber(RAND()),
		   @OrderTotal,
		   @OrderStatusID,
		   @CurrentDate,
		   @CurrentDate,
		   @UserID,
		   @UserID
		);

	DECLARE @OrderID INT = SCOPE_IDENTITY();

	-- Save the associated line item records.
	INSERT INTO dbo.OrderLineItems
	(
		[OrderID],
		[Description],
		[LineItemTotal]
	)
	SELECT @OrderID, [Description], LineItemTotal FROM @OrderLineItems;

	-- Return the newly created Order ID.
	SELECT @OrderID;

END;