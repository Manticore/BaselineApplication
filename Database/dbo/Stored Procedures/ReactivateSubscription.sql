﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure reactivates a listing subscription that had previously been cancelled.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.ReactivateSubscription
       @SubscriptionID INT
AS
BEGIN
    SET NOCOUNT ON;

    -- Set the cancelled date to NULL.
    UPDATE dbo.Subscriptions
           SET CancelledDate = NULL
    WHERE SubscriptionID = @SubscriptionID;

    DECLARE @ListingID INT;
    -- Retrieve the listing ID for the associated subscription.
    SELECT @ListingID = ListingID
    FROM dbo.Subscriptions
    WHERE SubscriptionID = @SubscriptionID;
    
    -- Set the cancelled date to NULL for the associated listing.
    UPDATE dbo.Listings
           SET CancelledDate = NULL
    WHERE ListingID = @ListingID;

END;