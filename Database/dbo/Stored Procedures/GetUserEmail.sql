﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns an email address associated with a specified user.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetUserEmail
       @UserID INT
AS
BEGIN

	SET NOCOUNT ON;

    SELECT EmailAddress
    FROM dbo.EmailAddresses
    WHERE UserID = @UserID;

END;