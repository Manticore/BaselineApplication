﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure saves an edit to an existing industry, or creates a new one.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.SaveIndustry
       @IndustryID   INT = NULL, 
       @IndustryName NVARCHAR(200)
AS
BEGIN

    -- Update the industry if it exists in the database.
    UPDATE dbo.Industries
           SET Name = @IndustryName
    WHERE IndustryID = @IndustryID;

    -- If not, create a new industry.
    IF @@ROWCOUNT = 0
        BEGIN

		  INSERT INTO dbo.Industries
		  (
			 Name
		  )
		  VALUES
		  (
			 @IndustryName
		  );

        END;

END;