﻿using System.Collections.Generic;
using Models.Models;

namespace Models.Interfaces
{
    public interface IOrderRepository
    {
        PagedList<Order> GetOrdersPaged(OrdersParameterSet parameters);
        Order GetOrder(int orderID);
        int? SaveOrder(Order order, bool isPaid, int userID);
        IEnumerable<OrderStatus> GetOrderStatuses();
        IEnumerable<PaymentType> GetPaymentTypes(bool isAdmin);
        int? SaveOrderPayment(Payment payment, int orderIdentifier, int userIdentifier);
        PagedList<PrintOrderReportModel> GetUnpaidOrdersForPrinting(PrintOrdersParameterSet parameters);
        int? GetOrderOwnerUserID(int orderIdentifier);
        bool IsUserOrderOwner(int userIdentifier, int orderIdentifier);
        void SaveOrderPrintedDate(int orderIdentifier, int userIdentifier);
        OrderBillDataModel GetOrderDetailsForBill(int orderIdentifier);
        Payment GetPayment(int paymentIdentifier);
        int? GetUnpaidOrderIdentifierForListing(int listingIdentifier);
        int? GetCreditCardPaymentTypeID();
        bool HasOrderBeenInvoiced(int orderIdentifier);
        void DeleteUnpaidOrder(int orderIdentifier);
    }
}
