﻿
// JavaScript class used to access Post methods asynchronously.
// Each method corresponds to a Post method in a Controller.
var AjaxMethods = (function () {
    var ajax = {};

    // Returns the rendered _ListingDetail partial view for the specified listing.
    ajax.GetListingDetailsHTML = function (listingID) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ listingID: listingID }),
            url: '/Listings/GetListingDetailsHTML',
            cache: false
        });
    };

    // Returns the rendered _ListindgDetail partial view for the specified listing.
    ajax.GetOrderDetailsHTML = function (orderID) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ orderID: orderID }),
            url: '/Orders/GetOrderDetailsHTML',
            cache: false
        });
    };

    // Converts a bulk listing to a manual listing.
    ajax.ConvertBulkListing = function (listingIdentifier, username, emailAddress) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ listingIdentifier: listingIdentifier, username: username, emailAddress: emailAddress }),
            url: '/SiteUtilities/ConvertBulkListing',
            cache: false
        });
    };

    // Marks the listing as cancelled in the database.
    ajax.CancelListing = function (listingIdentifier) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ listingIdentifier: listingIdentifier }),
            url: '/Listings/CancelListing',
            cache: false
        });
    };

    // Clears a cancelled flag for a given listing.
    ajax.ClearCancelledListingFlag = function (listingIdentifier) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ listingIdentifier: listingIdentifier }),
            url: '/Listings/ClearCancelledListingFlag',
            cache: false
        });
    };

    // Converts a site user to an administrator.
    ajax.ConvertUserToAdmin = function (userID) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ userID: userID }),
            url: '/SiteUtilities/ConvertUserToAdmin',
            cache: false
        });
    };

    // Returns the subscription details rendered as HTML.
    ajax.GetSubscriptionDetailsHTML = function (subscriptionIdentifier) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ subscriptionIdentifier: subscriptionIdentifier }),
            url: '/Subscriptions/GetSubscriptionDetailsHTML',
            cache: false
        });
    };

    // Returns the rendered view of the _ListingSummary partial view.
    ajax.SearchListings = function (searchValues, page, numberPerPage) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ pageNumber: page, numberPerPage: numberPerPage, search: searchValues.search }),
            url: '/Listings/GetListingResults',
            cache: false
        });
    };

    // Returns the rendered view of the _TradeLeadSummary partial view.
    ajax.SearchTradeLeads = function (searchValues, page, numberPerPage) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ pageNumber: page, numberPerPage: numberPerPage, search: searchValues.search }),
            url: '/TradeLeads/GetTradeLeadResults',
            cache: false
        });
    };

    // Returns the rendered partial view _TradeLeadDetail for the specified trade lead.
    ajax.GetTradeLeadDetail = function (tradeLeadID) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ tradeLeadID: tradeLeadID }),
            url: '/TradeLeads/GetQuickViewDetail',
            cache: false
        });
    };

    // Returns the rendered partial view _TradeLeadDetail for the specified trade lead.
    ajax.DeleteTradeLead = function (tradeLeadID) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ tradeLeadID: tradeLeadID }),
            url: '/SiteUtilities/DeleteTradeLead',
            cache: false
        });
    };

    // Returns the rendered partial view _TradeLeadDetail for the specified trade lead.
    ajax.DeleteDirectory = function (directoryID) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ directoryID: directoryID }),
            url: '/SiteUtilities/DeleteDirectory',
            cache: false
        });
    };

    // Ajax method used to save a user's default credit card.
    ajax.SaveCustomerCard = function (cardToken) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify(cardToken),
            dataType: 'json',
            url: '/Account/UpdatePaymentMethod',
            cache: false
        });
    }

    // Ajax method used to change a user's password.
    ajax.ChangePassword = function (passwordModel) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ password: passwordModel.Password }),
            dataType: 'json',
            url: '/Account/ChangePassword',
            cache: false
        });
    }

    // Ajax method used to change a user's default email address.
    ajax.ChangeEmailAddress = function (emailModel) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ emailModel: emailModel }),
            dataType: 'json',
            url: '/Account/UpdateEmailAddress',
            cache: false
        });
    }

    // Ajax method used to save a new or existing trade lead.
    ajax.UpsertTradeLead = function (formObject) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ tradeLead: formObject.TradeLead }),
            dataType: 'json',
            url: '/TradeLeads/UpsertTradeLead',
            cache: false
        });
    }

    // Ajax method used to save a new or existing listing.
    ajax.UpsertListing = function(listing) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            // Pass the anti-forgery token to be validated.
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify(listing),
            dataType: 'json',
            url: '/Listings/UpsertListing',
            cache: false
        });
    }
    
    // Ajax method used to generate orders for a specified set of listings.
    ajax.GenerateOrders = function (listingIdentifiers) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            headers: { "__RequestVerificationToken": token },
            data: { "listingIdentifiers" : listingIdentifiers },
            url: '/SiteUtilities/GenerateOrders'
        });
    }

    // Ajax method used to cancel a specified subscription.
    ajax.CancelSubscription = function (listingIdentifier) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ listingIdentifier: listingIdentifier }),
            dataType: 'json',
            url: '/Subscriptions/CancelSubscription',
            cache: false
        });
    }

    // Ajax method used to reactivate a cancelled subscription.
    ajax.ReactivateSubscription = function (listingIdentifier) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ listingIdentifier: listingIdentifier }),
            dataType: 'json',
            url: '/Subscriptions/ReactivateSubscription',
            cache: false
        });
    }

    // Returns a rendered partial view of a user's credit card.
    ajax.GetUserCreditCards = function () {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: null,
            url: '/Account/GetUserCreditCards',
            cache: false
        });
    };

    // Ajax method used to send a contact email from a user.
    ajax.SendContactEmail = function (message) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ message: message }),
            url: '/Information/SendContactEmail',
            cache: false
        });
    };

    // Ajax method used to save a note associated with a listing.
    ajax.SaveListingNote = function (note) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ note: note }),
            dataType: 'json',
            url: '/Listings/SaveListingNote',
            cache: false
        });
    }

    // Ajax method used to retrieve notes for a specified listing.
    ajax.GetListingNotes = function (listingIdentifier) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ listingIdentifier: listingIdentifier }),
            url: '/Listings/GetListingNotes',
            cache: false
        });
    };

    // Ajax method used to save an email added via the "Subscribe" form on the homepage.
    ajax.SaveMailingListEmail = function (emailAddress) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ emailAddress: emailAddress }),
            dataType: 'json',
            url: '/Home/SaveMailingListEmail',
            cache: false
        });
    }

    // Returns the industries in the database as JSON.
    ajax.GetIndustries = function () {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            url: '/SiteUtilities/GetIndustries',
            cache: false
        });
    };

    // Saves a given industry in the database.
    ajax.SaveIndustry = function (industry) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ industry: industry }),
            url: '/SiteUtilities/SaveIndustry',
            cache: false
        });
    };

    // Deletes a specified industry from the database.
    ajax.DeleteIndustry = function (industryIdentifier) {
        // Grab the anti-forgery token to be validated in the POST request.
        var token = $('[name=__RequestVerificationToken]').val();
        return $.ajax({
            type: 'POST',
            contentType: 'application/json',
            headers: { "__RequestVerificationToken": token },
            data: JSON.stringify({ industryIdentifier: industryIdentifier }),
            url: '/SiteUtilities/DeleteIndustry',
            cache: false
        });
    };

    return ajax;
}());