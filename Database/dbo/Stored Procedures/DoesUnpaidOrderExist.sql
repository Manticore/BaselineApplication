﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns a boolean value indicating whether an unpaid order
-- exists for the specified listing. This procedure is used primarily to stop
-- additional orders from being created when an unpaid one exists.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.DoesUnpaidOrderExist
       @ListingIdentifier INT
AS
BEGIN

	SET NOCOUNT ON;

    DECLARE @UnpaidOrderStatusID INT;

    SELECT @UnpaidOrderStatusID = OrderStatusID
    FROM dbo.OrderStatuses
    WHERE Status = 'Unpaid';

    IF EXISTS(SELECT 1 FROM dbo.Orders WHERE ListingID = @ListingIdentifier AND Orders.OrderStatusID = @UnpaidOrderStatusID)
	   SELECT 1;
    ELSE
	   SELECT 0;

END;