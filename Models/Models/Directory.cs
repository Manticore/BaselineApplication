﻿namespace Models.Models
{
    public class Directory
    {
        public string Description { get; set; }
        public int    DirectoryID { get; set; }
        public string Name        { get; set; }
        public bool IsDefault { get; set; }
    }
}