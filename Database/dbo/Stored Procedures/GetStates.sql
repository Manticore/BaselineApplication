﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the States available in the site.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetStates
AS
BEGIN

	SET NOCOUNT ON;

    SELECT [StateID],
		   [Abbreviation],
           [Name]
    FROM dbo.States;

END;