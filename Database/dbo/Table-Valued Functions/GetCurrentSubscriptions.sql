﻿
-------------------------------------------------------------------------------
-- Table-Valued Function Description						  
-------------------------------------------------------------------------------
-- This table valued function returns the most recent subscription record where 
-- the subscription has not expired (EndDate < CurrentDate).
-------------------------------------------------------------------------------
CREATE FUNCTION dbo.GetCurrentSubscriptions()
RETURNS @Subscriptions TABLE
(
    SubscriptionID INT NOT NULL, 
    ListingID INT NOT NULL, 
    OrderID INT NOT NULL, 
    StripeSubscriptionID NVARCHAR(50),
	CurrentDirectoryCount INT NOT NULL,
    PaidDirectoryCount INT NOT NULL, 
	CurrentDescriptionLinesCount INT NOT NULL,
    PaidDescriptionLinesCount INT NOT NULL, 
    StartDate DATETIME2 NOT NULL, 
    EndDate DATETIME2 NOT NULL, 
    CancelledDate DATETIME2, 
    SubscriptionCost INT NOT NULL, 
    MaxSubscriptionCost INT NOT NULL, 
    SubscriptionStatusID INT NOT NULL, 
    CreateDate DATETIME2 NOT NULL, 
    EditDate DATETIME2 NOT NULL, 
    CreateUser INT NOT NULL, 
    EditUser INT NOT NULL
)
AS
BEGIN

    DECLARE @CurrentDate DATETIME2 = dbo.GetCurrentDate();

    INSERT INTO @Subscriptions
    (
	   SubscriptionID, 
	   ListingID, 
	   OrderID, 
	   StripeSubscriptionID,
	   CurrentDirectoryCount,
	   PaidDirectoryCount,
	   CurrentDescriptionLinesCount,
	   PaidDescriptionLinesCount, 
	   StartDate, 
	   EndDate, 
	   CancelledDate, 
	   SubscriptionCost, 
	   MaxSubscriptionCost, 
	   SubscriptionStatusID, 
	   CreateDate, 
	   EditDate, 
	   CreateUser, 
	   EditUser
    )
    SELECT S.SubscriptionID, 
           S.ListingID, 
           S.OrderID, 
           S.StripeSubscriptionID,
		   S.CurrentDirectoryCount,
           S.PaidDirectoryCount, 
		   S.CurrentDescriptionLinesCount,
           S.PaidDescriptionLinesCount, 
           S.StartDate, 
           S.EndDate, 
           S.CancelledDate, 
           S.SubscriptionCost, 
           S.MaxSubscriptionCost, 
           S.SubscriptionStatusID, 
           S.CreateDate, 
           S.EditDate, 
           S.CreateUser, 
           S.EditUser
    FROM(SELECT MAX(SubscriptionID) AS SubscriptionID, 
                ListingID
         FROM dbo.Subscriptions S
         WHERE S.StartDate <= @CurrentDate AND S.EndDate >= @CurrentDate
         GROUP BY ListingID) AS MostRecentActiveSubscription 
	    INNER JOIN dbo.Subscriptions S
            ON S.SubscriptionID = MostRecentActiveSubscription.SubscriptionID;

    RETURN;
END;