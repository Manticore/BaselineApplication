﻿
-------------------------------------------------------------------------------
-- Scalar-Valued Function Description						  
-------------------------------------------------------------------------------
-- This scalar valued function returns BIGINT to be used as the order number.
-------------------------------------------------------------------------------
CREATE FUNCTION dbo.GenerateOrderNumber(@Rand FLOAT)
RETURNS BIGINT
AS
BEGIN

    DECLARE @OrderNumber BIGINT;
    DECLARE @OrderNumberExists BIT;

    -- Genertae an order number and see if it exists.
    SET @OrderNumber	   = CONVERT(NUMERIC(10, 0), @Rand * 8999999999) + 1000000000;
    SET @OrderNumberExists = dbo.DoesOrderNumberExist(@OrderNumber);

    -- If it does, regenerate and check again. Continue until a unique order number is created.
    WHILE @OrderNumberExists = 1
        BEGIN
            SET @OrderNumber	   = CONVERT(NUMERIC(10, 0), @Rand * 8999999999) + 1000000000;
            SET @OrderNumberExists = dbo.DoesOrderNumberExist(@OrderNumber);
        END;

    RETURN @OrderNumber;

END;
