﻿using System;
using System.Collections.Generic;
using Models.Models;

namespace Models.Interfaces
{
    public interface IListingRepository
    {
        IEnumerable<Listing> GetListings(int page, int numberOfRecords, string keyword, int? directoryID, List<int?> industryIDs, int? stateID);
        Listing GetSpecificListing(int? listingID);
        int? UpsertListing(Listing listing, int userID);
        void DeleteListing(int listingID);
        PagedList<BillableListingsModel> GetBillableListings(BillableListingParameterSet parameters);
        PagedList<BulkListingsReportModel> GetBulkListings(BulkListingParameterSet parameters);
        bool IsUserListingOwner(int? listingID, int userID);
        IEnumerable<Listing> GetUserListings(int userID);
        int? CreateSubscription(Subscription subscription, int userID);
        Listing GetSpecificListingFull(int? listingID);
        void CancelSubscription(int subscriptionID);
        void ReactivateSubscription(int subscriptionID);
        Subscription GetSubscription(int subscriptionIdentifier);
        PagedList<ManagedListingsModel> GetManagedListings(ManagedListingsReportParameters parameters);
        string GetDefaultPlanID();
        bool DoesUnpaidOrderExist(int listingID);
        void UpdateLocalSubscription(int subscriptionID, int directoryCount, int descriptionLineCount, int? newSubscriptionCost, string stripeSubscriptionID, int userID);
        IEnumerable<ListingType> GetListingTypes();
        Subscription GetCurrentSubscription(int listingIdentifier);
        int? GetListingOwnerUserID(int listingIdentifier);
        void SaveListingNote(Note note, int userIdentifier);
        IEnumerable<Note> GetListingNotes(int listingIdentifier);
        void UpdateListingType(int listingIdentifier, string listingType);
        bool IsUserSubscriptionOwner(int subscriptionIdentifier, int userIdentifier);
        bool DoesSubscriptionExist(string stripeSubscriptionID, DateTime subscriptionStart, DateTime subscriptionEnd);
        void ClearCancelledFlag(int listingIdentifier);
        void CancelListing(int listingIdentifier);
    }
}