﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure indicates whether a local subscription record exists with a
-- specified Stripe Subscription ID and a specific start and end date range.
-- This used primarily to determine if a webhook for a subscription payment 
-- needs to create a local subscription in the database. The event that carries
-- a new subscription purchase can also be triggered by other processes. If this
-- check was not used it's possible multiple subscriptions could be entered for 
-- a single listing.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.DoesSubscriptionExist
       @StripeSubscriptionID NVARCHAR(50), 
       @StartDate			 DATETIME2, 
       @EndDate				 DATETIME2
AS
BEGIN

    IF EXISTS(SELECT 1
              FROM Subscriptions
              WHERE CAST(StartDate AS DATE) = CAST(@StartDate AS DATE)
				AND CAST(EndDate AS DATE)   = CAST(@EndDate AS DATE)
				AND Subscriptions.StripeSubscriptionID = @StripeSubscriptionID)
        BEGIN
            SELECT 1;
        END;
    ELSE
        BEGIN
            SELECT 0;
        END;

END;