﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns a boolean value indicating whether a given user is the
-- owner of a given listing identifier.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.IsUserListingOwner
       @ListingID INT, 
       @UserID INT
AS
BEGIN

	SET NOCOUNT ON;

    IF EXISTS(SELECT '1' FROM dbo.Listings WHERE ListingID = @ListingID AND UserID = @UserID)
		SELECT 1;
    ELSE
		SELECT 0;

END;
