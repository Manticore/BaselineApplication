﻿using System;

namespace Models.Models
{
    public class Payment
    {
        public int      PaymentID      { get; set; }
        public DateTime PaymentDate    { get; set; }
        public string   PaymentDetails { get; set; }
        public int      PaymentTypeID  { get; set; }
        public string   Type    { get; set; }
        public int      OrderID        { get; set; }

        // Credit Card Fields
        public string CardToken            { get; set; }
        public string CreditCardName       { get; set; }
        public string CreditCardNumber     { get; set; }
        public string CreditCardExpiration { get; set; }

        // Check Fields
        public string CheckBankName      { get; set; }
        public string CheckName          { get; set; }
        public string CheckRoutingNumber { get; set; }
        public string CheckAccountNumber { get; set; }
        public string CheckNumber        { get; set; }

        // ACH Field
        public string TransferBankName      { get; set; }
        public string TransferName          { get; set; }
        public string TransferRoutingNumber { get; set; }
        public string TransferAccountNumber { get; set; }
        public string TransferCheckNumber   { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime EditDate   { get; set; }
        public int      CreateUser { get; set; }
        public int      EditUser   { get; set; }
    }
}
