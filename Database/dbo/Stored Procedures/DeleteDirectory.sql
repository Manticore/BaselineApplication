﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This stored procedure deletes a specified directory. A directory cannot be
-- deleted if it is associated with one or more listings due to a foreign
-- key constraint.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.DeleteDirectory
       @DirectoryID INT
AS
BEGIN

    DELETE FROM dbo.Directories
    WHERE DirectoryID = @DirectoryID;

END;