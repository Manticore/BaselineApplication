﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns all the listing notes for a given listing.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetListingNotes
       @ListingID INT
AS
BEGIN

	SET NOCOUNT ON;

    SELECT NoteID, 
           NoteText, 
           CreateDate, 
           CreateUser,
		   UP.UserName AS CreateUserName
    FROM dbo.Notes N
		INNER JOIN dbo.UserProfile UP
			ON N.CreateUser = UP.UserId
    WHERE ListingID = @ListingID
    ORDER BY CreateDate DESC;

END;