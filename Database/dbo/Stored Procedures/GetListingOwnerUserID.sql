﻿
-------------------------------------------------------------------------------
-- Stored Procedure Description							  
-------------------------------------------------------------------------------
-- This procedure returns the user identifier of the "owner" user. The user 
-- that is associated with the listing.
-------------------------------------------------------------------------------
CREATE PROCEDURE dbo.GetListingOwnerUserID
       @ListingID INT
AS
BEGIN

	SET NOCOUNT ON;

    SELECT UserID
    FROM dbo.Listings
    WHERE ListingID = @ListingID;

END;